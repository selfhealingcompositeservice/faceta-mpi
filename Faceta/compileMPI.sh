CLASSPATH=
for x in $(find lib/ | grep jar$); do CLASSPATH="$CLASSPATH:$x"; done
SRC=$(find | grep java$)

export MPJ_HOME=/home/rafa/workspace/dauphine_ws/mpi/mpj-v0_38/
export JUNG_HOME=/home/rafa/workspace/jung/jung2-2_0_1/
export COLT_HOME=/home/rafa/workspace/jung/colt/lib/
export COMMOS_HOLE=/home/rafa/workspace/jung/commons-collections4-4.0-alpha1/
export CLASSPATH=$CLASSPATH:$MPJ_HOME/lib/mpj.jar:$JUNG_HOME/*.jar:$COLT_HOME/*.jar:$COMMOS_HOME/*.jar
javac -cp "$CLASSPATH" $SRC -d bin
