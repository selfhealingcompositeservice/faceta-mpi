CLASSPATH=
for x in $(find lib/ | grep jar$); do CLASSPATH="$CLASSPATH:$x"; done

CLASSPATH=$CLASSPATH:bin

export MPJ_HOME=mpi/mpj-v0_38/
export PATH=$PATH:$MPJ_HOME/bin


mpjrun.sh -np 9 -cp $CLASSPATH ve.usb.Main.MainSimpleMPI

for ((  i = 0 ;  i <= 10;  i++  ))
do
  mpjrun.sh -np 9 -cp $CLASSPATH ve.usb.Main.MainSimpleMPI
done
