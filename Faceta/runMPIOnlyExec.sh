#Entrada
# <Ahora viene desde archivo> 1. Nombre del query (Contiene el query y la WSDN resultado)
#2. Tamaño del plan + 1 (Un nodo para cada Engine Thread y un nodo para el Execution EnginUn nodo para cada Engine Thread y un nodo para el Execution Engine)
#3. Numero de ejecuciones
# <Ahora viene desde archivo> 4. Mecanismo de tolerancia a fallas a ser usado: BR (Backward Recovery), FR (Forward Recovery). 
#   Si no se especifica el mecanismo de tolerancia a fallas, NIL es usado, lo que implica que cualquier mecanismo es valido.

CLASSPATH=lib/nanoxml-2.2.1.jar:lib/nanoxml-lite-2.2.1.jar:lib/owls-api-3.0.jar:lib/nanoxml-lite-2.2.1.jar:lib/log4j-1.2.16.jar:lib/slf4j-api-1.6.4.jar:lib/slf4j-log4j12-1.6.4.jar:lib/jgrapht-jdk1.6.jar

CLASSPATH=
for x in $(find lib/ | grep jar$); do CLASSPATH="$CLASSPATH:$x"; done
SRC=$(find | grep java$)
CLASSPATH=$CLASSPATH:bin



for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1 -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done
