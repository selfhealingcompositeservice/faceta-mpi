#!/bin/bash
#Entrada
# <Ahora viene desde archivo> 1. Nombre del query (Contiene el query y la WSDN resultado)
#2. Tamaño del plan + 1 (Un nodo para cada Engine Thread y un nodo para el Execution EnginUn nodo para cada Engine Thread y un nodo para el Execution Engine)
#3. Numero de ejecuciones
# <Ahora viene desde archivo> 4. Mecanismo de tolerancia a fallas a ser usado: BR (Backward Recovery), FR (Forward Recovery). 
#   Si no se especifica el mecanismo de tolerancia a fallas, NIL es usado, lo que implica que cualquier mecanismo es valido.

CLASSPATH=lib/nanoxml-2.2.1.jar:lib/nanoxml-lite-2.2.1.jar:lib/owls-api-3.0.jar:lib/nanoxml-lite-2.2.1.jar:lib/log4j-1.2.16.jar:lib/slf4j-api-1.6.4.jar:lib/slf4j-log4j12-1.6.4.jar:lib/jgrapht-jdk1.6.jar


CLASSPATH=$CLASSPATH:bin


#######################EJECUCION###################
echo '../saved_data/query_results/NP-10/atomic/Q-Transac-Escenario2-BA200_3_8-NP-10-IND-11' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.1
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.01
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.05
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.001
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.005
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

#######################EJECUCION###################
echo '../saved_data/query_results/NP-10/atomic/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-01' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.1
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.01
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.05
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.001
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.005
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done



#######################EJECUCION###################
echo '../saved_data/query_results/NP-10/atomic/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-04' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.1
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.01
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.05
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.001
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.005
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

#######################EJECUCION###################
echo '../saved_data/query_results/NP-10/atomic/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-06' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.1
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.01
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.05
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.001
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.005
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

#######################EJECUCION###################
echo '../saved_data/query_results/NP-10/atomic/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-07' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.1
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.01
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.05
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.001
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.005
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


#######################EJECUCION###################
echo '../saved_data/query_results/NP-10/atomic/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-10' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.1
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.01
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.05
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.001
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.005
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


#######################EJECUCION###################
echo '../saved_data/query_results/NP-10/atomic/Q-Transac-Manual-R1-SF50_4_8-NP-10-IND-10' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.1
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.01
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.05
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.001
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.005
executor.ftmechanism=NIL
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


