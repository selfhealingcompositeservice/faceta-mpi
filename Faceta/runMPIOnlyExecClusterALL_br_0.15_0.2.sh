#!/bin/bash
#Entrada
# <Ahora viene desde archivo> 1. Nombre del query (Contiene el query y la WSDN resultado)
#2. Tamaño del plan + 1 (Un nodo para cada Engine Thread y un nodo para el Execution EnginUn nodo para cada Engine Thread y un nodo para el Execution Engine)
#3. Numero de ejecuciones
# <Ahora viene desde archivo> 4. Mecanismo de tolerancia a fallas a ser usado: BR (Backward Recovery), FR (Forward Recovery). 
#   Si no se especifica el mecanismo de tolerancia a fallas, NIL es usado, lo que implica que cualquier mecanismo es valido.

CLASSPATH=lib/nanoxml-2.2.1.jar:lib/nanoxml-lite-2.2.1.jar:lib/owls-api-3.0.jar:lib/nanoxml-lite-2.2.1.jar:lib/log4j-1.2.16.jar:lib/slf4j-api-1.6.4.jar:lib/slf4j-log4j12-1.6.4.jar:lib/jgrapht-jdk1.6.jar


CLASSPATH=$CLASSPATH:bin


#######################EJECUCION###################
echo '../saved_data/query_results/BA200_3_4/Q-Transac-Escenario1-BA200_3_4-NP-10-IND-01' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

#######################EJECUCION###################
echo '../saved_data/query_results/BA200_3_4/Q-Transac-Escenario1-BA200_3_4-NP-10-IND-02' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

#######################EJECUCION###################
echo '../saved_data/query_results/BA200_3_8/Q-Transac-Escenario2-BA200_3_8-NP-10-IND-03' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

#######################EJECUCION###################
echo '../saved_data/query_results/BA200_3_8/Q-Transac-Escenario2-BA200_3_8-NP-10-IND-04' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

#######################EJECUCION###################
echo '../saved_data/query_results/BA800_3_4/Q-Transac-Escenario1-BA800_3_4-NP-10-IND-05' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


#######################EJECUCION###################
echo '../saved_data/query_results/BA800_3_4/Q-Transac-Escenario1-BA800_3_4-NP-10-IND-06' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


#######################EJECUCION###################
echo '../saved_data/query_results/BA800_3_4/Q-Transac-Escenario1-BA800_3_4-NP-10-IND-07' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done














#######################EJECUCION###################
echo '../saved_data/query_results/BA800_3_8/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-08' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


#######################EJECUCION###################
echo '../saved_data/query_results/BA800_3_8/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-09' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


#######################EJECUCION###################
echo '../saved_data/query_results/BA800_3_8/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-10' > tcws_to_run

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.15
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done

echo 'executor.threads=5
executor.mpi.processes.number=10
executor.service.execution.failure=0.2
executor.ftmechanism=BR
executor.queryresult.file=tcws_to_run' > executor.conf


for ((  i = 1 ;  i <= $2;  i++  ))
do
  mpjrun.sh -np $1  -dev niodev  -cp $CLASSPATH ve.usb.Main.MainMPIOWLS
done


