CLASSPATH=
for x in $(find lib/ | grep jar$); do CLASSPATH="$CLASSPATH:$x"; done

CLASSPATH=$CLASSPATH:bin

export MPJ_HOME=/home/rafa/workspace_composer/mpi/mpj-v0_38/
export PATH=$PATH:$MPJ_HOME/bin

filename=results/MPI_result_Q-Transac-Escenario1-BA200_3_4-NP-10-IND-10_Q-Transac-Escenario1-BA200_3_4-NP-10-IND-10_size
n=1
size=$(awk -v fld=$n '{if(NF>=fld) {print $fld} } ' "$filename")

size=`expr $size + 1`



for ((  i = 1 ;  i <= $1;  i++  ))
do
  mpjrun.sh -np $size -cp $CLASSPATH ve.usb.executor.mpi.ExecutionEngineMPI MPI_result_Q-Transac-Escenario1-BA200_3_4-NP-10-IND-10_Q-Transac-Escenario1-BA200_3_4-NP-10-IND-10 Q-Transac-Escenario1-BA200_3_4-NP-10-IND-10
done
