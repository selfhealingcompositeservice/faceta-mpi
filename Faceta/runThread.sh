CLASSPATH=
for x in $(find lib/ | grep jar$); do CLASSPATH="$CLASSPATH:$x"; done

CLASSPATH=$CLASSPATH:bin

export MPJ_HOME=/home/rafa/workspace_composer/mpi/mpj-v0_38/
export PATH=$PATH:$MPJ_HOME/bin

num=1
if [ $# -ne 0 ]
then
     num=$1
fi

for ((  i = 0 ;  i < num;  i++  ))
do
 java -cp $CLASSPATH ve.usb.Main.MainThreadOWLS
done
