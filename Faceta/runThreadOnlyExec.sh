#Entrada
#1. Nombre del query (Contiene el query y la WSDN resultado)
#2. Numero de ejecuciones
#3. Mecanismo de tolerancia a fallas a ser usado: BR (Backward Recovery), FR (Forward Recovery). 
#   Si no se especifica el mecanismo de tolerancia a fallas, NIL es usado, lo que implica que cualquier mecanismo es valido.

CLASSPATH=
for x in $(find lib/ | grep jar$); do CLASSPATH="$CLASSPATH:$x"; done

CLASSPATH=$CLASSPATH:bin

export MPJ_HOME=/home/rafa/workspace_composer/mpi/mpj-v0_38/
export PATH=$PATH:$MPJ_HOME/bin


for ((  i = 0 ;  i < $2;  i++  ))
do
 java -cp $CLASSPATH ve.usb.Main.MainThreadOWLS $1 $3
done
