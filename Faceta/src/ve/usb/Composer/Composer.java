/*
 * Composer.java
 *
 * Created on 3 de febrero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.Composer;

import java.util.ArrayList;

import ve.usb.PetriNets.Arco;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;

public class Composer {

	private MarkedPN WSDN_Q;
	private Query query;
	private ArrayList<Vertice> O_M;
	private QueryResults q_result;

	public Composer(MarkedPN myMPN, Query myq) {
		WSDN_Q = myMPN;
		query = myq; 
		q_result = new QueryResults();
		O_M = new ArrayList<Vertice>();
	}

	private boolean CutOff(Vertice tr) {
		boolean ret = true;
		ArrayList<Vertice> suc = WSDN_Q.sucesores(tr);
		for (int i=0;i<suc.size();i++) {
			if (!O_M.contains(suc.get(i))) {
				ret=false;
				break;
			}
		}
		return ret;
	}

	private void fireTrans(Vertice tr, ArrayList<Vertice> suc, ArrayList<Vertice> pred) {
		addTokensToOutputs(tr,suc,pred);
		removeTokensFromInputs(pred);
		updateColor(tr);
	}

	private void addTokensToOutputs(Vertice tr, ArrayList<Vertice> suc, ArrayList<Vertice> pred) {
		boolean go=true;	

		for (int i=0;i<pred.size();i++) {
			if (pred.get(i).isAtomic()) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomic();
					}
				}
				go=false;
				break;
			} // if atomic
			else if (pred.get(i).isAtomicRet()) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomicRet();
					}
				}
				go=false;
				break;
			} // is atomic ret
			else if ((pred.get(i).isCompensa()) && ((tr.isPivot()) || (tr.isPivotRet()) || (tr.isAtomic()) || (tr.isAtomicRet()))) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomic();
					}
				}

				go=false;
				break;
			} //is compensa
			else if ((pred.get(i).isCompensa()) && ((tr.isCompensa()) || (tr.isCompensaRet()))) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setCompensa();
					}
				}

				go=false;
				break;
			} // is compensa 2
		}

		if (go) {
			if ((tr.isCompensa()) || (tr.isCompensaRet()) || (tr.isAtomic()) || (tr.isAtomicRet())) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setColor(tr.getColor().get(0));
					}
				}
			}
			else if (tr.isPivot()) { 
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomic();
					}
				}
			}
			else if (tr.isPivotRet()) {
				for (int j=0;j<suc.size();j++){
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomicRet();
					}
				}
			}
		}
	} 

	private void removeTokensFromInputs(ArrayList<Vertice> pred) {
		for (int i=0;i<pred.size();i++) {
			if (!query.getOuts().contains(pred.get(i))){
				WSDN_Q.getPlace(pred.get(i).getName(),pred.get(i).getReplica()).removeColors();
			}
		}
	}

	private void updateColor(Vertice tr) {
		if (((WSDN_Q.getMarked() == Vertice.INITIAL) ||  (WSDN_Q.getMarked() == Vertice.COMPENSARET)) && (tr.isPivot())) 
			WSDN_Q.setMarkedColor(Vertice.ATOMIC);
		else if (((WSDN_Q.getMarked() == Vertice.INITIAL) ||  (WSDN_Q.getMarked() == Vertice.COMPENSARET)) && (tr.isPivotRet())) 
			WSDN_Q.setMarkedColor(Vertice.ATOMICRET); 	
		else if (((WSDN_Q.getMarked() == Vertice.INITIAL) ||  (WSDN_Q.getMarked() == Vertice.COMPENSARET)) && (tr.isAtomic() || tr.isAtomicRet() || tr.isCompensa() || tr.isCompensaRet() )) 
			WSDN_Q.setMarkedColor(tr.getColor().get(0)); 	
		else if ((WSDN_Q.getMarked() == Vertice.COMPENSA) && (tr.isAtomic() || tr.isAtomicRet() || tr.isPivot() || tr.isPivotRet() )) 
			WSDN_Q.setMarkedColor(Vertice.ATOMIC); 	
	}

	private ArrayList<Vertice>  getSucesorsTransactions(ArrayList<Vertice> suc) {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();
		for (int i=0;i< suc.size();i++) {
			ret.addAll(WSDN_Q.sucesores(suc.get(i)));
		} 
		return ret;
	}

	private ArrayList<Vertice> calculateParallel(ArrayList<Vertice> firab, ArrayList<Vertice> t_suc, Vertice s) {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();
		for (int i=0;i< firab.size();i++) {
			if (t_suc.contains(firab.get(i)) || (firab.get(i).getName().compareTo(s.getName())==0)) ;
			else ret.add(firab.get(i));
		}
		return ret;
	}

	private Vertice getBest(ArrayList<Vertice> firab) {
		double qlt=0.0;
		int pos = 0;
		for (int i=0;i<firab.size();i++) {
			if (qlt < firab.get(i).getQuality()) {

				qlt = firab.get(i).getQuality();
				pos = i;
			}
		}
		return firab.get(pos);
	}


	private boolean reachedFinal(Query q) {
		boolean ret=false;
		ArrayList<Vertice> q_outputs = q.getOuts();
		int m=q_outputs.size();
		ArrayList<Vertice> wsdn_outputs = WSDN_Q.getOutputs();
		for (int i= 0; i <q_outputs.size(); i++) {
			for (int j=0;j<wsdn_outputs.size();j++) {
				Vertice o = wsdn_outputs.get(j);		
				if (o.getName().compareTo(q_outputs.get(i).getName())==0) {
					if (o.getColor().size() != 0) {
						if (q.getRisk() == Query.R0) {
							if (o.isCompensa() || o.isCompensaRet()) { 
								m--;
								break;				
							}
						}
						else if (o.isCompensa() || o.isCompensaRet() || o.isAtomic() || o.isAtomicRet()) {    
							m--;
							break;
						}
					}
				}
			}
		}
		if (m==0) ret = true;
		return ret;
	}

	/**
	 * Implementation of Step 4: Create Final Plan (cleaning) Alg. 5
	 **/

	 //DEBERIA ESTAR EN MARKEDPN?? O DONDE??,no AQUIIII
			 private MarkedPN createFinalPlan(ArrayList<Vertice> seq, Query aq) {
				 ArrayList<Vertice> predec = new ArrayList<Vertice>();
				 ArrayList<Vertice> pda = new ArrayList<Vertice>();
				 ArrayList<Vertice> temp = new ArrayList<Vertice>();
				 MarkedPN ret = new MarkedPN(aq.getName());
				 ArrayList<Vertice> pdatoadd = null;
				 ArrayList<String> inputQ = aq.getInsName();
				 Vertice a;
				 Arco ar;
				 ret.setIniOuts(aq.getOuts());
				 ArrayList<Vertice> places = ret.getPlaces();
				 for (int i=0;i<places.size();i++) {
					 temp = WSDN_Q.predecesores(places.get(i));
					 for (int j=0;j<temp.size();j++) {
						 if (seq.contains(temp.get(j))) {		
							 ret.addT(temp.get(j));
							 ar = new Arco(temp.get(j).getName(),places.get(i).getName());
							 ret.add(ar);//Add arc F(Si,o)
							 if (!predec.contains(temp.get(j)))
								 predec.add(temp.get(j));

						 }
					 }
				 }
				 while (predec.size()!=0) {
					 pda.clear();
					 a = predec.get(0);
					 pda = WSDN_Q.predecesores(a);
					 if (pda.size()!=0) {
						 if (a.getType() == Vertice.TRANSITION) {
							 if (seq.contains(a)) { // a is a Transitions
								 pdatoadd=ret.addPlaces(pda,a); // add all its predecessors and its arcs			
							 } 
							 else pdatoadd.clear();
						 }
						 else { //a is a Place

							 if (!inputQ.contains(a.getName())) {
								 pdatoadd=ret.addTransitions(pda,a,seq); // add all its predecessors and its arcs
							 } 
							 else pdatoadd.clear();

						 }		    
						 predec.addAll(pdatoadd);
					 }
					 predec.remove(a);
				 }
				 return ret;
			 }

			 //   private double getScore(ArrayList<Vertice> sq) {
			 //        double qlt=0.0;
			 //	for (int i=0;i<sq.size();i++) {
			 //	    qlt = qlt + sq.get(i).getQuality();
			 //        }
			 //	return qlt;
			 //   }

			 /**
			  * Get cost in terms of exec. time and price of a sequence of transitions (execution plan) 
			  **/                
			 // private double getCost(ArrayList<Vertice> sq) {
			 //      double qlt=0.0;
			 //	for (int i=0;i<sq.size();i++) {
			 //	    qlt = qlt + sq.get(i).getCost();
			 //      }
			 //return qlt;
			 //}

			 private double getCost(ArrayList<Vertice> sq) {
				 double cost=0.0;
				 for (int i=0;i<sq.size();i++) {
					 cost = cost + sq.get(i).getCost();
				 }
				 return cost;
			 }

			 public QueryResults run() {
				 // java.util.Date t1 = new java.util.Date();
				 ArrayList<Vertice> firables,suc,pred,parallel,trans_succ;
				 boolean reachedquery = false;

				 parallel = new ArrayList<Vertice>(); 
				 WSDN_Q.setInitialMarking(query);
				 firables = WSDN_Q.getFirables();
				 WSDN_Q.setQuality(query); //Quality function in terms of qos parameter, reached outputs, etc.	
				 WSDN_Q.setCost(query); // Cost only in terms of exec. time and price (the real values, not normalized

				 while (firables.size()!=0){
					 Vertice s = getBest(firables);

					 if (!CutOff(s)) {
						 suc = WSDN_Q.sucesores(s);
						 pred = WSDN_Q.predecesoresWithRep(s);
						 fireTrans(s,suc,pred);
						 O_M.addAll(suc);
						 q_result.addService(s);
						 trans_succ = getSucesorsTransactions(suc);
						 parallel.addAll(calculateParallel(firables,trans_succ,s));
						 int n_suc = trans_succ.size();

						 for (int i=0;i<n_suc;i++) {
							 Vertice w=trans_succ.get(0);
							 if (((WSDN_Q.getMarked()==Vertice.ATOMIC) || (WSDN_Q.getMarked()==Vertice.ATOMICRET)) && ((w.isPivotRet()) || (w.isAtomicRet()) || (w.isCompensaRet()))) {
								 if ((!firables.contains(w)) && (WSDN_Q.isFirable(w,WSDN_Q.predecesoresWithRep(w))))  
									 firables.add(w);
							 }
							 else if (((WSDN_Q.getMarked()==Vertice.COMPENSA) || (WSDN_Q.getMarked()==Vertice.COMPENSARET)) && (WSDN_Q.isFirable(w,WSDN_Q.predecesoresWithRep(w))) && (!firables.contains(w)))
								 firables.add(w);
							 trans_succ.remove(w);
						 }
						 int n_paral = parallel.size();

						 for (int i=0;i<n_paral;i++) {
							 Vertice w=parallel.get(0);
							 if ((WSDN_Q.getMarked()==Vertice.ATOMIC) && (!w.isCompensaRet()))
								 firables.remove(w);
							 else if  ((WSDN_Q.getMarked()==Vertice.ATOMICRET) && ((!w.isPivotRet()) && (!w.isAtomicRet()) &&  (!w.isCompensaRet())))
								 firables.remove(w);
							 else if ((WSDN_Q.getMarked()==Vertice.COMPENSA) && ((!w.isCompensa()) && (!w.isCompensaRet())))
								 firables.remove(w);
							 parallel.remove(w);
						 }
					 } // !cutoff
					 firables.remove(s);
					 if (reachedFinal(query)) { 
						 reachedquery=true;
						 break;
					 }
				 } // del while firables

				 //java.util.Date t2 = new java.util.Date();
				 //System.out.println("Automatic_Selection_Time " + (t2.getTime() - t1.getTime()));
				 //double selectionTime = t2.getTime() - t1.getTime();


				 if (reachedquery) {
					 //t1 = new java.util.Date();
					 //q_result.setWSDN(createFinalPlan(q_result.getServices(),query));
					 //t2 = new java.util.Date();
					 //System.out.println("Clear_Time " + (t2.getTime() - t1.getTime()));
					 System.out.println("-------------------------------------");
					 System.out.println("Query Name: " + query.getName());
					 //System.out.println("Automatic_Selection_Time " + selectionTime);
					 System.out.println("FINAL SEQ " + q_result.getServices());

					 System.out.println("SOLUTION  COST " + getCost(q_result.getServices()));
					 System.out.println("TRANSACTIONAL PROPERTY  " + WSDN_Q.getMarked());

					 //Rafael: agregue setTime y setExito
					 //q_result.setTime(selectionTime);
					 q_result.setExito(true);
					 q_result.setWSDN(createFinalPlan(q_result.getServices(), query));
					 q_result.getWSDN().setMarkedColor(WSDN_Q.getMarked());
					 return q_result;
				 } else {
					 System.out.println("-------------------------------------");
					 System.out.println("There is no SOLUTION");
					 //Rafael: agregue setTime para saber cuanto tiempo tardo
					 //q_result.setTime(selectionTime);
					 q_result.clearServices();
					 return q_result;
				 }
			 } // del run
} 
