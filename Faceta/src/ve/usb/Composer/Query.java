/*
 * Query.java
 *
 * Created on 24 de enero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */
package ve.usb.Composer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import nanoxml.XMLElement;
import ve.usb.PetriNets.PetriNet;
import ve.usb.PetriNets.Vertice;

public class Query implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int R0 = 0;
	public static final int R1 = 1;

	protected int len;
	protected int np;
	protected int risk;
	protected ArrayList<Vertice> ins; 
	protected Map<Vertice, Object> inputValues;
	public Map<Vertice, Object> getInputValues() {
		return inputValues;
	}

	public void setInputValues(Map<Vertice, Object> inputValues) {
		this.inputValues = inputValues;
	}


	protected ArrayList<Vertice> outs;
	protected double weights[];

	protected String name;

	protected ArrayList<Vertice> SRq = null; /* transitions satisfying risk */

	protected QueryResults results = null;

	protected ArrayList<Vertice> BestExha = null;



	//------TO KEEP REAL VALUES----------------------
	//protected ArrayList<String> inData[] ;
	//protected ArrayList<String> outData[] ;


	public void setBestPlan(ArrayList<Vertice> seq) {
		BestExha = seq;
	}

	public ArrayList<Vertice>  getBestPlan() {
		return BestExha;
	}

	public void setSRq(ArrayList<Vertice> sr) {
		SRq = sr;
	}

	public ArrayList<Vertice>  getSRq() {
		return SRq;
	}

	/** Creates a new instance of Query */
	public Query(String n) {
		name = n;
	}

	public Query(int l, int n, List<Vertice> ins2, List<Vertice> outs2, int r, double w[]) {
		len = l;
		np = n;
		risk = r;
		ins = new ArrayList<Vertice>(ins2.size());
		outs = new ArrayList<Vertice>(outs2.size());
		weights = new double[w.length];

		//---------TO KEEP REAL VALUES-----WE'll USE ON EXECUTION----------------
		/*        inData =  new ArrayList[i.length];
        outData =  new ArrayList[o.length];

        for (int j = 0; j < i.length; j++) {
            inData[j] = new ArrayList<String>();
        }

        for (int j = 0; j < o.length; j++) {
            outData[j] = new ArrayList<String>();            
        }
		 */        
		//---------------------------------

		ins.addAll(0,ins2);      
		outs.addAll(0,outs2);      
		System.arraycopy(w, 0, weights, 0, w.length);

		results = new QueryResults();
		BestExha = new ArrayList<Vertice>();

	}

	public Query clone() {
		Query q = new Query(new String(this.name));
		q.len = this.len;
		q.np = this.np;
		q.risk = this.risk;

		for (int i=0; i < ins.size();i++) {
			Vertice v=ins.get(i);
			q.ins.add(v.clone());
		}

		for (int i=0; i < outs.size();i++) {
			Vertice v=outs.get(i);
			q.outs.add(v.clone());
		}

		System.arraycopy(weights, 0, q.weights, 0,weights.length);


		for (int i=0; i < SRq.size();i++) {
			Vertice v=SRq.get(i);
			q.SRq.add(v.clone());
		}

		q.results = this.results.clone();

		q.BestExha = (ArrayList<Vertice>) this.BestExha.clone();

		return q;
	}

	public void setName(String n) {
		name = n;
	}

	public String getName() {
		return name;
	}

	public void setRisk(int r) {
		risk = r;
	}

	public int getRisk() {
		return risk;
	}

	public ArrayList<Vertice> getIns() {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();

		for (int i=0;i<ins.size();i++) {
			ret.add(ins.get(i));
		}  
		return ret;   
	}


	public ArrayList<String> getInsName() {
		ArrayList<String> ret = new ArrayList<String>();

		for (int i=0;i<ins.size();i++) {
			ret.add(ins.get(i).getName());
		}  
		return ret;   
	}

	public ArrayList<Vertice> getOuts() {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();


		for (int i=0;i<outs.size();i++) {
			ret.add(outs.get(i));
		}  
		return ret;   
	}



	public double [] getWeights() {
		double ret[]= new double[weights.length];
		System.arraycopy(weights, 0, ret, 0, weights.length);
		return ret;
	}


	public ArrayList<String> getNameOuts() {
		ArrayList<String> ret=new ArrayList<String>(outs.size());

		for (int i=0;i<outs.size();i++)
			ret.add(outs.get(i).getName());
		return ret;
	}

	public ArrayList<String> getNameIns() {
		ArrayList<String> ret=new ArrayList<String>(ins.size());

		for (int i=0;i<ins.size();i++)
			ret.add(ins.get(i).getName());
		return ret;
	}

	public int getLen() {
		return len;
	}

	public int getNP() {
		return np;
	}

	public void setNP(int n) {
		np = n;
	}

	public boolean equals(Query q) {
		if ((ins.size() != q.ins.size()) || (outs.size() != q.outs.size()))
			return false;

		for(int i = 0; i < ins.size(); i++) {
			if (ins.get(i).getName().compareTo(q.ins.get(i).getName()) != 0)
				return false;
		}

		for(int i = 0; i < outs.size(); i++) {
			if (outs.get(i).getName().compareTo(q.outs.get(i).getName()) != 0)
				return false;
		}

		return true;
	}

	public String toString2() {
		String ret = this.name + " INS ";

		for(int i = 0; i < ins.size(); i++) {
			ret += ins.get(i) + ", ";
		}
		ret += "OUTS ";
		for(int i = 0; i < outs.size(); i++) {
			ret += outs.get(i).getName() + ", ";
		}
		ret +="RISK = " + risk + " ";
		String len = " NP="+ np;
		return len + ret;
	}


	public void setResults(QueryResults r) {
		results.setWSDN(r.getWSDN());
		results.setServices(r.getServices());
		//Rafael: agregue setTime y setExito
		results.setTime(r.getTime());
		results.setExito(r.getExito());
	}

	public QueryResults getResults() {
		return results;
	}


	private void setInputs(Vector<XMLElement> inputs) {
		int i = 0;
		for (XMLElement ele : inputs) {          
			ins.add(i,new Vertice(ele.getStringAttribute("type"),Vertice.PLACE));
			i++;
		}

		/* THIS IS TO MANAGE VALUES...SEE WHEN WE'RE EXECUTING 
        ins = ve.usb.utils.utils.sort(ins);
        for (XMLElement ele : inputs) {
            String type = ele.getStringAttribute("type");
            //System.out.printf("TYPE %s\n", type);

            Vector<XMLElement> values = ele.getChildren();
            for (XMLElement v: values){
                String val = v.getStringAttribute("val");
                //System.out.printf("IN val %s en \n%s\n", val, v.toString().replace(">", ">\n"));

                this.addInputValue(type, val);
            }
        }
		 */
	}

	private void setOutputs(Vector<XMLElement> outputs) {
		int i = 0;
		for (XMLElement ele : outputs) {

			outs.add(i,new Vertice(ele.getStringAttribute("type"),Vertice.PLACE));
			i++;
		}

		/* THIS IS TO MANAGE VALUES...SEE WHEN WE'RE EXECUTING
        outs = ve.usb.utils.utils.sort(outs);
        for (XMLElement ele : outputs) {
            String type = ele.getStringAttribute("type");
            //System.out.printf("TYPE %s\n", type);

            Vector<XMLElement> values = ele.getChildren();
            for (XMLElement v: values){
                String val = v.getStringAttribute("val");
                //System.out.printf("IN val %s en \n%s\n", val, v.toString().replace(">", ">\n"));

                this.addOutputValue(type, val);
            }
        }
		 */
	}

	public XMLElement toXml() {
		XMLElement e = new XMLElement();

		e.setName("Query");
		e.setIntAttribute("len", len);
		e.setIntAttribute("np", np);

		if (name != null)
			e.setAttribute("name", name);
		else
			e.setAttribute("name", "noname");

		e.addChild(getInputs());
		e.addChild(getOutputs());

		return e;
	}

	protected XMLElement getInputs() {
		XMLElement e = new XMLElement();

		e.setName("Inputs");
		e.setIntAttribute("Num", ins.size());

		for (int i = 0; i < ins.size(); i++) {
			XMLElement ec = new XMLElement();

			ec.setName("Input");
			ec.setAttribute("type", ins.get(i));

			e.addChild(ec);
		}

		return e;
	}

	protected XMLElement getOutputs() {
		XMLElement e = new XMLElement();

		e.setName("Outputs");
		e.setIntAttribute("Num", outs.size());

		for (int i = 0; i < outs.size(); i++) {
			XMLElement ec = new XMLElement();

			ec.setName("Output");
			ec.setAttribute("type", outs.get(i));
			e.addChild(ec);
		}

		return e;
	}      

	public String toString() {
		return toXml().toString().replaceAll(">", ">\n");
	}


	// These Methods are to manege values 


	/*    public void addInputValue(String t, String instance) {
        for (int i = 0; i < ins.length; i++) {
            if (t.equalsIgnoreCase(ins[i])) {
                inData[i].add(instance);
                break;
            } 
        }
    }

    public void addOutputValue(String t, String instance) {
        for (int i = 0; i < ins.length; i++) {
            if (t.equalsIgnoreCase(outs[i])) {
                outData[i].add(instance);
                break;
            }
        }
    }

    public ArrayList<String> getInputData(String t) {
        ArrayList<String> ret = null;

        for (int i = 0; i < ins.length; i++) {
            if (ins[i].equalsIgnoreCase(t) ) {
                for (int j = 0; j < inData.length; j++) {
                    ret.add(inData[i].get(j));
                }                        
                break;
            }
        }

        return ret;        
    }

    public ArrayList<String> getOutputData(String t) {
        ArrayList<String> ret = null;

        for (int i = 0; i < outs.length; i++) {
            if (outs[i].equalsIgnoreCase(t) ) {
                for (int j = 0; j < outData.length; j++) {
                    ret.add(outData[i].get(j));
                }                        
                break;
            }
        }

        return ret;        
    }
	 */

	/* 
	 * Implementation of Algo 2: Validation of Q 
	 */

	public boolean validate(PetriNet wsdn) {
		boolean inRet = false;
		boolean outRet = false;
		ArrayList<String> outsTemp, outsuc, inpred;
		SRq=wsdn.getServicesbyRisk(risk); //Obtain services that satisfy RISK

		if (SRq.size() == 0)
			return inRet; /* There is not services that satisfy the RISK, Q is not valid */
		else {
			outsTemp = new ArrayList<String>(outs.size());
			outsuc = new ArrayList<String>();
			outsTemp=getNameOuts();
			for (int i=0; i < SRq.size(); i++) {
				if (!outRet) {//if we do not have all outputs yet
					outsuc.clear();
					outsuc = wsdn.sucesores(SRq.get(i).getName());	
					for (int j=0;j<outsuc.size();j++) {
						if (outsTemp.contains(outsuc.get(j))) {
							outsTemp.remove(outsuc.get(j));
						}
					}
					if (outsTemp.size()==0) {
						outRet = true; 
					}
				}
				if (!inRet) {//if we do not have al least one input yet
					inpred = null;
					inpred = wsdn.predecesores(SRq.get(i).getName());
					int allin = inpred.size();
					for (int j=0;(j<ins.size() && !inRet);j++) {
						if (inpred.contains(ins.get(j).getName())) allin--;
						if (allin ==0) inRet = true;
					}
				}	
				if (inRet && outRet) break; 
			}
		}
		return (inRet && outRet);
	}

}
