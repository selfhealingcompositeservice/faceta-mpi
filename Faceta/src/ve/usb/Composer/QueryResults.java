/*
 * QueryResults.java
 *
 * Created on 24 de enero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.Composer;

import java.util.ArrayList;

import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;

public class QueryResults implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double time = 0;
    private double cost = 0;
    private boolean exito = false;

    private double numServices = 0;
    private MarkedPN wsdn_q;
    private ArrayList<Vertice> services;

    //Used only by Exhaustive to keep more best solutions
    private int numBestSoluc = 0;
    private ArrayList<ArrayList<Vertice>> services_listofbests;
    private ArrayList<Double> costs_listofbest;
         
    /** Creates a new instance of QueryResults */
    public QueryResults() {
		wsdn_q = new MarkedPN("Solution");
		services = new ArrayList<Vertice>();
    }

    public QueryResults(int nbests) {
		wsdn_q = new MarkedPN("Solution");
		services = new ArrayList<Vertice>();
		numBestSoluc = nbests;	
	    services_listofbests = new ArrayList<ArrayList<Vertice>>();
		costs_listofbest = new  ArrayList<Double>();
    }
       
    public QueryResults(double ns, double tim, double cos, boolean ex, int np) {
        time = tim;
        numServices = ns;
        cost = cos;
        exito = ex;
        numBestSoluc = np;
		wsdn_q = new MarkedPN("Solution");
		services = new ArrayList<Vertice>();
    }
    
    public QueryResults clone() {
    	QueryResults qr = new QueryResults(numServices,time,cost,exito,numBestSoluc);
        qr.wsdn_q = wsdn_q.clone();
        for (int i=0; i < services.size();i++) {
            Vertice v=services.get(i);
            qr.services.add(v.clone());
        }
        return qr;
    }

    public MarkedPN getWSDN() {
    	MarkedPN ret;
        ret=wsdn_q;
        return ret;	
    }

    public void setWSDN(MarkedPN pn) {
    	wsdn_q = pn;
    	services = pn.getTransitions();
    }

    public int getNumBestSoluc() {
        return numBestSoluc;
    }
    
    public double getTime() {
        return time;
    }
    
    public double getNumServices() {
        return numServices;
    }
      
    public double getCost() {
        return cost;
    }
    
    public boolean getExito() {
        return exito;
    }
    
    public void setTime(double t) {
        time = t;
    }
    
    public void setNumServices(double ns) {
        numServices = ns;
    }
    
    
    public void setCost(double c) {
        cost = c;
    }
    
    public void setExito(boolean e) {
        exito = e;
    }

    public void setNumBestSoluc(int nb) {
        numBestSoluc = nb;
    }

 //   public void incNumBestSoluc() {
 //       numBestSoluc++;
 //   }

    public void clearServices() {
        services.clear();
        numServices =0;
    }

    public void addService(Vertice s) {
        services.add(s);
        numServices++;
    }
    
    public ArrayList<Vertice> getServices() {
    ArrayList<Vertice> ret = new ArrayList<Vertice>();
 
      
	for (int i=0;i<services.size();i++)
	    ret.add(services.get(i));
        return ret;        
    }

    public void setServices(ArrayList<Vertice> se) {
    	services = se;
    }

    public ArrayList<ArrayList<Vertice>>  getListBest() {
    	return services_listofbests;
    }

    public ArrayList<Double>  getCostListBest() {
    	return costs_listofbest;
    }

    public void addSoluc(ArrayList<Vertice> seq, double cost) {
    	services_listofbests.add(seq);
        costs_listofbest.add(cost);	
    }

    public void deleteLastSoluc() {
    	services_listofbests.remove(numBestSoluc-1);
        costs_listofbest.remove(numBestSoluc-1);	
    }

    public void insertSoluc(ArrayList<Vertice> seq, double cost) {
        boolean inserted = false;
        for (int i=0;i< numBestSoluc-1; i++) {
		    if (costs_listofbest.get(i) > cost) {
		    	services_listofbests.add(i,seq);
	        	costs_listofbest.add(i,cost);
	        	inserted = true;
	            break;
		    }
        }

        if (!inserted) {
        	addSoluc(seq,cost);
        	//	    services_listofbests.add(i-1,seq);
        	//            	costs_listofbest.add(i-1,cost);	
        }
    }

}
