/*
 * Current.java
 *
 * Created on 15 de febrero de 2010, 05:33 PM
 *
 */

/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.Exhaustive;

import java.util.ArrayList;

import ve.usb.PetriNets.Vertice;

public class Current {

	private ArrayList<Vertice> wsdn_places;
	private int wsdn_color;
	private ArrayList<Vertice> firables;
	private ArrayList<Vertice> q_result;
	private ArrayList<Vertice> O_M;

	public Current() {
		wsdn_places = null;
		firables = null;
		q_result = null;
	}

	public Current(ArrayList<Vertice> myMPN_places, int color, ArrayList<Vertice> firab , ArrayList<Vertice> qr, ArrayList<Vertice> om) {
		wsdn_places = myMPN_places;
		wsdn_color = color;
		firables = firab;
		q_result = qr;
		O_M = om;
	}

	public ArrayList<Vertice> getPlaces() {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();
		for (int i=0;i<wsdn_places.size();i++)
			ret.add(wsdn_places.get(i).clone());	
		return ret;
	}

	public int getColor() {
		return wsdn_color;
	}

	public ArrayList<Vertice> getFirables() {
		return firables;
	}

	public ArrayList<Vertice> getQResult() {
		return q_result;
	}

	public ArrayList<Vertice> getOM() {
		return O_M;
	}

	public void  setPlaces(ArrayList<Vertice> ws_p) {
		wsdn_places=ws_p;
	}

	public void  setColor(int ws_c) {
		wsdn_color=ws_c;
	}

	public void setFirables(ArrayList<Vertice> fir) {
		firables=fir;
	}

	public void setQResult(ArrayList<Vertice> qr) {
		q_result=qr;
	}

	public void setOM(ArrayList<Vertice> om) {
		O_M=om;
	}

}
