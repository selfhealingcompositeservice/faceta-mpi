/*
 * Exhaustive.java
 *
 * Created on 3 de febrero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.Exhaustive;

import java.util.ArrayList;
import java.util.Stack;

import ve.usb.Composer.Query;
import ve.usb.Composer.QueryResults;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;

public class Exhaustive {

	private MarkedPN WSDN_Q;
	private Query query;
	private ArrayList<Vertice> O_M;
	private QueryResults q_result;

	public Exhaustive(MarkedPN myMPN, Query myq, int howmanybest) {
		WSDN_Q = new MarkedPN(myMPN);
		query = myq; 
		q_result = new QueryResults(howmanybest);
		O_M = new ArrayList<Vertice>();
	}


	private double getCost(ArrayList<Vertice> sq) {
		double cost=0.0;
		for (int i=0;i<sq.size();i++) {
			cost = cost + sq.get(i).getCost();
		}
		return cost;
	}

	private void fireTrans(Vertice tr, ArrayList<Vertice> suc, ArrayList<Vertice> pred) {
		addTokensToOutputs(tr,suc,pred);
		removeTokensFromInputs(pred);
		updateColor(tr);
	}

	private void addTokensToOutputs(Vertice tr, ArrayList<Vertice> suc, ArrayList<Vertice> pred) {
		boolean go=true;	

		for (int i=0;i<pred.size();i++) {
			if (pred.get(i).isAtomic()) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomic();
					}
				}
				go=false;
				break;
			} // if atomic
			else if (pred.get(i).isAtomicRet()) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomicRet();
					}
				}
				go=false;
				break;
			} // is atomic ret
			else if ((pred.get(i).isCompensa()) && ((tr.isPivot()) || (tr.isPivotRet()) || (tr.isAtomic()) || (tr.isAtomicRet()))) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomic();
					}
				}

				go=false;
				break;
			} //is compensa
			else if ((pred.get(i).isCompensa()) && ((tr.isCompensa()) || (tr.isCompensaRet()))) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setCompensa();
					}
				}

				go=false;
				break;
			} // is compensa 2
		}

		if (go) {
			if ((tr.isCompensa()) || (tr.isCompensaRet()) || (tr.isAtomic()) || (tr.isAtomicRet())) {
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setColor(tr.getColor().get(0));
					}
				}
			}
			else if (tr.isPivot()) { 
				for (int j=0;j<suc.size();j++) {
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomic();
					}
				}
			}
			else if (tr.isPivotRet()) {
				for (int j=0;j<suc.size();j++){
					int rep = WSDN_Q.getLastReplica(suc.get(j));
					for (int z=0;z<=rep;z++) {
						WSDN_Q.getPlace(suc.get(j).getName(),z).setAtomicRet();
					}
				}
			}
		}
	} 

	private void removeTokensFromInputs(ArrayList<Vertice> pred) {
		for (int i=0;i<pred.size();i++) {
			if (!query.getOuts().contains(pred.get(i))){
				WSDN_Q.getPlace(pred.get(i).getName(),pred.get(i).getReplica()).removeColors();
			}
		}
	}

	private void updateColor(Vertice tr) {
		if (((WSDN_Q.getMarked() == Vertice.INITIAL) ||  (WSDN_Q.getMarked() == Vertice.COMPENSARET)) && (tr.isPivot())) 
			WSDN_Q.setMarkedColor(Vertice.ATOMIC);
		else if (((WSDN_Q.getMarked() == Vertice.INITIAL) ||  (WSDN_Q.getMarked() == Vertice.COMPENSARET)) && (tr.isPivotRet())) 
			WSDN_Q.setMarkedColor(Vertice.ATOMICRET); 	
		else if (((WSDN_Q.getMarked() == Vertice.INITIAL) ||  (WSDN_Q.getMarked() == Vertice.COMPENSARET)) && (tr.isAtomic() || tr.isAtomicRet() || tr.isCompensa() || tr.isCompensaRet() )) 
			WSDN_Q.setMarkedColor(tr.getColor().get(0)); 	
		else if ((WSDN_Q.getMarked() == Vertice.COMPENSA) && (tr.isAtomic() || tr.isAtomicRet() || tr.isPivot() || tr.isPivotRet() )) 
			WSDN_Q.setMarkedColor(Vertice.ATOMIC); 	
	}

	private ArrayList<Vertice>  getSucesorsTransactions(ArrayList<Vertice> suc) {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();
		for (int i=0;i< suc.size();i++) {
			ret.addAll(WSDN_Q.sucesores(suc.get(i)));
		} 
		return ret;
	}

	private ArrayList<Vertice> calculateParallel(ArrayList<Vertice> firab, ArrayList<Vertice> t_suc, Vertice s) {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();
		for (int i=0;i< firab.size();i++) {
			if (t_suc.contains(firab.get(i)) || (firab.get(i).getName().compareTo(s.getName())==0)) ;
			else ret.add(firab.get(i));
		}
		return ret;
	}

	private boolean CutOff(Vertice tr) {
		boolean ret = true;
		ArrayList<Vertice> suc = WSDN_Q.sucesores(tr);
		for (int i=0;i<suc.size();i++) {
			if (!O_M.contains(suc.get(i))) {
				ret=false;
				break;
			}
		}
		return ret;
	}

	private boolean reachedFinal(Query q) {
		boolean ret=false;//true; 
		ArrayList<Vertice> q_outputs = q.getOuts();
		int m=q_outputs.size();
		ArrayList<Vertice> wsdn_outputs = WSDN_Q.getOutputs();
		for (int i= 0; i <q_outputs.size(); i++) {
			for (int j=0;j<wsdn_outputs.size();j++) {
				Vertice o = wsdn_outputs.get(j);		
				if (o.getName().compareTo(q_outputs.get(i).getName())==0) {
					if (o.getColor().size() != 0) {
						if (q.getRisk() == Query.R0) {
							if (o.isCompensa() || o.isCompensaRet()) { 
								m--;
								break;				
							}
						}
						else if (o.isCompensa() || o.isCompensaRet() || o.isAtomic() || o.isAtomicRet()) {    
							m--;
							break;
						}
					}
				}
			}
		}
		if (m==0) ret = true;
		return ret;
	}



	private void insertinListBest(ArrayList<Vertice> seq, double c) {

		if ( (q_result.getCostListBest().size() == 0) || ( (q_result.getCostListBest().get(q_result.getCostListBest().size()-1) < c) && ( q_result.getCostListBest().size() < q_result.getNumBestSoluc() ) ) ) {//OJO AQUI SERA get(0) o get(size) dependienpo de como se ordenan 
			//This Solution is added at the end of the list
			q_result.addSoluc(seq,c);
		}
		else if ( (q_result.getCostListBest().get(q_result.getCostListBest().size()-1) > c) && (q_result.getCostListBest().size() < q_result.getNumBestSoluc() ) )    {
			q_result.insertSoluc(seq,c);
		}
		else if ( (q_result.getCostListBest().get(q_result.getCostListBest().size()-1) > c) && (q_result.getCostListBest().size() == q_result.getNumBestSoluc() ) ) { 
			//OJO AQUI SERA get(0) o get(size) dependienpo de como se ordenan 
			q_result.deleteLastSoluc(); //Descart the last one to insert the new one
			q_result.insertSoluc(seq,c);
		} 
	}


	public ArrayList<Vertice> run() {
		java.util.Date t1 = new java.util.Date();
		ArrayList<Vertice> firables,suc,pred,parallel,trans_succ;
		Stack<ArrayList<Vertice>> stackMPN_places = new Stack<ArrayList<Vertice>>();
		Stack<Integer> stackMPN_color = new Stack<Integer>();
		Stack<ArrayList<Vertice>> stackfirab = new Stack<ArrayList<Vertice>>();
		Stack<ArrayList<Vertice>> stackq_result = new Stack<ArrayList<Vertice>>();
		Stack<ArrayList<Vertice>> stackO_M = new Stack<ArrayList<Vertice>>();

		ArrayList<Vertice> BestSeq = new ArrayList<Vertice>();
		int ColorBest=0;
		double CostBest=0.0;
		int howmanySolut = 0;
		int longestSolut = 0;
		boolean reachedquery = false;	

		parallel = new ArrayList<Vertice>(); 
		WSDN_Q.setInitialMarking(query);
		firables = WSDN_Q.getFirables();
		WSDN_Q.setQuality(query);
		WSDN_Q.setCost(query);
		do {
			while (firables.size()!=0){
				Vertice s = firables.get(0);
				firables.remove(s);
				Current ActualWSDN = new Current(WSDN_Q.getPlaces(),WSDN_Q.getMarked(),firables,q_result.getServices(),O_M);
				if (firables.size()!=0) {

					stackMPN_places.push(new ArrayList<Vertice>(ActualWSDN.getPlaces()));
					stackMPN_color.push(new Integer(WSDN_Q.getMarked()));
					stackfirab.push(new ArrayList<Vertice>(ActualWSDN.getFirables()));
					stackq_result.push(new ArrayList<Vertice>(ActualWSDN.getQResult()));
					stackO_M.push(new ArrayList<Vertice>(ActualWSDN.getOM()));

				}
				firables.clear();		

				if (!CutOff(s)) {
					suc = WSDN_Q.sucesores(s);
					pred = WSDN_Q.predecesoresWithRep(s);

					if (!q_result.getServices().contains(s)) {		   
						fireTrans(s,suc,pred);
						O_M.addAll(suc);	  
						q_result.addService(s);

						trans_succ = getSucesorsTransactions(suc);
						parallel.addAll(calculateParallel(firables,trans_succ,s));
						int n_suc = trans_succ.size();
						for (int i=0;i<n_suc;i++) {
							Vertice w=trans_succ.get(0);
							if (((WSDN_Q.getMarked()==Vertice.ATOMIC) || (WSDN_Q.getMarked()==Vertice.ATOMICRET)) && ((w.isPivotRet()) || (w.isAtomicRet()) || (w.isCompensaRet()))) {
								if ((!firables.contains(w)) && (WSDN_Q.isFirable(w,WSDN_Q.predecesoresWithRep(w))))  
									firables.add(w);
							}
							else if (((WSDN_Q.getMarked()==Vertice.COMPENSA) || (WSDN_Q.getMarked()==Vertice.COMPENSARET)) && (WSDN_Q.isFirable(w,WSDN_Q.predecesoresWithRep(w))) && (!firables.contains(w)))
								firables.add(w);
							trans_succ.remove(w);
						}
						int n_paral=parallel.size();
						for (int i=0;i<n_paral;i++) {
							Vertice w=parallel.get(0);
							if ((WSDN_Q.getMarked()==Vertice.ATOMIC) && (!w.isCompensaRet()))
								firables.remove(w);
							else if  ((WSDN_Q.getMarked()==Vertice.ATOMICRET) && ((!w.isPivotRet()) && (!w.isAtomicRet()) &&  (!w.isCompensaRet())))
								firables.remove(w);
							else if ((WSDN_Q.getMarked()==Vertice.COMPENSA) && ((!w.isCompensa()) && (!w.isCompensaRet())))
								firables.remove(w);
							parallel.remove(w);
						}
					}
				} // !cutoff

				if (reachedFinal(query)) {
					howmanySolut++;
					ArrayList<Vertice> temseq=q_result.getServices();
					if (longestSolut < temseq.size()) longestSolut = temseq.size();
					if (BestSeq.size() != 0) {
						double temcost = getCost(temseq);

						if (temcost < CostBest){
							if (q_result.getNumBestSoluc() > 1) {
								insertinListBest(BestSeq,CostBest);
							}
							BestSeq = temseq;
							ColorBest= WSDN_Q.getMarked();
							CostBest=temcost;
						}
						else if (q_result.getNumBestSoluc() > 1) {
							insertinListBest(temseq,temcost);
						}
					}
					else {
						BestSeq = q_result.getServices();  	
						ColorBest= WSDN_Q.getMarked();
						CostBest=getCost(BestSeq);
						longestSolut = BestSeq.size();
					}
					if (stackfirab.size()!=0) {
						//Current ActualWSDN = new Current(WSDN_Q.getPlaces(),WSDN_Q.getMarked(),firables,q_result.getServices(),O_M);
						ActualWSDN.setPlaces((ArrayList<Vertice>) stackMPN_places.pop());
						ActualWSDN.setColor((Integer) stackMPN_color.pop());
						ActualWSDN.setFirables((ArrayList<Vertice>) stackfirab.pop());
						ActualWSDN.setQResult((ArrayList<Vertice>) stackq_result.pop());
						ActualWSDN.setOM((ArrayList<Vertice>) stackO_M.pop());
						WSDN_Q.setPlaces(ActualWSDN.getPlaces());
						WSDN_Q.setMarkedColor(ActualWSDN.getColor());
						firables = ActualWSDN.getFirables();
						q_result.setServices(ActualWSDN.getQResult());
						O_M = ActualWSDN.getOM();
					}
				} // if reached
				else if (firables.size()==0) {
					if (stackfirab.size()!=0) {
						ActualWSDN.setPlaces((ArrayList<Vertice>) stackMPN_places.pop());
						ActualWSDN.setColor((Integer) stackMPN_color.pop());
						ActualWSDN.setFirables((ArrayList<Vertice>) stackfirab.pop());
						ActualWSDN.setQResult((ArrayList<Vertice>) stackq_result.pop());
						ActualWSDN.setOM((ArrayList<Vertice>) stackO_M.pop());
						WSDN_Q.setPlaces(ActualWSDN.getPlaces());
						WSDN_Q.setMarkedColor(ActualWSDN.getColor());
						firables = ActualWSDN.getFirables();
						q_result.setServices(ActualWSDN.getQResult());
						O_M = ActualWSDN.getOM();
					}

				} // else reached
			} // del while firables
		} while (stackfirab.size()!=0);  // while stack
		
		System.out.println("-------------------------------------");
	    System.out.println("Query Name: " + query.getName());
		java.util.Date t2 = new java.util.Date();
		
		System.out.println("Automatic_Selection_Time " + (t2.getTime() - t1.getTime()));
		if (BestSeq.size() != 0) {
			System.out.println("BEST FINAL SEQ " + BestSeq);
			System.out.println("COST SOLUTION  " + CostBest);
			System.out.println("TRANSACTIONAL PROPERTY  " + ColorBest);
			System.out.println("How Many Total Solutions?  " + howmanySolut);
			System.out.println("LONGEST SOLUTION?  " + longestSolut);
			System.out.println("The Best " + q_result.getNumBestSoluc() +  " SOLUTIONS:");
			int maxsoluc=0;
			if (howmanySolut <= q_result.getNumBestSoluc())  maxsoluc = howmanySolut-1;
			else maxsoluc = q_result.getNumBestSoluc();
			for (int b=0; b < maxsoluc;b++) {
				System.out.println("Solution " + b + " " +  q_result.getListBest().get(b) + " COST: " + q_result.getCostListBest().get(b)
						+ " NP: " + q_result.getListBest().get(b).size());
			}
		}
		else System.out.println("EXHAUSTIVE: There is not SOLUTION");
		return BestSeq;
	} // del run
} 
