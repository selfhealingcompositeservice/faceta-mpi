package ve.usb.KnowledgedeBase;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComplexElement {
	
	private String name;
	private Map<String, URI> attributes = new HashMap<String, URI>();
	private List<ComplexElement> complexElements = new ArrayList<ComplexElement>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, URI> getAttributes() {
		return attributes;
	}
	public void setAttributes(Map<String, URI> attributes) {
		this.attributes = attributes;
	}
	public List<ComplexElement> getComplexElements() {
		return complexElements;
	}
	public void setComplexElements(List<ComplexElement> complexElements) {
		this.complexElements = complexElements;
	}
	
	public void addAttribute(String attribute) {
		this.attributes.put(attribute, null);
	}
	
	public void addComplexElement(ComplexElement complexElement) {
		this.complexElements.add(complexElement);
	}
	@Override
	public String toString() {
		String c = "*** Complex Element *** \n";
		c+= "name: " + this.name + "\n";
		for(String s:attributes.keySet())
			c+= "- attribute: " + s + "\n";
		
		for(ComplexElement e:complexElements)
			c+= e.toString();
		
		return c;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof ComplexElement))
			return false;
		
		ComplexElement ce = (ComplexElement)obj;
		
		for(URI a:this.attributes.values()) {
			boolean in = false;
			for(URI a2:ce.getAttributes().values()) {
				if(a.equals(a2))
					in = true;
			}
			if(!in)
				return false;
		}
		
		for(ComplexElement ce1:this.complexElements) {
			for(ComplexElement ce2:ce.getComplexElements())
				if(!ce2.equals(ce1))
					return false;
		}
		
		return super.equals(obj);
	}
	public void addAttribute(String attribute, URI uri) {
		this.attributes.put(attribute, uri);
		
	}
	
	
	
	

	
}
