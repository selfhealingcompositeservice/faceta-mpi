package ve.usb.KnowledgedeBase;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.mindswap.owl.OWLFactory;
import org.mindswap.owl.OWLIndividualList;
import org.mindswap.owl.OWLKnowledgeBase;
import org.mindswap.owls.process.variable.Input;
import org.mindswap.owls.process.variable.Output;
import org.mindswap.owls.service.Service;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.test.data.TestURIs;
 

//tomado del codigo de Eduardo
public class KnowledegeBase {
	
	private org.mindswap.owl.OWLKnowledgeBase kb;
	private OWLIndividualList<Service> services;
	private String reasoner = "Pellet";
	private List<Query> queries = new ArrayList<Query>();
	private java.util.Hashtable<String,ProcessStatistic> processesStatistics = null;
	
	public java.util.Hashtable<String, ProcessStatistic> getProcessesStatistics() {
		return processesStatistics;
	}


	public KnowledegeBase(URI baseUri, int riskFromFile) {
		try {
			kb = OWLFactory.createKB();

			kb.setReasoner(reasoner);
			
			final OWLKnowledgeBase kb = OWLFactory.createKB();
			services = kb.readAllServices(appendURI(baseUri, TestURIs.SERVICES));
			
			
			loadQueries(kb, baseUri, riskFromFile);
			
			System.out.println("Number of queries: " + queries.size());
			
			loadProcessesStatistics(baseUri);
			
		} catch(IOException e) {
			System.out.println("Error reading Services");
			e.printStackTrace();
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}
	}
	
	public void remove() {
		synchronized(services) { 
		for(Service v:services)
			if(!v.getName().contains("REPLICA-0"))
				services.remove(v);
		}
	}
	
	 public void loadQueries(OWLKnowledgeBase kb, URI baseUri, int riskFromFile) throws URISyntaxException{
		 
		 	kb.setAutoConsistency(true);  
		 
	        URI concepts = appendURI(baseUri, TestURIs.CONCEPTS);
	        
	        try {
	            kb.read(appendURI(baseUri, TestURIs.QUERIES));
	            
	            org.mindswap.owl.OWLClass c = kb.getClass(new URI(concepts.toString() + "#Query"));

	            System.out.println("el uri de query es: " + new URI(concepts.toString() + "#Query"));
	            
	            //System.out.printf("\n\n-------------------------Nombre de query: %s %s\n\n", concepts , c);

	            OWLIndividualList 	il = kb.getInstances(c, false) ;
	            
	            org.mindswap.owl.OWLObjectProperty pi = (org.mindswap.owl.OWLObjectProperty) kb.getObjectProperty(new URI(concepts.toString() +"#Qin"));
	            org.mindswap.owl.OWLObjectProperty po = (org.mindswap.owl.OWLObjectProperty) kb.getObjectProperty(new URI(concepts.toString()+"#Qout"));
	            org.mindswap.owl.OWLDataProperty   pl = (org.mindswap.owl.OWLDataProperty) kb.getDataProperty(new URI(concepts.toString() +"#Qlen"));
	            org.mindswap.owl.OWLDataProperty   pn = (org.mindswap.owl.OWLDataProperty) kb.getDataProperty(new URI(concepts.toString()+"#QNP"));
	            org.mindswap.owl.OWLDataProperty   Rq = (org.mindswap.owl.OWLDataProperty) kb.getDataProperty(new URI(concepts.toString()+"#QRq"));
	            org.mindswap.owl.OWLDataProperty   Wq = (org.mindswap.owl.OWLDataProperty) kb.getDataProperty(new URI(concepts.toString()+"#QWq"));
	            
	            int ql = 0;
	            int q2 = 0;
	            for (org.mindswap.owl.OWLIndividual indi : (List<org.mindswap.owl.OWLIndividual>) il) {
	               // System.out.println("INDI: " + indi.getURI());
	                //q = new mio.Composser.Query(indi.toString());
	                ArrayList<Vertice> ins = new ArrayList<Vertice>();
	                ArrayList<Vertice> outs = new ArrayList<Vertice>();
	                
	                org.mindswap.owl.OWLIndividualList lin = indi.getProperties(pi);
	                org.mindswap.owl.OWLIndividualList lout = indi.getProperties(po); 
	                org.mindswap.owl.OWLDataValue 	llen = indi.getProperty(pl);
	                org.mindswap.owl.OWLDataValue 	lnp = indi.getProperty(pn);
	                org.mindswap.owl.OWLDataValue 	lRq = null;
	                
	                
	                
	                if(Rq != null)
	                	lRq = indi.getProperty(Rq);
	                org.mindswap.owl.OWLDataValue 	lWq = null;
	                if(Wq !=null)
	                	lWq = indi.getProperty(Wq);
	 
	                try {
	                    ql = ((Integer) llen.getValue()).intValue();
	                } catch (Exception e) {}
	                try {
	                    q2 = ((Integer) lnp.getValue()).intValue();
	                } catch (Exception e) {}
	                Vertice vertice;
	                //Saca los Ins del query
	                for (org.mindswap.owl.OWLIndividual v : (List<org.mindswap.owl.OWLIndividual>) lin) {
	                    //q.addIn(v.getURI().toString());
	                	 vertice = new Vertice(v.getURI().getFragment(),Vertice.PLACE);
	                     ins.add(vertice);
	                }

	                //Saca los Outs del query
	                for (org.mindswap.owl.OWLIndividual v : (List<org.mindswap.owl.OWLIndividual>) lout) {
//	                    q.addOut(v.getURI().toString());
	                	vertice = new Vertice(v.getURI().getFragment(),Vertice.PLACE);
	                    outs.add(vertice);
	                }
	                
	                //TODO escribo todos los pesos
	                double weights[] = {1};
	                
	                int risk=0;
	                if(lRq != null)
	                	risk = ((Integer) lRq.getValue()).intValue();
	                
	                //for (org.mindswap.owl.OWLIndividual v : (List<org.mindswap.owl.OWLIndividual>) lWq) {
	                	//System.out.println(v);
	                //}
	                int np = new Integer(lnp.getValue().toString()).intValue();
	                int qlen = new Integer(llen.getValue().toString()).intValue();
	                
	                //setting the risk from the configuration file
	                risk = riskFromFile;
	                ve.usb.Composer.Query myq = new ve.usb.Composer.Query(qlen, np, ins, outs,risk,weights);
	               
	                myq.setName(indi.getLocalName());

	                //System.out.println("Setting Query name: " + indi.toString());
	                // Inserta los queries ordenadamente
	                int i=0;
	                //System.out.println("Queries size: " + queries.size());
	               for (i=0; i < queries.size(); i ++) {

	                    if (queries.get(i).getName().compareToIgnoreCase(myq.getName()) > 0) 
	                        break;
	                }
	                
	                queries.add(i, myq);
	               
	            }
	        } catch (Exception e) {
	            System.out.println("Error cargando los queries: " + e);
	            e.printStackTrace();
	        }

	    }
	 
	public OWLIndividualList<Service> getServices() {
		return services;
	}
	
	public Service getServiceByName(String name) {
		
		for(Service s:services)
			if(s.getName().equals(name))
				return s;
		
		return null;
	}
	
	public List<Query> getQueries() {
		return queries;
	}
	
	private URI appendURI(URI uri1, URI uri2) {
		
		try {
			System.out.println(uri1.toString() + uri2.toString());
			return new URI(uri1.toString() + uri2.toString());
		} catch(URISyntaxException e) {
			System.out.println("Error in the URI syntax");
			e.printStackTrace();
		}
		return null;
	}
	
	public int getServicesNumber() {
		return services.size();
	}
	
	public int getProcessesNumber() {
		return kb.getProcesses(org.mindswap.owls.process.Process.ATOMIC, false).size();
	}
	
	public void loadProcessesStatistics(URI baseUri) {
		
		processesStatistics = new java.util.Hashtable<String,ProcessStatistic>();
		
		try {
    	   URL url = new URL(baseUri.toString() + TestURIs.STATISTICS);
           java.io.BufferedReader br = new java.io.BufferedReader(new InputStreamReader(url.openStream()));
           String line = "";
           String name = "", in="", ex="", size="", transactionalProperty="";
           int tp=0;
           java.util.StringTokenizer st = null;
           
           //to be used for normalization
           double maxCostValue = 0;
        
           while ((line = br.readLine()) != null) {
               st = new java.util.StringTokenizer(line, ";");
               name = st.nextToken();
               in = st.nextToken();
               ex = st.nextToken();
               size = st.nextToken();
               transactionalProperty = st.nextToken();
               
               ProcessStatistic statistics = new ProcessStatistic(name);
               
               
               if(transactionalProperty.compareTo("p") == 0)
            	   tp = Vertice.PIVOT;
               else if(transactionalProperty.compareTo("pr") == 0)
            	   tp = Vertice.PIVOTRET;
               else if(transactionalProperty.compareTo("c") == 0)
            	   tp = Vertice.COMPENSA;
               else if(transactionalProperty.compareTo("cr") == 0)
            	   tp = Vertice.COMPENSARET;
               
               statistics.setTransactionalProperty(tp);
               //statistics.setExecutionCost(new Double(in).doubleValue()+ new Double(ex).doubleValue());
               double executionCost = new Double(in).doubleValue()+ new Double(ex).doubleValue();
               
               if(executionCost > maxCostValue) {
            	   maxCostValue = executionCost;
               }
               
               statistics.setExecutionCostReal(executionCost);
               statistics.setOutputSize(new Integer(size).intValue());
               processesStatistics.put(name, statistics);
              
           }
           
           br.close();
           
           calculateNormalizedCost(maxCostValue);
           
           
       } catch (Exception e) {

           System.out.println("Error al leer: " + e);
           e.printStackTrace();
       }
	}
	
	private void calculateNormalizedCost(double maxCost) {
		Enumeration<ProcessStatistic> statisticsList = processesStatistics.elements();
		
		while(statisticsList.hasMoreElements()) {
			ProcessStatistic ps= statisticsList.nextElement();
			
			double realCost = ps.getExecutionCostReal();
			
			ps.setExecutionCost((maxCost-realCost)/maxCost);
			
			
		}
	}
	
	public void setEquivalentServices(MarkedPN pn) {
		for(Vertice s: pn.getTransitions()) {
			s.setEquivalents(new ArrayList<Vertice>());
			List<String> sInputs = new ArrayList<String>();
			List<String> sOutputs = new ArrayList<String>();
			
			Service s2 = getServiceByName(s.getName());
			
			
			
			for(Input i:s2.getProcess().getInputs())
				sInputs.add(i.getParamType().getURI().getFragment());
			
			for(Output o:s2.getProcess().getOutputs())
				sOutputs.add(o.getParamType().getURI().getFragment());
			
			
			for(Service sOwls:getServices()) {
				 List<Input> serviceInputs = sOwls.getProcess().getInputs();
				 List<String> inputs = new ArrayList<String>();
				 for(Input i:serviceInputs)
					 inputs.add(i.getParamType().getURI().getFragment());
				 
				 List<Output> serviceOutputs = sOwls.getProcess().getOutputs();
				 List<String> outputs = new ArrayList<String>();
				 for(Output o:serviceOutputs)
					 outputs.add(o.getParamType().getURI().getFragment());
				 
				 if(areExactFunctionallyEquivalent(sInputs, sOutputs, inputs, outputs)) {
					 s.getEquivalents().add(
							 new Vertice(sOwls.getName(), Vertice.TRANSITION, s.getTransactionalProperty()
									 , s.getQoS(),  s.getQoSReal(), sOwls.getURI())
					 );
				 }
			}
		}
		
	}
	
	private boolean areExactFunctionallyEquivalent(List<String> ins1, List<String> outs1,  List<String> ins2, List<String> outs2) {
		boolean ret = false;
		
		
		//if(ins1.containsAll(ins2) && ins2.containsAll(ins1) && outs1.containsAll(outs2) && outs2.containsAll(outs1))
		if(ins1.containsAll(ins2) & outs2.containsAll(outs1))
			ret = true;
		
		return ret;
	}
	

}
