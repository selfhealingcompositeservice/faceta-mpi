package ve.usb.KnowledgedeBase;


//tomado del codigo de Eduardo
public class ProcessStatistic {
    
    private String name = "";
    private double cost = -1;
    private double intCost = -1;
    private double extCost = -1;
    private double executionCost = -1;
    private double executionCostReal = -1;
    private int transactionalProperty = -1;
    private int outputSize = -1;
    
    /**
     * Creates a new instance of ProcessStatistic
     */
    public ProcessStatistic(String name) {
        this.name = name;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getIntCost() {
		return intCost;
	}

	public void setIntCost(double intCost) {
		this.intCost = intCost;
	}

	public double getExtCost() {
		return extCost;
	}

	public void setExtCost(double extCost) {
		this.extCost = extCost;
	}

	public int getTransactionalProperty() {
		return transactionalProperty;
	}

	public void setTransactionalProperty(int transactionalProperty) {
		this.transactionalProperty = transactionalProperty;
	}

	public double getExecutionCost() {
		return executionCost;
	}

	public void setExecutionCost(double executionCost) {
		this.executionCost = executionCost;
	}
	
	public double getExecutionCostReal() {
		return executionCostReal;
	}

	public void setExecutionCostReal(double executionCostReal) {
		this.executionCostReal = executionCostReal;
	}

	public int getOutputSize() {
		return outputSize;
	}

	public void setOutputSize(int outputSize) {
		this.outputSize = outputSize;
	}
    
}
