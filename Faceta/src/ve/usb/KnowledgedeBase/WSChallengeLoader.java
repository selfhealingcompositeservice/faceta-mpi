package ve.usb.KnowledgedeBase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nu.xom.*;


public class WSChallengeLoader {
	
	/**
	 * Reads the file and returns the content of the file as a string.
	 * @param fileURL The URL of the file.
	 * @return The content of the file.
	 */
	private void readFile(String fileURL) {
		
		try {
			URL source = new URL(fileURL);
			BufferedReader bufferedFileReader = new BufferedReader(new InputStreamReader(source.openStream()));			
			String line = null;		
		
			while((line = bufferedFileReader.readLine()) != null) {
				System.out.println(line);
			}
			
			bufferedFileReader.close();			
			
		} catch (Exception exception) {			
			exception.printStackTrace();			
		}
	}
	
	static String message = "";
	static List<WSService> services = new ArrayList<WSService>();
	
	static List<ComplexElement> complexElements = new ArrayList<ComplexElement>();
	static Map<String,URI> semanticExtensions = new HashMap<String, URI>();
	
	public static void listChildren(Node current, int depth) throws URISyntaxException {
		   
		
	    if (current instanceof Element) {
	    	
	    	Elements children = ((Element)current).getChildElements();
	    	WSService service = null;
	        for (int i = 0; i < children.size(); i++) {
	        
	    	
	        	
		    
		    
	        Element temp = (Element) children.get(i);
	        System.out.println(temp.getQualifiedName());
	        if(temp.getQualifiedName().equalsIgnoreCase("service")) {
	        	System.out.println("THE SERVICE!!!!!!!!!!!!!!! " + temp.getAttributeValue("name"));
	        	service = new WSService();
	        	service.setName(temp.getAttributeValue("name").split("Service")[0]);
	        }
	        boolean done = false;
	        if(temp.getQualifiedName().equalsIgnoreCase("message")) {
	        	System.out.println("THE MESSAGE!!!!!!!!!!!!!!! "  + temp.getAttributeValue("name"));
	        	message =  temp.getAttributeValue("name");
	        	
	        	Elements childrenPart = temp.getChildElements();
		        for (int j = 0; j < childrenPart.size(); j++) {
		        	Element part = (Element) childrenPart.get(j);
		        	if(part.getQualifiedName().equalsIgnoreCase("part")) {
			        	System.out.println("In the part " + message);
			        	if(message.contains("Request")) {
			        		
			        		System.out.println("input: " + part.getAttributeValue("element").split(":")[1]);
			        		service.addInput(part.getAttributeValue("element").split(":")[1], "");
			        	} else if(message.contains("Response")) {
			        		
			        		System.out.println("output: " + part.getAttributeValue("element").split(":")[1]);
			        		service.addOutput(part.getAttributeValue("element").split(":")[1], "");
			        		
			        		done = true;
			        	}
			        
			        }
		        	
		        }
		        
	        	
	        }
	        if(temp.getQualifiedName().equalsIgnoreCase("mece:semExtension")) {
	        	System.out.println("Semantic Extensions");
	        	Elements semExt = temp.getChildElements();
	        	for (int k = 0; k < semExt.size(); k++) {
	        		
	        		Element ext = (Element) semExt.get(k);
	        		
	        		if(ext.getAttributeValue("id").contains("Request")) {
	        			
	        			String serviceName = ext.getAttributeValue("id").split("RequestMessage")[0];
	        			System.out.println("inputs: " + serviceName);
	        			WSService s = getServiceByName(serviceName);
	        			
	        			Elements messageConcepts = ext.getChildElements();
	        			for (int n = 0; n < messageConcepts.size(); n++) {
	        				
	        				Element messageConcept = (Element) messageConcepts.get(n);
	        				System.out.println("messageConcept message: " + messageConcept.getAttributeValue("id"));
	        				Element concept = (Element) messageConcept.getChild(1);
	        				System.out.println("messageConcept concept: " + concept.getValue());
	        				s.addInput(messageConcept.getAttributeValue("id"), concept.getValue());
	        				
	        				if(semanticExtensions.put(messageConcept.getAttributeValue("id"), new URI(concept.getValue())) != null)
	        					System.exit(0);
	        			}
	        			
	        			
	        			
	        		} else if(ext.getAttributeValue("id").contains("Response")) {
	        			String serviceName = ext.getAttributeValue("id").split("ResponseMessage")[0];
	        			System.out.println("outputs: " + serviceName);
	        			
	        			WSService s = getServiceByName(serviceName);
	        			
	        			Elements messageConcepts = ext.getChildElements();
	        			for (int n = 0; n < messageConcepts.size(); n++) {
	        				
	        				Element messageConcept = (Element) messageConcepts.get(n);
	        				System.out.println("messageConcept message: " + messageConcept.getAttributeValue("id"));
	        				Element concept = (Element) messageConcept.getChild(1);
	        				System.out.println("messageConcept concept: " + concept.getValue());
	        				s.addOutput(messageConcept.getAttributeValue("id"), concept.getValue());
	        				
	        				if(semanticExtensions.put(messageConcept.getAttributeValue("id"), new URI(concept.getValue())) != null)
	        					System.exit(0);
	        			}
	        		}
	        	}
	        }
	        
	        if(temp.getQualifiedName().equalsIgnoreCase("types")) {
	        	listChildren(temp, null);
	        }
	        
	        if(done)
	        	services.add(service);
	        
	        }
	        
	    }
	    
	    System.out.println(services.size());
	    
	    for(WSService s:services) {
	    	for (Map.Entry<String,URI> entry : s.getInputs().entrySet()) {
	    		  String key = entry.getKey();
	    		  URI value = entry.getValue();
	    		  System.out.println(key + " - " + value);
	    		}
	    
			   for (Map.Entry<String,URI> entry : s.getOutputs().entrySet()) {
		  		  String key = entry.getKey();
		  		  URI value = entry.getValue();
		  		  System.out.println(key + " - " + value);
		  		}
		  	}	
	   
	    fillSemanticsForComplexElements(complexElements);
	    
	    for(ComplexElement c:complexElements)
	    	for(ComplexElement c2:complexElements)
	    		if(!c.getName().equals(c2.getName()))
	    			if(c.equals(c2))
	    				System.out.println(c.getName() + " - " + c2.getName());
	
	    findWSSameComplexElement();
	}
	
	private static void findWSSameComplexElement() {
		System.out.println("Same Complex Finding");
		for(WSService s:services) {
			for(WSService s2:services) {
				//if(!s.getName().equals(s2.getName())) {
					for(String a1:s.getInputs().keySet()) {
						if(a1.contains("Complex")) {
							for(String a2:s2.getInputs().keySet()) {
								if(a1.equals(a2))
									System.out.println("YEAHHHH: " + s.getName() + " - " + s2.getName());
							}
							
							for(String a2:s2.getOutputs().keySet()) {
								if(a1.equals(a2))
									System.out.println("YEAHHHH: " + s.getName() + " - " + s2.getName());
							}
						}
					}
					
					for(String a1:s.getOutputs().keySet()) {
						if(a1.contains("Complex")) {
							for(String a2:s2.getInputs().keySet()) {
								if(a1.equals(a2))
									System.out.println("YEAHHHH: " + s.getName() + " - " + s2.getName());
							}
							
							for(String a2:s2.getOutputs().keySet()) {
								if(a1.equals(a2))
									System.out.println("YEAHHHH: " + s.getName() + " - " + s2.getName());
							}
						}
					}
				//}
			}
		}
	}
	
	private static void  fillSemanticsForComplexElements(List<ComplexElement>  l) {
		
		for(ComplexElement c:l) {
			for(String a:c.getAttributes().keySet()) {
				c.getAttributes().put(a, semanticExtensions.get(a));
			}
			fillSemanticsForComplexElements(c.getComplexElements());
		}
	
	}
	
	private static void listChildren(Element temp, ComplexElement ce) {
		System.out.println("TYPES");
    	Elements complexEls = temp.getChildElements();

    	for (int k = 0; k < complexEls.get(0).getChildElements().size(); k++) {
    		Element el = (Element)  complexEls.get(0).getChildElements().get(k);
			
    		if(el.getAttributeValue("name").contains("Complex")) {
    			System.out.println("COMPLEX: " +  el.getAttributeValue("name"));
    			ComplexElement cet = new ComplexElement();
    			cet.setName(el.getAttributeValue("name"));
    			
    			if(ce != null) {
    				ce.addComplexElement(cet);
    			} else {
    				complexElements.add(cet);
    			}
    				listChildren((Element)el.getChild(1), cet);
    		} else {
    			System.out.println("µµµµ: " + el.getAttributeValue("name") + " --- " + semanticExtensions.get(el.getAttributeValue("name")));
    			if(ce != null)
    				ce.addAttribute(el.getAttributeValue("name"), semanticExtensions.get(el.getAttributeValue("name")));
    		}
    		
    	}
	}
	  
	private static WSService getServiceByName(String name) {
		for(WSService s:services)
			if(s.getName().equals(name))
				return s;
		return null;
	}
	  
	public void initialize(String wsdlServiceDescriptionsURL,
			String owlTaxonomyURL,
			String wslaSLAgreementsURL) throws URISyntaxException {
		
		// Print the given documents.
		System.out.println("WSDL-Service-Descriptions:");
		//readFile(wsdlServiceDescriptionsURL);
		
		try {
			  Builder parser = new Builder();
			  Document doc = parser.build(wsdlServiceDescriptionsURL);
			  
			  Element root = doc.getRootElement();
		      System.out.println(root);
		      listChildren(root, 0);
		}
		catch (ParsingException ex) {
		  System.err.println("Cafe con Leche is malformed today. How embarrassing!");
		}
		catch (IOException ex) {
		  System.err.println("Could not connect to Cafe con Leche. The site may be down.");
		}
		
		
		System.out.println();
		System.out.println("OWL-Taxonomy:");
		//readFile(owlTaxonomyURL);		
		
		System.out.println();
		System.out.println("WSLA-Agreements:");
		//readFile(wslaSLAgreementsURL);		
	}
	
	public static void main(String...str) throws URISyntaxException {
		WSChallengeLoader loader = new WSChallengeLoader();
		
		loader.initialize("http://localhost/test/Services.wsdl", "http://localhost/test/Taxonomy.owl"
				, "http://localhost/test/Servicelevelagreements.wsla");
		
	}

}
