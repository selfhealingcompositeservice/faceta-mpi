package ve.usb.KnowledgedeBase;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class WSService {
	
	String name;
	Map<String, URI> inputs;
	Map<String, URI> outputs;
	
	public WSService() {
		inputs = new HashMap<String,URI>();
		outputs = new HashMap<String,URI>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, URI> getInputs() {
		return inputs;
	}

	public void setInputs(Map<String, URI> inputs) {
		this.inputs = inputs;
	}

	public Map<String, URI> getOutputs() {
		return outputs;
	}

	public void setOutputs(Map<String, URI> outputs) {
		this.outputs = outputs;
	}
	
	public void addInput(String name, URI uri) {
		inputs.put(name, uri);
	}
	
	public void addOutput(String name, URI uri) {
		outputs.put(name, uri);
	}

	public void addInput(String name, String uri) {
		try {
			inputs.put(name, new URI(uri));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void addOutput(String name, String uri) {
		try {
			outputs.put(name, new URI(uri));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
