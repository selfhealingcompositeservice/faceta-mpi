package ve.usb.Main;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.utils.LoadUtils;

public class ChangePlanToAtomicMain {

	
	public static void main(String...args) {
		
		Query query = LoadUtils.loadQueryFromFile("../saved_data/query_results/BA200_3_4/" +
				"Q-Transac-Escenario1-BA200_3_4-NP-10-IND-04");
		
		MarkedPN mp = query.getResults().getWSDN();	
		
		
		for(Vertice v:mp.getTransitions())
			if(v.getName().equals("BA200_3_013-REPLICA-0")
					|| v.getName().equals("BA200_3_097-REPLICA-0"))
				v.setPivot();
				
		
		for(Vertice v:mp.getTransitions())
			System.out.println(v.getName() + " - " + v.getTransactionalProperty());
		
		query.setName(query.getName() + "_" + "a");
		LoadUtils.saveQuery(query);
	}

}
