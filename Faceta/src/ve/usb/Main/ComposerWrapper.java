package ve.usb.Main;

import java.util.ArrayList;
import java.util.List;

import ve.usb.Composer.Composer;
import ve.usb.Composer.Query;
import ve.usb.Composer.QueryResults;
import ve.usb.Exhaustive.Exhaustive;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.PetriNet;

public class ComposerWrapper {
	
	private MarkedPN myMPN;
	
	public MarkedPN getMyMPN() {
		return myMPN;
	}

	public void setMyMPN(MarkedPN myMPN) {
		this.myMPN = myMPN;
	}

	public List<QueryResults> runComposer(PetriNet mipn, List<Query> queries) throws InterruptedException {

			List<QueryResults> results = new ArrayList<QueryResults>();
			System.out.println(mipn.toString());
			
			//run yellow coloring and composer
			for(Query myq:queries) {
				if(myq.getName().contains("NP-10") || myq.getName().contains("NP-09")) {
					System.out.println("Query: " + myq.getName());
					java.util.Date t1 = new java.util.Date();
					if (myq.validate(mipn)) { 
							java.util.Date t2 = new java.util.Date();
							System.out.println("Validation_Time " + (t2.getTime() - t1.getTime()));
				
							t1 = new java.util.Date();
							myMPN = new MarkedPN("MYMPN"); 
							myMPN.yellowColoring(myq,mipn);
							t2 = new java.util.Date();
							System.out.println("Yellow_Coloring_Time " + (t2.getTime() - t1.getTime()));
				
							//Composer com = new Composer(myMPN,myq);
				
							//myq.setResults(com.run());
							Exhaustive exhaustive = new Exhaustive(myMPN, myq, 5);
							exhaustive.run();
							results.add(myq.getResults());
							
							/*Exhaustive exhaus = new Exhaustive(myMPN, myq, 5);
							System.out.println("===== EXHAUSTIVE ====== ");
							exhaus.run();*/
							
						} else System.out.println("Query NO valido");
				}
				
			}
			return results;
		}
}
