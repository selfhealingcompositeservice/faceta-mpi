package ve.usb.Main;

import org.jgrapht.graph.DefaultWeightedEdge;

public class ExecutorDefaultWeightedEdge extends DefaultWeightedEdge {
	
	double w;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getSource() + "->" + getTarget();
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return w;
	}
	
	public void setWeight(double d) {
		this.w = d;
	}

	@Override
	public Object getSource() {
		// TODO Auto-generated method stub
		return super.getSource();
	}

	@Override
	public Object getTarget() {
		// TODO Auto-generated method stub
		return super.getTarget();
	}
	
	

}
