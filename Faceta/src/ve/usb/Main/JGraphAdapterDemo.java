/* ==========================================
 * JGraphT : a free Java graph-theory library
 * ==========================================
 *
 * Project Info:  http://jgrapht.sourceforge.net/
 * Project Creator:  Barak Naveh (http://sourceforge.net/users/barak_naveh)
 *
 * (C) Copyright 2003-2008, by Barak Naveh and Contributors.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/* ----------------------
 * JGraphAdapterDemo.java
 * ----------------------
 * (C) Copyright 2003-2008, by Barak Naveh and Contributors.
 *
 * Original Author:  Barak Naveh
 * Contributor(s):   -
 *
 * $Id: JGraphAdapterDemo.java 725 2010-11-26 01:24:28Z perfecthash $
 *
 * Changes
 * -------
 * 03-Aug-2003 : Initial revision (BN);
 * 07-Nov-2003 : Adaptation to JGraph 3.0 (BN);
 *
 */
package ve.usb.Main;

import java.awt.*;
import java.awt.geom.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import org.jgraph.*;
import org.jgraph.graph.*;

import org.jgrapht.*;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.ext.*;
import org.jgrapht.graph.*;

// resolve ambiguity
import org.jgrapht.graph.DefaultEdge;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.config.Config;
import ve.usb.config.ConfigExecutor;
import ve.usb.config.ExecutorRunningProperties;
import ve.usb.utils.LoadUtils;
import ve.usb.utils.ResultsPrinter;


/**
 * A demo applet that shows how to use JGraph to visualize JGraphT graphs.
 *
 * @author Barak Naveh
 * @since Aug 3, 2003
 */
public class JGraphAdapterDemo
    extends JApplet
{
    //~ Static fields/initializers ---------------------------------------------

    private static final long serialVersionUID = 3256444702936019250L;
    private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
    private static final Dimension DEFAULT_SIZE = new Dimension(1000, 1000);

    //~ Instance fields --------------------------------------------------------

    //
    private JGraphModelAdapter<String, DefaultEdge> jgAdapter;

    //~ Methods ----------------------------------------------------------------

    /**
     * An alternative starting point for this demo, to also allow running this
     * applet as an application.
     *
     * @param args ignored.
     */
    public static void main(String [] args)
    {
    	
    	
        JGraphAdapterDemo applet = new JGraphAdapterDemo();
        applet.init();

        JFrame frame = new JFrame();
        frame.getContentPane().add(applet);
        frame.setTitle("JGraphT Adapter to JGraph Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    public void init()
    {
        // create a JGraphT graph
        ListenableGraph<String, DefaultEdge> g =
            new ListenableDirectedMultigraph<String, DefaultEdge>(
                DefaultEdge.class);

       
        
        try {
			//ExecutorRunningProperties runningProperties = ConfigExecutor.loadProperties();

			
			Query query = LoadUtils.loadQueryFromFile("results/NP-10/atomic/Q-Transac-Manual-R1-SF50_4_8-NP-10-IND-10");

			
			MarkedPN mp = query.getResults().getWSDN();
			//WSee_i
			Vertice wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
			mp.addWebService(wsee_i, new ArrayList<Vertice>(), mp.getInputs());
			
			//WSee_f
			Vertice  wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
			mp.addWebService(wsee_f, mp.getOutputs(), new ArrayList<Vertice>());
			
			
			Graph<String, DefaultEdge> myg =  ResultsPrinter.getGraph(mp);
			
			
			// create a visualization using JGraph, via an adapter
	        jgAdapter = new JGraphModelAdapter<String, DefaultEdge>(myg);

	        JGraph jgraph = new JGraph(jgAdapter);
	       
	        adjustDisplaySettings(jgraph);
	        getContentPane().add(jgraph);
	        resize(DEFAULT_SIZE);
	        
			
	        
	        Map<String, Integer> distances = new HashMap<String, Integer>();
	        Map<Integer, Integer> ydistances = new HashMap<Integer, Integer>();
	        
	        for(Vertice t:mp.getTransitions()) {
	        	int mdti = (int) (new DijkstraShortestPath<String, DefaultEdge>(myg, "WSee_i()", ResultsPrinter.getVertexName(t))).getPathLength();
	        	
	        	distances.put(ResultsPrinter.getVertexName(t), mdti);
	        	ydistances.put(mdti, 400);
	        	
	        	
	        }
	        
	       
	        System.out.println(ydistances);
			for(String v:myg.vertexSet()) {
					System.out.println(distances.get(v));
				//if(mp.getPredTransitions(mp.getTransition(v)).size() != 0 || v.equals(wsee_i.getName())) {
					//positionVertexAt(v, distances.get(v)*400, ydistances.get(distances.get(v)));
					ydistances.put(distances.get(v), ydistances.get(distances.get(v))-40);
				//}
			}
			
			
			 
			
//	        positionVertexAt(v2, 60, 200);
//	        positionVertexAt(v3, 310, 230);
//	        positionVertexAt(v4, 380, 70);
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        

        // that's all there is to it!...
    }

    private void adjustDisplaySettings(JGraph jg)
    {
        jg.setPreferredSize(DEFAULT_SIZE);

        Color c = DEFAULT_BG_COLOR;
        String colorStr = null;
        
        try {
            colorStr = getParameter("bgcolor");
        } catch (Exception e) {
        }

        if (colorStr != null) {
            c = Color.decode(colorStr);
        }

        jg.setBackground(c);
        
       
    }

    @SuppressWarnings("unchecked") // FIXME hb 28-nov-05: See FIXME below
    private void positionVertexAt(Object vertex, int x, int y)
    {
        DefaultGraphCell cell = jgAdapter.getVertexCell(vertex);
        AttributeMap attr = cell.getAttributes();
        Rectangle2D bounds = GraphConstants.getBounds(attr);

        Rectangle2D newBounds =
            new Rectangle2D.Double(
                x,
                y,
                bounds.getWidth(),
                bounds.getHeight());

        GraphConstants.setBounds(attr, newBounds);

        // TODO: Clean up generics once JGraph goes generic
        AttributeMap cellAttr = new AttributeMap();
        cellAttr.put(cell, attr);
        jgAdapter.edit(cellAttr, null, null, null);
    }

    //~ Inner Classes ----------------------------------------------------------

    /**
     * a listenable directed multigraph that allows loops and parallel edges.
     */
    private static class ListenableDirectedMultigraph<V, E>
        extends DefaultListenableGraph<V, E>
        implements DirectedGraph<V, E>
    {
        private static final long serialVersionUID = 1L;

        ListenableDirectedMultigraph(Class<E> edgeClass)
        {
            super(new DirectedMultigraph<V, E>(edgeClass));
        }
        
        
    }
}

// End JGraphAdapterDemo.java