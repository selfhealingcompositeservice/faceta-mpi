package ve.usb.Main;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import ve.usb.Composer.Query;
import ve.usb.KnowledgedeBase.KnowledegeBase;
import ve.usb.PetriNets.PetriNet;
import ve.usb.PetriNets.Vertice;
import ve.usb.config.Config;
import ve.usb.config.RunningProperties;
import ve.usb.config.URILoader;
import ve.usb.executor.thread.EngineThreadThread;
import ve.usb.executor.thread.ExecutionEngineThread;
import ve.usb.utils.LoadUtils;

public class MainComposerUtil {
	
	private RunningProperties properties;
	
	public static void main(String args[]) {
		try {

			(new MainComposerUtil()).run();

		} catch(Exception e) {

			e.printStackTrace();
		}
   }

	public void run() throws Exception {
		System.out.println("Running...");
		
		properties = Config.loadProperties();
		
		for(URI uri:URILoader.loadURIs()) {

			System.out.println("Loading KB...");
			KnowledegeBase mykb = new KnowledegeBase(uri, properties.getRisk());
			//mykb.remove();
			LoadUtils loadUtil = new LoadUtils(mykb, properties, uri);
			
			System.out.println("Loading PN...");
			PetriNet pn = loadUtil.loadPetriNet();
			
			System.out.println("PN NP before: " + pn.getNumProcecsos());
			/*for(Vertice t:pn.getTransitions())
				if(!(t.getName().contains("REPLICA-1") || t.getName().contains("REPLICA-5") || t.getName().contains("REPLICA-7")) )
					pn.deleteVertice(t.getName());
			*/
			System.out.println("PN NP after: " + pn.getNumProcecsos());

			ComposerWrapper composerWrapper = new ComposerWrapper();
			System.out.println("Running Composer...");
			composerWrapper.runComposer(pn, mykb.getQueries());
			
			//salvo queries de determinado tamano
			
			List<Query> listToSave = new ArrayList<Query>();
			
			for(Query q:mykb.getQueries()) {
				//if(q.getResults().getWSDN().getNumProcecsos() == properties.getProcessesNumber()) {
				if((q.getResults().getWSDN().getMarked() == 128 || q.getResults().getWSDN().getMarked() == 32) && q.getResults().getWSDN().getNumProcecsos() == 10) {
					//listToSave.add(q);
					LoadUtils.saveQuery(q);
				}
			}
			
			//LoadUtils.saveQueryList(mykb.getQueries(), pn.getName() + "_results_" + properties.getRisk());		
			
			//LoadUtils.saveQueryList(mykb.getQueries(), "queryList800");
			//LoadUtils.loadQueryList("queryList");
			//Query queryMPI = LoadUtils.loadQueryList("queryList").get(LoadUtils.loadQueryList("queryList").size()-1);
			/*Query queryMPI = mykb.getQueries().get(mykb.getQueries().size()-1);
			queryMPI.getResults().getWSDN().setName("MPI_result_"+queryMPI.getResults().getWSDN().getName()+"_"+queryMPI.getName());
			LoadUtils.saveResult(queryMPI.getResults().getWSDN());
			LoadUtils.saveQuery(queryMPI);
			
			for(Query query:LoadUtils.loadQueryList("queryList")) {
			//for(Query query:mykb.getQueries()) {
				//if(query.getName().equals("Q-Transac-Escenario1-BA200_3_4-NP-10-IND-06")) {
					//set the quality of the equivalent services
					for(Vertice v:query.getResults().getWSDN().getTransitions()) {
						if(v.getEquivalents() != null) {
							for(Vertice eq:v.getEquivalents()) {
								//eq.setQuality(composerWrapper.getMyMPN().getTransition(v.getName()).getQuality());
								
							}
						}
					}
					//saving plan
					/*query.getResults().getWSDN().setName("result_"+pn.getName()+"_"+query.getName());
					LoadUtils.saveResult(query.getResults().getWSDN());
					LoadUtils.saveQuery(query);*/
				//	EngineThreadThread.reachedCompensation = 0;
					//ExecutionEngineThread.queues = null;

				//}
//			}
		
		}
	}
	
}
