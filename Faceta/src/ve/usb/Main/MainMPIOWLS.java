package ve.usb.Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mpi.MPI;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.config.ConfigExecutor;
import ve.usb.config.ExecutorRunningProperties;
import ve.usb.executor.EngineThreadResult;
import ve.usb.executor.FTMechanism.Mechanism;
import ve.usb.executor.mpi.ExecutionEngineMPI;
import ve.usb.serialization.XMLPNSerializer;
import ve.usb.serialization.XMLQuerySerializer;
import ve.usb.utils.LoadUtils;
import ve.usb.utils.OutputDegree;
import ve.usb.utils.ResultsPrinter;

public class MainMPIOWLS {
	
	private static final int MASTER = 0;

	public static void main(String...args) throws Exception {

		Logger logger = Logger.getLogger(MainMPIOWLS.class);
		

		MPI.Init(args);
		ExecutorRunningProperties runningProperties = ConfigExecutor.loadProperties();

		
		

		int me = MPI.COMM_WORLD.Rank();
		if(me == MASTER) {
			
			BasicConfigurator.configure();
			logger.info("Starting application");
			
			
			//Load Query
			//List<Query> queryResultList = LoadUtils.loadQueryList(runningProperties.getQueryResultFileName());
			List<Query> queryResultList = LoadUtils.loadQueryList("output/queries.faceta");
			System.out.println("Query List: " + queryResultList.size());
			
			//Query query = LoadUtils.loadQueryFromFile(queryResultList.get(0));
			//Query query = (new XMLQuerySerializer()).XMLtoPN(args[0]);
			
			//MarkedPN mp = query.getResults().getWSDN();
			//MarkedPN mp = (new XMLPNSerializer()).XMLtoPN(args[1]);
			
			//equivalentes
			

			
			//String uri = null;
			
			//results/Q-Transac-Escenario1-BA200_3_4-NP-10-IND-01
			/*String [] parts = queryResultList.get(0).split("-");
			int nreplicas = 0;
			for(String p:parts)
				System.out.println(p);
			if(parts[3].equals("BA200_3_4"))
				//uri= "http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario1/BA200_3_4/";
				nreplicas = 4;
			else if(parts[3].equals("BA800_3_4"))
				//uri = "http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario1/BA800_3_4/";
				nreplicas = 4;
			else if(parts[3].equals("BA200_3_8"))
				//uri = "http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario2/BA200_3_8/";
				nreplicas = 8;
			else if(parts[3].equals("BA800_3_8"))
				//uri = "http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario2/BA800_3_8/";
				nreplicas = 8;*/
			
			for(Query query:queryResultList) {
			if(query.getName().equals("Q-Transac-Escenario2-BA800_3_8-NP-10-IND-09")){
				
				
				
				MarkedPN mp = query.getResults().getWSDN();
				
				//MaximunCostPath.multipleQosBy(10, mp);
				
				MaximunCostPath mcp = new MaximunCostPath(mp);
				
				int maximumCostPath = mcp.getMaximumCostPath();
				
				//select one service to fail at random
				int nodeIndex = 1 + (int)(Math.random() * ((mp.getTransitions().size() - 1) + 1));
				//System.out.println("Faulty Node: " + nodeIndex);
				
				
				int nreplicas = 8;
				
				for(Vertice v:mp.getTransitions()) {
					List<Vertice> eqs = new ArrayList<Vertice>();
					for(int i=1;i<=nreplicas;i++) {
						double [] qos_real = {v.getQoSReal()[0]+(i*1000)};
						Vertice e = new Vertice(v.getName()+"-replica-"+(i), Vertice.TRANSITION, v.getTransactionalProperty(),v.getQoS(),qos_real);
						eqs.add(e); 
					}
					v.setEquivalents(eqs);
				}
				
				//visualization
				
				ve.usb.ui.jung.SimpleGraphView2 graphUI = new ve.usb.ui.jung.SimpleGraphView2();
				graphUI.run(mp);
				
				
				/*for(Vertice v:mp.getTransitions()) {
					System.out.println("***V**");
					System.out.println("V: " + v.getName() + ", C: " + v.getQoSReal()[0]);
					System.out.println("***equivalentes**");
					for(Vertice e : v.getEquivalents())
						System.out.println("E: " + e.getName() + ", C: " + e.getQoSReal()[0]);
				}*/
				
				//equivalent services
				/*ServiceEquivalence seq = 
					new ServiceEquivalence(
							mp.getTransitions().get(0), 
							new URI(uri));
				seq.getEquivalents();*/
				//KnowledegeBase kb = new KnowledegeBase(new URI("http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario1/BA200_3_4/"), 0);
				//kb.setEquivalentServices(query.getResults().getWSDN());
				
				java.util.Date t1 = new java.util.Date();
			    
				ExecutionEngineMPI executor = new ExecutionEngineMPI(query, mp, args, 0, maximumCostPath, 
						mcp.getMaximumCostPathFromNode(), runningProperties.getQoSNodes(mp.getTransitions().size()));
				
				executor.setFtMechanism(runningProperties.getFtMechanism());
				
				executor.setFaultyNode(nodeIndex);
				
				executor.setDeltaPercent(runningProperties.getDeltaPercent());
				
				Map<String, List<String>> outputDependencies = (new OutputDegree(mp, query.getOuts())).getDependencies();
						
				executor.setOutputDependencies(outputDependencies);
				
				List<EngineThreadResult> results = executor.run();
				
				
				
				java.util.Date t2 = new java.util.Date();
			    
			    double executionTime = t2.getTime() - t1.getTime();
			    double failureRealExecutionTime = 0;
			    int checkpointedSize = 0;
			    String faultyWs = null;
			    String mechanism = Mechanism.NIL.toString();
			    boolean previousMechanism = false;
			    int replicationState = 0;
			    int forwardRecovery = 0;
			    int replication = 0;
			    int replicationOriginalSuccess = 0;
			    double replicationNecessity;
			    int failedReplication = 0;
			    int successfullyExecuted = 0;
			    int failedNoForwardRecovery = 0;
			    int compensated = 0;
			    int generatedOutput = 0;
			    List<String> generatedOutputList = new ArrayList<String>();
			    double earliestFailureTime = 999999999;
			    int outputsDependencyFailedWS = 0;
			    double estimatedExecutionTimeLeft = 0;
			    
			    //Gather the information from Engine Threads results
			    
			    for(EngineThreadResult r:results) {
			    	if(r.getFtMechanism() == Mechanism.FORWARD_RECOVERY || r.getFtMechanism() == Mechanism.REINTENT_RECOVERY)
			    		forwardRecovery+=1;
			    	else if(r.getFtMechanism() == Mechanism.REPLICATION)
			    		replication+=1;
		    		else if(r.getFtMechanism() == Mechanism.CHECKPOINTING || r.isCheckpointingSkip())
		    			checkpointedSize++;

			    	replicationOriginalSuccess = r.getReplicationState() == 1 ? replicationOriginalSuccess + 1 : replicationOriginalSuccess;
		    		
		    		failedReplication = r.getReplicationState() == 3 ? failedReplication + 1 : failedReplication;
		    		
		    		if(r.isSuccessfullyExecuted()) {
		    			successfullyExecuted++;
		    			generatedOutputList.addAll(mp.sucesores(r.getName()));
		    		} else {
		    			if (mcp.getMaximumCostPathFromNode().get(r.getName()) > estimatedExecutionTimeLeft)
		    				estimatedExecutionTimeLeft = mcp.getMaximumCostPathFromNode().get(r.getName());
		    		}
		    		
		    		if(r.isFailed())
		    			failedNoForwardRecovery++;
		    		
		    		if(r.isCompensated())
		    			compensated++;
		    		
		    		if(r.getFailureRealExecutionTime() != 0 && r.getFailureRealExecutionTime() < earliestFailureTime) {
		    			earliestFailureTime = r.getFailureRealExecutionTime();
		    			outputsDependencyFailedWS = r.getOutputsDependency();
		    		}
			    }
			    
			    for(Vertice s:query.getOuts())
			    	if(generatedOutputList.contains(s.getName()))
			    		generatedOutput++;
			    
			    System.out.println("failedReplication=" + failedReplication);
			    replicationNecessity = replication > 0 ? 100 - (replicationOriginalSuccess*100)/replication : 0;
			    
			    System.out.println("checkpointed size: " + checkpointedSize);
			    System.out.println("query out size: " + query.getOuts().size());
			    System.out.println("outputs: " + query.getOuts());
			    System.out.println("outputs size: " + query.getOuts().size());
			    System.out.println("outputs received: " + executor.getOutput());

			    
			    (new ResultsPrinter()).printExecutionResults(query, runningProperties.getDeltaPercent(), executionTime, maximumCostPath, 
			    		executor.getElapsedTimeNodes(), faultyWs, mechanism, previousMechanism
			    		, forwardRecovery, compensated, replication , replicationNecessity
			    		, failedReplication, checkpointedSize, successfullyExecuted, failedNoForwardRecovery, (generatedOutput*100)/query.getOuts().size()
			    		, earliestFailureTime != 999999999 ? earliestFailureTime : 0, (outputsDependencyFailedWS*100)/query.getOuts().size()
			    		, estimatedExecutionTimeLeft);
			    
			}
			
			}
	
			} else {
				//TODO:arreglar esto? es decir, no instanciar el EE en todos los nodos
				ExecutionEngineMPI exec = new ExecutionEngineMPI(runningProperties.getServiceExecutionFailure()); 
			
				exec.setFtMechanism(runningProperties.getFtMechanism());
				
				
				exec.run();
			}
		
		logger.info("-- THE END -- of " + me);
		
		MPI.Finalize();
		
			
	}

	

}
