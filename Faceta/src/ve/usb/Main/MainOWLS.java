package ve.usb.Main;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import org.mindswap.owls.process.variable.Input;
import org.mindswap.owls.process.variable.Output;
import org.mindswap.owls.service.Service;

import ve.usb.Composer.Composer;
import ve.usb.Composer.Query;
import ve.usb.Composer.QueryResults;
import ve.usb.Exhaustive.Exhaustive;
import ve.usb.KnowledgedeBase.KnowledegeBase;
import ve.usb.KnowledgedeBase.ProcessStatistic;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.PetriNet;
import ve.usb.PetriNets.Vertice;
import ve.usb.config.Config;
import ve.usb.config.RunningProperties;
import ve.usb.config.URILoader;
import ve.usb.utils.LoadUtils;

public class MainOWLS {
	
	/*
	 * 

	 MyStatis.qos: en cada linea como esta 
	 (http://www.ldc.usb.ve/~eduardo/Ontologies/Graphs/Stress/Small/BA100_3_2/MyServices.owl#BA100_3_002-REPLICA-0-Process;545.0;225.0;22;c)
	 esta:
		- el nombre del proceso del servicio (solo hay servivios atomicos)
		- el costo de ejecutar el servicio (suma de 545+225)
		- el tamano de la salida (nume de tuplas de salida)
		- la propiedad transaccional

	 
	 Salida:
	 	-size: se refiere a la clasificacion que viene en el query, es decir, cuantos servicios son necesarios para responder el query. 
	 	       El valor se saca del owl donde esta definidos los queries, es el valor del atributo QNP 
	 	       (<j.0:QNP rdf:datatype="http://www.w3.org/2001/XMLSchema#int">9</j.0:QNP>)

	 */
	
	private String petriNetURLName;
	private RunningProperties properties;
	private URL petriNetURI;
	
	public static void main(String args[]) {
		try {

			(new MainOWLS()).run();

		} catch(Exception e) {

			e.printStackTrace();
		}
   }

	public void run() throws Exception {
		System.out.println("Running...");
		
		properties = Config.loadProperties();
		
		
		
		
		for(URI uri:URILoader.loadURIs()) {
			
			petriNetURI = uri.toURL();
			//fixing the URI if it does not have a forward slash as a last character
			String url =uri.toString();
			
			if(url.charAt(url.length()-1) != '/') {
				uri = new URI(url+'/');
			}
			
			//loading services information
			KnowledegeBase mykb = new KnowledegeBase(uri, properties.getRisk());
			
			//setting the PetriNet's name for serialization purposes
			setPetriNetName(uri);
	
			runAlgorithm(mykb);
	
			printResults(mykb);
			
			LoadUtils.saveQueries(mykb.getQueries());
			
			System.out.println("END");
		}
	}
	


	public void runAlgorithm(KnowledegeBase mykb) {

		try {	

			PetriNet petriNet = null;
			
			
			//load PetriNet
			if(properties.getLoadPetriNetFromFile())
				petriNet = PNInfo.loadPetriNetFromFile(new URL(petriNetURI + "output/BA800_3_8"));
			else {
				petriNet = loadPetriNet(mykb);
				if(!properties.getRunComposer())
					return;
			}
			
			//run yellow coloring and composer
			for(Query myq:mykb.getQueries()) {
		        java.util.Date t1 = new java.util.Date();
		        
		        //if(myq.getName().contains("NP-01-IND-01")){
	
		        if (myq.validate(petriNet)) { 
	
				    java.util.Date t2 = new java.util.Date();
				    
				    double validationTime = t2.getTime() - t1.getTime();
				    System.out.println("Validation_Time " + validationTime);
	
				    //Create Marked Petri Net
	
				    t1 = new java.util.Date();
	
				    MarkedPN myMPN = new MarkedPN(petriNetURLName);
	
				    System.out.println("Yellow_Coloring...");
	
				    myMPN.yellowColoring(myq, petriNet);
	
				    t2 = new java.util.Date();
				    
				    double yellowColoringTime = t2.getTime() - t1.getTime();
				    System.out.println("Yellow_Coloring_Time " + yellowColoringTime);
	
				    //End Marked Petri Net

				    //Run Composer
				    System.out.println("Running Composer...");
				    double composerTime = 0;
				    QueryResults qr = null;
				    if(!properties.getRunExhaustive()) {
					    Composer com = new Composer(myMPN, myq);
					    
					    t1 = new java.util.Date();
					    
					    qr = com.run();
					    
					    t2 = new java.util.Date();
				    } else {
				    	Exhaustive com = new Exhaustive(myMPN, myq, 5);
						    
					    t1 = new java.util.Date();
					    
					    List<Vertice> er = com.run();
					    
					    t2 = new java.util.Date();
				    }
				    
				    composerTime = t2.getTime() - t1.getTime();
				    
				    System.out.println("Composer Time: " + composerTime);

				    double totalTime = validationTime + yellowColoringTime + composerTime;
				    System.out.println("Total Time: " + totalTime);
				    
				    if(qr!=null) {
					    qr.setTime(totalTime);
					    
				 	    myq.setResults(qr);
				 	    
				 	    System.out.println("****************************");
						System.out.println("***********Executor*********");
						System.out.println("****************************");
						//Executor exec = new Executor(myq, myq.getResults().getWSDN()); 
						//exec.run();
						
						System.out.println("Number of services: " + qr.getNumServices());
				    }
		        //}
			 	    
			 	    
	
		        } else {
	
		        	System.out.println("Invalid Query");	
		        }
			}

		} catch(final RuntimeException e) {

			System.out.println("The following error occurred: ");
			e.printStackTrace();

		} catch (Exception e) {

			System.out.println("Error executing result: ");
			e.printStackTrace();

		}

	}
    
    private PetriNet loadPetriNet(KnowledegeBase mykb) {
    	PetriNet petriNet = new PetriNet(petriNetURLName);

		java.util.Hashtable<String, ProcessStatistic> statisticsList = mykb.getProcessesStatistics();
		System.out.println("Number of services: " + mykb.getServices().size());
		int i_s = 0;
		for(Service s: mykb.getServices()) {
			System.out.println("-Services number: " + ++i_s);
			final org.mindswap.owls.profile.Profile profile = s.getProfile();
			
			ProcessStatistic statistics = statisticsList.get(profile.getProcess().getURI().toString());
			
			int transactionalProperty = statistics.getTransactionalProperty();
			
			//Creating the Vertice that represents the service
			
			//TODO: revisar bien que va aqui (en qos[])
			
			double qos[] = {statistics.getExecutionCost()};
			double qos_real[] = {statistics.getExecutionCostReal()};

			Vertice v = new Vertice(s.getName(), Vertice.TRANSITION, transactionalProperty, qos, qos_real, s.getURI());

			ArrayList<Vertice>  inputs = new ArrayList<Vertice>();

            ArrayList<Vertice>  outputs = new ArrayList<Vertice>();

			org.mindswap.owls.process.Process p = s.getProcess();
			
			//Inputs
            List<Input> serviceInputs = p.getInputs();

            for(Input i : serviceInputs) {

                inputs.add(new Vertice(i.getParamType().getURI().getFragment(), Vertice.PLACE));
            }

            //Outputs
         	List<Output> serviceOutputs = p.getOutputs();

            for(Output o : serviceOutputs) {

            	outputs.add(new Vertice(o.getParamType().getURI().getFragment(), Vertice.PLACE));
            }
            
            //Adding the new web service to the Petri Net

            petriNet.addWebService(v, inputs, outputs);

		}
		
		//The creation of the PetriNet has been completed

		LoadUtils.savePetriNet(petriNet);
		return petriNet;
    }
      
    
    private void setPetriNetName(URI uri) {

    	petriNetURLName = uri.toString().replaceFirst(".*/([^/?]+).*", "$1");
    	
    }
    
  //tomado del codigo de Eduardo
    protected void printResults(KnowledegeBase kb) {
		String all = "", fs = "";
		int widSize = 3;
		int widQue = 50;
		int widAlg = 12;
		int widTim = 12;
		int widCos = 12;
		
	    java.io.PrintWriter pw, firingSequence;
			
	    try {
	    	pw = new java.io.PrintWriter(new FileOutputStream("output/OUTPUT_" + petriNetURLName + "_R" + properties.getRisk() + ".txt"), true);
	    	firingSequence = new java.io.PrintWriter(new FileOutputStream("output/OUTPUT_" + petriNetURLName  + "_R" + properties.getRisk() + "_FS.txt"), true);
				
	    	all = "\n" + 
	        	   widthen("Size", widSize) +
	               widthen("Query            ", widQue) +
	               widthen("Alg    ", widAlg) +
	               widthen("Time  ", widTim) +
	               widthen("Cost  ", widCos);
	    	
	    	fs =  "\n" + widthen("Query            ", widQue) + widthen("FIRING SEQUENCE            ", widQue);

	        pw.print(all);
	        firingSequence.print(fs);
	        
	        for (Query q: kb.getQueries()) {  

	            String ppp = "\n" + widthen(q.getNP(), widSize) + 
	                    widthen(q.getName().replaceAll("http.*#", ""), widQue);
	         

				all = ppp +  
				      widthen("PARIS", widAlg) + 
				      widthen(q.getResults().getTime(), widTim);
				      //widthen(q.getResults().getCost(), widCos) +
				      
				//prueba costo solucion
				ArrayList<Vertice> vertices = q.getResults().getServices();
				
				double solutionCost = 0;
				for(Vertice v:vertices) {
					solutionCost += v.getQoSReal()[0];
				}
				
				if(!q.getResults().getExito())
					solutionCost = -1;
			
				all +=  widthen(solutionCost , widTim);
				//q.getResults().getServices().size()
				pw.print(all);
				
				
				fs = "\n" + widthen(q.getName().replaceAll("http.*#", ""), widQue)
					 
					+ "       " + orderFiringSequence(q.getResults().getServices());
				
				firingSequence.print(fs);
				
	        }
	        pw.flush();
	        firingSequence.flush();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}	       
	}
	 
	private String widthen(String val, int width) {
        String ret = val;

        while (ret.length() < width) {
            ret = " " + ret;
        }

        return ret;
	}
	    
    private String widthen(double val, int width) {
        return widthen(val + "", width);
    }

    private String widthen(int val, int width) {
        return widthen(val + "", width);
    }
    
    private String orderFiringSequence(List<Vertice> services) {
    	String orderedSequence = "";
    	
    	Collections.sort(services,  new Comparator<Vertice>() {

			@Override
			public int compare(Vertice o1, Vertice o2) {
				
				return o1.getName().compareTo(o2.getName());
			} 
    		
    	});
    	
    	for(Vertice v:services)
    		orderedSequence += " " + v.getName();
    	
    	return orderedSequence;
    	
    }
}