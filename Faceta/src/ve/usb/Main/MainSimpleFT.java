package ve.usb.Main;


import java.util.ArrayList;

import mpi.MPI;
import ve.usb.Composer.Composer;
import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.PetriNet;
import ve.usb.PetriNets.Vertice;
import ve.usb.executor.mpi.ExecutionEngineMPI;

public class MainSimpleFT {
	public static void main(String args[]) throws Exception {
		PetriNet mipn = new PetriNet("My PN");
		double qos[] = {0};
		double qos_r[] = {1};
		// S1 
		//Vertice s= new Vertice("WS1",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		Vertice s= new Vertice("WS1",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		ArrayList<Vertice> in = new ArrayList<Vertice>();
		ArrayList<Vertice> ou = new ArrayList<Vertice>();
		//Inputs
		Vertice v = new Vertice("a1",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a2",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a4",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a5",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S2 
		//	s= new Vertice("WS2",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		s= new Vertice("WS2",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a3",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a5",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a6",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S3 
		//s= new Vertice("WS3",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		s= new Vertice("WS3",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a4",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a7",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S4 
		//s= new Vertice("WS4",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		s= new Vertice("WS4",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a5",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a7",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a8",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S5 
		s= new Vertice("WS5",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S5",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a6",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a9",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S6 
		s= new Vertice("WS6",Vertice.TRANSITION,Vertice.COMPENSARET ,qos,qos_r);	
		//s= new Vertice("S6",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a7",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a10",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S7 
		s= new Vertice("WS7",Vertice.TRANSITION, Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S7",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a8",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a11",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S8 
		s= new Vertice("WS8",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S8",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a8",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a9",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a12",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		
		//PetriNet Complete

		System.out.println(mipn.toString());

		//inputs
		ArrayList<Vertice> inp = new ArrayList<Vertice>();
		v = new Vertice("a1",Vertice.PLACE);
		inp.add(v);
		v = new Vertice("a2",Vertice.PLACE);
		inp.add(v);
		v = new Vertice("a3",Vertice.PLACE);
		inp.add(v);
		//outputs
		ArrayList<Vertice> out = new ArrayList<Vertice>();
		v = new Vertice("a10",Vertice.PLACE);
		out.add(v);
		v = new Vertice("a11",Vertice.PLACE);
		out.add(v);
		v = new Vertice("a12",Vertice.PLACE);
		out.add(v);
		double weights[] = {1};
		Query myq = new Query(0,0,inp,out,1,weights);
		
		

		java.util.Date t1 = new java.util.Date();
		if (myq.validate(mipn)) { 
			java.util.Date t2 = new java.util.Date();
			System.out.println("Validation_Time " + (t2.getTime() - t1.getTime()));

			t1 = new java.util.Date();
			MarkedPN myMPN = new MarkedPN("MYMPN"); 
			myMPN.yellowColoring(myq,mipn);
			t2 = new java.util.Date();
			System.out.println("Yellow_Coloring_Time " + (t2.getTime() - t1.getTime()));

			Composer com = new Composer(myMPN,myq);

			myq.setResults(com.run()); 

			System.out.println("****************************");
			System.out.println("***********Executor*********");
			System.out.println("****************************");
			ExecutionEngineMPI exec = new ExecutionEngineMPI(myq, myq.getResults().getWSDN(), args); 
			exec.run();
			//System.out.println("Servicios "+ myq.getResults().getServices());

			/*Exhaustive exhaus = new Exhaustive(myMPN, myq, 5);
			System.out.println("===== EXHAUSTIVE ====== ");
			exhaus.run();*/
		}
		else System.out.println("Query NO valido");	

		/*
	Exhaustive exh = new Exhaustive(myMPN,myq);


        myq.setBestPlan(exh.run());

System.out.println("THE BEST " + myq.getBestPlan());
		 */
		if(MPI.COMM_WORLD.Rank()==0)
			System.out.println("-- THE END --");
	}
}
