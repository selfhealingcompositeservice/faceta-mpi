package ve.usb.Main;

import java.util.ArrayList;
import java.util.List;

import mpi.MPI;
import ve.usb.Composer.Query;
import ve.usb.PetriNets.PetriNet;
import ve.usb.executor.mpi.ExecutionEngineMPI;
import ve.usb.test.data.ExamplePetriNets;

public class MainSimpleMPI {
	public static void main(String args[]) throws Exception {
		List<Query> queries = new ArrayList<Query>();
		MPI.Init(args);
		int me = MPI.COMM_WORLD.Rank();

		if(me==0) {
			PetriNet pn = ExamplePetriNets.getPNSimpleFT();
			System.out.println(pn.toString());
			Query myq = ExamplePetriNets.getQuerySimpleFT();
			
			queries.add(myq);
			
			ComposerWrapper composerWrapper = new ComposerWrapper();
			composerWrapper.runComposer(pn, queries);
			
			for(Query query:queries) {
				ExecutionEngineMPI exec = new ExecutionEngineMPI(query, query.getResults().getWSDN(), args); 
				exec.run();
			}
				
		} else {
			ExecutionEngineMPI exec = new ExecutionEngineMPI(); 
			exec.run();
		}
		System.out.println("-- THE END -- of " + me);
		
		MPI.Finalize();
	}
}
