package ve.usb.Main;


import java.util.List;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.config.ConfigExecutor;
import ve.usb.config.ExecutorRunningProperties;
import ve.usb.executor.thread.ExecutionEngineThread;
import ve.usb.serialization.XMLPNSerializer;
import ve.usb.serialization.XMLQuerySerializer;
import ve.usb.utils.LoadUtils;

public class MainSimpleThread {
	public static void main(String args[]) throws Exception {
		ExecutorRunningProperties runningProperties = ConfigExecutor.loadProperties();
		/*List<Query> queries = new ArrayList<Query>();
		
		PetriNet pn = ExamplePetriNets.getPNSimpleFT();
		System.out.println(pn.toString());
		Query myq = ExamplePetriNets.getQuerySimpleFT();
		queries.add(myq);
		
		ComposerWrapper composerWrapper = new ComposerWrapper();
		composerWrapper.runComposer(pn, queries);*/
		
		//Query query = LoadUtils.loadQueryFromFile(queryResultList.get(0));
		Query query = (new XMLQuerySerializer()).XMLtoPN(args[0]);
		
		//MarkedPN mp = query.getResults().getWSDN();
		MarkedPN mp = (new XMLPNSerializer()).XMLtoPN(args[1]);
		

		ExecutionEngineThread exec = new ExecutionEngineThread(query,mp, runningProperties.getServiceExecutionFailure()); 
		exec.run();

	}	
}
