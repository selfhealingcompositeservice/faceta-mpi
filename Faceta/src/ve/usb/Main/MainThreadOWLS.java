package ve.usb.Main;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.config.RunningProperties;
import ve.usb.executor.EngineThreadResult;
import ve.usb.executor.thread.EngineThreadThread;
import ve.usb.executor.thread.ExecutionEngineThread;
import ve.usb.serialization.XMLPNSerializer;
import ve.usb.serialization.XMLQuerySerializer;
import ve.usb.utils.LoadUtils;
import ve.usb.utils.ResultsPrinter;

public class MainThreadOWLS {
	
	private RunningProperties properties;
	
	public static void main(String args[]) throws InterruptedException, ExecutionException {
		Logger logger = Logger.getLogger(MainThreadOWLS.class);
		
		BasicConfigurator.configure();
		logger.info("Starting application");
		
		if(args.length < 1) {
			System.out.println("Wrong number of arguments");
			return;
		}
	
		//Load Query
		//Query query = LoadUtils.loadQueryFromFile("results/"+args[0]);
		Query query = (new XMLQuerySerializer()).XMLtoPN(args[0]);
		//MarkedPN mp = query.getResults().getWSDN();	
		MarkedPN mp = (new XMLPNSerializer()).XMLtoPN(args[1]);
		//equivalent services
		/*ServiceEquivalence seq = 
			new ServiceEquivalence(
					mp.getTransitions().get(0), 
					new URI("http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario1/BA200_3_4/"));
		seq.getEquivalents();*/
		//KnowledegeBase kb = new KnowledegeBase(new URI("http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario1/BA200_3_4/"), 0);
		//kb.setEquivalentServices(query.getResults().getWSDN());
		
		java.util.Date t1 = new java.util.Date();
		EngineThreadThread.reachedCompensation = 0;
		//ExecutionEngineThread.queues = null;
		ExecutionEngineThread executor = new ExecutionEngineThread(query, mp); 

		//executor.setFtMechanism(MainMPIOWLS.getFtMechanism(args));
		
		List<EngineThreadResult> results = executor.run();
		
		java.util.Date t2 = new java.util.Date();
	    
	    double executionTime = t2.getTime() - t1.getTime();
	    
	    //(new ResultsPrinter()).printExecutionResults(query, executionTime, 0, results, "SM", 0 , 0 ,0);
	    
	    
	}
	
}
