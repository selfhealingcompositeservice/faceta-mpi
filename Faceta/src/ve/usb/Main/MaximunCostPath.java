package ve.usb.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;

public class MaximunCostPath {
	
	private MarkedPN pn;
	
	public MaximunCostPath(MarkedPN pn) {
		this.pn = pn;
	}
	
	Map<String,Double> maximumCostPathFromNode = new HashMap<String, Double>();
			
	public int getMaximumCostPath() {
		
		Vertice wsee_i, wsee_f;

		//WSee_i
		wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
		pn.addWebService(wsee_i, new ArrayList<Vertice>(), pn.getInputs());
		
		//WSee_f
		wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
		pn.addWebService(wsee_f, pn.getOutputs(), new ArrayList<Vertice>());
		
		//Build the graph
		
		double totalSequential = 0;
		for(Vertice v:pn.getTransitions()) {
			
			maximumCostPathFromNode.put(v.getName(), new Double(0));
			
			if(!v.getName().equals(wsee_i.getName()) && !v.getName().equals(wsee_f.getName())) {
				System.out.println("v: " + v.getName() + ", cost: " + v.getQoSReal()[0]);
				totalSequential += v.getQoSReal()[0];
			}
		}
		
		System.out.println("Total Sequential: " + totalSequential);

		System.out.println();
		System.out.println("All paths");
		System.out.println();

		AllPaths(pn, wsee_i, wsee_f);
		
		int pathCost;
		int maxCostPath = 0;
		for(List<Vertice> p:allPaths) {
			
			Map<String,Double> costFromNode = new HashMap<String, Double>();
			
			pathCost = 0;
			for(Vertice v:p) {
				if(!v.getName().equals(wsee_i.getName()) && !v.getName().equals(wsee_f.getName())) {
					pathCost += v.getQoSReal()[0];
					
					for(String k:costFromNode.keySet())
						costFromNode.put(k, costFromNode.get(k) + v.getQoSReal()[0]);
					
					costFromNode.put(v.getName(), v.getQoSReal()[0]);
				}
			}
			
			System.out.println(pathCost + "  ---> " + p);
			
			if(pathCost > maxCostPath)
				maxCostPath = pathCost;
			
			for(String k:costFromNode.keySet())
				if(costFromNode.get(k) > this.maximumCostPathFromNode.get(k))
					this.maximumCostPathFromNode.put(k, costFromNode.get(k));
			
		}
		
		pn.deleteVertice(wsee_i.getName());
		pn.deleteVertice(wsee_f.getName());
		
		System.out.println("  Cost From Nodes CFN ");
		
		System.out.println(this.maximumCostPathFromNode);
		System.out.println("  ******************* ");
		
		return maxCostPath;
	}
	

	
	private  static Stack<Vertice> path  = new Stack<Vertice>();   // the current path
    private  static List<Vertice> onPath  = new ArrayList<Vertice>();     // the set of vertices on the path
    
    private  static List<List<Vertice>> allPaths = new ArrayList<List<Vertice>>();

    public static void AllPaths(MarkedPN pn, Vertice i, Vertice f) {
        enumerate(pn, i , f);
    }

    // use DFS
    private static void enumerate(MarkedPN pn, Vertice v, Vertice t) {

        // add node v to current path from s
        path.push(v);
        onPath.add(v);

        // found path from s to t - currently prints in reverse order because of stack
        if (v.getName().equals(t.getName())) 
        	allPaths.add(new ArrayList<Vertice>(path));

        // consider all neighbors that would continue path with repeating a node
        else {
            for (Vertice w : pn.getSucTransitions(v)) {
                if (!onPath.contains(w)) enumerate(pn, w, t);
            }
        }

        // done exploring from v, so remove from path
        path.pop();
        onPath.remove(v);
    }

	public static void multipleQosBy(double i, MarkedPN mpn) {
		for(Vertice v:mpn.getTransitions()) 
			v.setQoS(0, v.getQoSReal()[0]*i);
		
	}

	public Map<String, Double> getMaximumCostPathFromNode() {
		return maximumCostPathFromNode;
	}
	
	

}
