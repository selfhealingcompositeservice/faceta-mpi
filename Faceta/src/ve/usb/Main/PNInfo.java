package ve.usb.Main;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import ve.usb.PetriNets.PetriNet;
import ve.usb.PetriNets.Vertice;
import ve.usb.utils.ResultsPrinter;

/**
 * Shows info of a Petri Net
 *
 * @author Rafael Angarita
 * 
 */
public class PNInfo {
	//Normalizacion : ((valor mayor - Valor del s)/ valor mayor)
	public static void main(String...args) throws MalformedURLException {
		System.out.println("Petri Net Info Utility");
		URL url = new URL("http://ldc.usb.ve/~rangarita/Ontologies/Graphs/Transac/Escenario2/BA800_3_8/output/BA800_3_8");
		String fileName = url.getPath();

		System.out.println("File name: " + fileName);
		PetriNet petriNet = loadPetriNetFromFile(url);
		System.out.println("PN loaded");
		System.out.println("Number of Transitions: " + petriNet.getTransitions().size());
		System.out.println("Max Real Cost: " + getMaxRealCost(petriNet.getTransitions()));
		System.out.println("************************************************************");
		System.out.println("*********************Transitions****************************");
		System.out.println("************************************************************");
		int conecctions = 0;
		int totalInputs = 0;
		int totalOutputs = 0;
		for(Vertice v:petriNet.getTransitions()) {
			System.out.println("-- Name: " + v.getName() + "(" + ResultsPrinter.getTransactionalProperty(v.getTransactionalProperty())  + ")");
			System.out.println("-- Real Cost: " + v.getQoSReal()[0]);
			System.out.println("-- Normalized Cost: " + v.getQoS()[0]);
			System.out.println("-- Quality: " + v.getQuality());
			System.out.println("-- Cost: " + v.getCost());
			System.out.println("-- Color: " + v.getColor());
			System.out.println("-- Connections: " + (petriNet.getPredTransitions(v).size() + petriNet.getSucTransitions(v).size()));
			System.out.println("-- Outputs: " + petriNet.sucesores(v).size());
			System.out.println("-- Suc: " + petriNet.getSucTransitions(v).size());
			System.out.println("-- Pred: " + petriNet.getPredTransitions(v).size());
			conecctions += petriNet.getPredTransitions(v).size();
			conecctions += petriNet.getSucTransitions(v).size();
			totalInputs += petriNet.predecesores(v).size();
			totalOutputs += petriNet.sucesores(v).size();
		}
		

		System.out.println("Connection Avg " + conecctions/petriNet.getTransitions().size());
		System.out.println("Input Avg " + totalInputs/petriNet.getTransitions().size());
		System.out.println("Oputput Avg " + totalOutputs/petriNet.getTransitions().size());
		System.out.println("*********************END PN****************************");
		System.out.println(petriNet.toString());
	}
	
	static double getMaxRealCost(List<Vertice> transitions) {
		
		double maxValue = 0;
		
		for(Vertice v:transitions) {
			if(v.getQoSReal()[0] > maxValue) {
				maxValue = v.getQoSReal()[0];
			}
			
		}
		
		return maxValue;
	}
	
    protected static PetriNet loadPetriNetFromFile(String name) {
    	
    	PetriNet petriNet = null;
    	ObjectInputStream in = null;
        try {
	        in = new ObjectInputStream( new FileInputStream(name));
	        petriNet = (PetriNet)in.readObject();
	        in.close();
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    	
        return petriNet;
    }
    
protected static PetriNet loadPetriNetFromFile(URL url) {
    	
    	PetriNet petriNet = null;
    	ObjectInputStream in = null;
        try {
	        in = new ObjectInputStream( url.openStream());
	        petriNet = (PetriNet)in.readObject();
	        in.close();
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    	
        return petriNet;
    }
}
