package ve.usb.Main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.utils.GraphViz;
import ve.usb.utils.LoadUtils;
import ve.usb.utils.ResultsPrinter;

/**
 * Prints a Petri Net to a file using the GraphViz utility
 *
 * @author Rafael Angarita
 * 
 */
public class PrintPNGraphViz
{
   public static void main(String[] args)
   {
      PrintPNGraphViz p = new PrintPNGraphViz();
      p.start();
//      p.start2();
   }

   /**
    * Construct a DOT graph in memory, convert it
    * to image and store the image in the file system.
    */
   private void start()
   {
	   List<Query> queryResultList = LoadUtils.loadQueryList("output/queries.faceta");
	   MarkedPN mp = null;
	   
	   for(Query query:queryResultList) {
			if(query.getName().equals("Q-Transac-Escenario2-BA800_3_8-NP-10-IND-09")){
				mp = query.getResults().getWSDN();
			}
	   }
				
	   //String path = "results/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-10";
	   //Query query = LoadUtils.loadQueryFromFile(path);

		
		//MarkedPN mp = query.getResults().getWSDN();
		//WSee_i
		Vertice wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
		mp.addWebService(wsee_i, new ArrayList<Vertice>(), mp.getInputs());
		
		//WSee_f
		Vertice  wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
		mp.addWebService(wsee_f, mp.getOutputs(), new ArrayList<Vertice>());
	   
      GraphViz gv = new GraphViz();
      gv.addln(gv.start_graph());
      /*gv.addln("A -> B;");
      gv.addln("A -> C;");
      gv.addln("C -> B;");*/
      
 
      for(Vertice v:mp.getTransitions()) {
    	  
    	  for(Vertice s:mp.getSucTransitions(v)) {
    		  if(!(v.getName().equals(wsee_i.getName()) && s.getName().equals(wsee_f.getName()))) {
	    		  String n1 = ResultsPrinter.getVertexNameNumber(v);
	    		  String n2 = ResultsPrinter.getVertexNameNumber(s);
	    		  gv.addln(n1 + " -> " + n2);
	    		  System.out.println("Adding: " + n1 + " -> " + n2);
    		  }
    		  
    	  }
    	  
      }
      
      
      
      gv.addln(gv.end_graph());
      System.out.println(gv.getDotSource());
      
//      String type = "gif";
//      String type = "dot";
//      String type = "fig";    // open with xfig
//      String type = "pdf";
//      String type = "ps";
//      String type = "svg";    // open with inkscape
      String type = "png";
//      String type = "plain";
      File out = new File("pn" + "." + type);   // Linux
//      File out = new File("c:/eclipse.ws/graphviz-java-api/out." + type);    // Windows
      gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), out );
   }
   
   /**
    * Read the DOT source from a file,
    * convert to image and store the image in the file system.
    */
   @SuppressWarnings("unused")
private void start2()
   {
      String dir = "/home/jabba/eclipse2/laszlo.sajat/graphviz-java-api";     // Linux
      String input = dir + "/sample/simple.dot";
//	   String input = "c:/eclipse.ws/graphviz-java-api/sample/simple.dot";    // Windows
	   
	   GraphViz gv = new GraphViz();
	   gv.readSource(input);
	   System.out.println(gv.getDotSource());
   		
      String type = "gif";
//    String type = "dot";
//    String type = "fig";    // open with xfig
//    String type = "pdf";
//    String type = "ps";
//    String type = "svg";    // open with inkscape
//    String type = "png";
//      String type = "plain";
	   File out = new File("/tmp/simple." + type);   // Linux
//	   File out = new File("c:/eclipse.ws/graphviz-java-api/tmp/simple." + type);   // Windows
	   gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), out );
   }
}
