package ve.usb.Main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.utils.LoadUtils;
import ve.usb.utils.ResultsPrinter;

public class QueryResultsInfo {
	
	static String BA200_3_4 = "../saved_data/query_results/BA200_3_4/";
	static String BA200_3_4_R0 = "../saved_data/query_results/BA200_3_4/r0/";
	static String BA800_3_8 = "../saved_data/query_results/BA800_3_8/";
	static String BA200_3_8 = "";
	
	static String[] BA200_3_4_LIST_10 = {"Q-Transac-Escenario1-BA200_3_4-NP-10-IND-01","Q-Transac-Escenario1-BA200_3_4-NP-10-IND-02",
		"Q-Transac-Escenario1-BA200_3_4-NP-10-IND-03","Q-Transac-Escenario1-BA200_3_4-NP-10-IND-04","Q-Transac-Escenario1-BA200_3_4-NP-10-IND-05"
		,"Q-Transac-Escenario1-BA200_3_4-NP-10-IND-06","Q-Transac-Escenario1-BA200_3_4-NP-10-IND-07","Q-Transac-Escenario1-BA200_3_4-NP-10-IND-08"
		,"Q-Transac-Escenario1-BA200_3_4-NP-10-IND-09", "Q-Transac-Escenario1-BA200_3_4-NP-10-IND-10"};
	
	static String[] BA200_3_8_LIST_10 = {"Q-Transac-Escenario2-BA200_3_8-NP-10-IND-01","Q-Transac-Escenario2-BA200_3_8-NP-10-IND-02",
		"Q-Transac-Escenario2-BA200_3_8-NP-10-IND-03","Q-Transac-Escenario2-BA200_3_8-NP-10-IND-04","Q-Transac-Escenario2-BA200_3_8-NP-10-IND-05"
		,"Q-Transac-Escenario2-BA200_3_8-NP-10-IND-06","Q-Transac-Escenario2-BA200_3_8-NP-10-IND-07","Q-Transac-Escenario2-BA200_3_8-NP-10-IND-08"
		,"Q-Transac-Escenario2-BA200_3_8-NP-10-IND-09", "Q-Transac-Escenario2-BA200_3_8-NP-10-IND-10"};
	
	static String[] BA800_3_8_LIST_10 = {"Q-Transac-Escenario2-BA800_3_8-NP-10-IND-01","Q-Transac-Escenario2-BA800_3_8-NP-10-IND-02",
		"Q-Transac-Escenario2-BA800_3_8-NP-10-IND-03","Q-Transac-Escenario2-BA800_3_8-NP-10-IND-04","Q-Transac-Escenario2-BA800_3_8-NP-10-IND-05"
		,"Q-Transac-Escenario2-BA800_3_8-NP-10-IND-06","Q-Transac-Escenario2-BA800_3_8-NP-10-IND-07","Q-Transac-Escenario2-BA800_3_8-NP-10-IND-08"
		,"Q-Transac-Escenario2-BA800_3_8-NP-10-IND-09", "Q-Transac-Escenario2-BA800_3_8-NP-10-IND-10"};
	
	
	
	static Vertice wsee_i, wsee_f;

	public static void main(String...args) throws IOException {
		
		int widSize = 5;
		int widQue = 50;
		int widDis = 5;
		
		List<String> qrs = new ArrayList<String>();
		
		/*for(int i=0; i<BA200_3_4_LIST_10.length; i++) {
			qrs.add(BA200_3_4 + BA200_3_4_LIST_10[i]);
		}
		
		for(int i=0; i<BA200_3_4_LIST_10.length; i++) {
			qrs.add(BA200_3_4_R0 + BA200_3_4_LIST_10[i]);
		}*/
		
		/*for(int i=0; i<BA200_3_8_LIST_10.length; i++) {
			qrs.add(BA200_3_8 + BA800_3_8_LIST_10[i]);
		}
		/*
		for(int i=0; i<BA800_3_8_LIST_10.length; i++) {
			qrs.add(BA800_3_8 + BA800_3_8_LIST_10[i]);
		}*/
		
		qrs.add("results/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-06");
		qrs.add("results/Q-Transac-Escenario2-BA800_3_8-NP-10-IND-07");
		String line = "";
		File f = new File("query_results.info");
		
		FileWriter pw = new FileWriter(f, false);
		
		line =
			  widthen("Size", widSize)
			+ widthen("Risk", widSize)
			+ widthen("Query", widQue)
            + widthen("MDTI", widDis)
            + widthen("MDTF", widDis)
            + widthen("(c)", widDis) 
            + widthen("(r)", widDis) 
            + widthen("(a)", widDis); 
		
		pw.write(line);
		
		for(String path:qrs) {
			//Load Query
			Query query = LoadUtils.loadQueryFromFile(path);
			
			MarkedPN mp = query.getResults().getWSDN();	
			
			ArrayList uniqueVals = new ArrayList();
			 
			for (Vertice x : mp.getPlaces())
			    if (!uniqueVals.contains(x))
			        uniqueVals.add(x);
			
			System.out.println("Numero de places: " + uniqueVals.size());
			
			//WSee_i
			wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
			mp.addWebService(wsee_i, new ArrayList<Vertice>(), query.getIns());
			
			//WSee_f
			wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
			mp.addWebService(wsee_f, query.getOuts(), new ArrayList<Vertice>());
			
			int mdti = 0;
			int mdto = 0;
			int ncs = 0;
			int nrs = 0;
			int a = 0;
			
			Graph<String, DefaultEdge> g = ResultsPrinter.getGraph(query.getResults().getWSDN());
			
			for(Vertice v:mp.getTransitions()) {
				System.out.println(v.getName());
				if(!v.getName().equals(wsee_i.getName()) && !v.getName().equals(wsee_f.getName())) {
				/*	int mdtitmp = (int) (new DijkstraShortestPath<String, DefaultEdge>(g, wsee_i.getName(), v.getName())).getPathLength();
					int mdtotmp = (int) (new DijkstraShortestPath<String, DefaultEdge>(g, v.getName(), wsee_f.getName())).getPathLength();
					
					if(mdtitmp > mdti)
						mdti = mdtitmp;
					if(mdtotmp > mdto)
						mdto = mdtotmp;
					*/
					if(v.isCompensa() || v.isCompensaRet())
						ncs++;
					
					if(v.isCompensaRet() || v.isPivotRet() || v.isAtomicRet())
						nrs++;
					if( v.isPivotRet() || v.isAtomicRet())
						a++;
					
				}
				
			}
			
			
			
			
			
			line =  "\n" +
				  widthen(mp.getNumProcecsos()-2, widSize)
				+ widthen(query.getRisk(), widSize)
				+ widthen(mp.getName(), widQue)
	            + widthen(mdti, widDis)
	            + widthen(mdto, widDis)
	            + widthen(ncs, widDis) 
	            + widthen(nrs, widDis) 
	            + widthen(a, widDis) 
	           
	        ; 
			
			pw.write(line);
		}
		pw.close();
		
	
	}
	
	private static String widthen(String val, int width) {
        String ret = val;

        while (ret.length() < width) {
            ret = " " + ret;
        }

        return ret;
	}
	    
    private static String widthen(double val, int width) {
        return widthen(val + "", width);
    }

    private static String widthen(int val, int width) {
        return widthen(val + "", width);
    }
}
