package ve.usb.Main;

import java.util.List;

import ve.usb.Composer.Query;
import ve.usb.utils.LoadUtils;

public class ResultsInfo {
	//Normalizacion : ((valor mayor - Valor del s)/ valor mayor)
	public static void main(String...args) {
		System.out.println("Results Info Utility");
		String fileName = "BA200_3_4" + "_results_r_" + 0;
		System.out.println("File name: " + fileName);
		List<Query> queryList = LoadUtils.loadQueryList(fileName);
		System.out.println("List<Query> loaded");
		System.out.println("Number of queries: " + queryList.size());
		
		int widQue = 50;
		int widCos = 12;
		int widNum = 7;
		
		String info = widthen("Name", widQue) + widthen("Size", widCos);
		
		
	
		
		for(Query q:queryList) {
			info = widthen(q.getName() , widQue) + widthen(new Integer(q.getResults().getWSDN().getNumProcecsos()).toString(), widCos);
			System.out.println(info);
		}
		

		System.out.println();
		System.out.println("*********************Total****************************");
		info = widthen("Size", widNum) + widthen("Total", widNum);
		System.out.println(info);
		
		int size = 0;
		int n = 0;
		
		for(Query q:queryList) {
			if(q.getResults().getWSDN().getNumProcecsos() != size) {
				if(size != 0) {
					info = widthen(new Integer(size).toString() , widNum) + widthen(new Integer(n).toString(), widNum);
					System.out.println(info);
				}
				n = 1;
				size = q.getResults().getWSDN().getNumProcecsos();
			} else {
				n++;
			}
			
		}

	}
	
	private static String widthen(String val, int width) {
        String ret = val;

        while (ret.length() < width) {
            ret = " " + ret;
        }

        return ret;
	}
	
}
