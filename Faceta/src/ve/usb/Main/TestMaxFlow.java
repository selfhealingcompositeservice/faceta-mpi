package ve.usb.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.graph.EdmondsKarp;
import ve.usb.graph.FlowNetwork;
import ve.usb.graph.Vertex;
import ve.usb.utils.LoadUtils;
import org.jgrapht.*;
import org.jgrapht.alg.BellmanFordShortestPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.alg.EdmondsKarpMaximumFlow;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class TestMaxFlow {
	
	private Vertice wsee_i, wsee_f;

	public TestMaxFlow() {
		// TODO Auto-generated constructor stub
	}
	
	public void run() {
		List<Query> queryResultList = LoadUtils.loadQueryList("output/queries.faceta");
		System.out.println("Query List: " + queryResultList.size());
		
		for(Query query:queryResultList) {
			if(query.getName().equals("Q-Transac-Escenario2-BA800_3_8-NP-10-IND-09")){
				MarkedPN mp = query.getResults().getWSDN();
				//WSee_i
				wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
				mp.addWebService(wsee_i, new ArrayList<Vertice>(), mp.getInputs());
				
				//WSee_f
				wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
				mp.addWebService(wsee_f, query.getOuts(), new ArrayList<Vertice>());
				
				runEdmonsKarp(mp);
			}
		}
	}

	private void runEdmonsKarp(MarkedPN mp) {
		
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph = new SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
		
		for(Vertice v:mp.getTransitions())
			graph.addVertex(v.getName());
		
		for(Vertice v:mp.getTransitions())
			for(Vertice vs:mp.getSucTransitions(v)) {
				DefaultWeightedEdge e1 = graph.addEdge(v.getName(), vs.getName());
				double cost = 0;
				if(vs.getName().compareTo(wsee_i.getName()) != 0 && vs.getName().compareTo(wsee_f.getName()) != 0)
					cost = -vs.getQoSReal()[0];
				graph.setEdgeWeight(e1, cost);
			}
		
		
		
		//System.out.println(DijkstraShortestPath.findPathBetween(graph, wsee_i.getName(), wsee_f.getName()));
		List<DefaultWeightedEdge> list = BellmanFordShortestPath.findPathBetween(graph, wsee_i.getName(), wsee_f.getName());
		int cost = 0;
		for(DefaultWeightedEdge e:list)
			cost+=graph.getEdgeWeight(e);
		System.out.println(list);
		System.out.println("cost=" + cost);
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		(new TestMaxFlow()).run();

	}

}
