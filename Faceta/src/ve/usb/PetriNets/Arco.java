/*
 * Arco.java
 *
 * Created on 20 de enero de 2010, 02:49 PM
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.PetriNets;


//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

public class Arco implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String           name; // Arc name (vi->vf)
    private String           vf;  //name of final vertice
    private String           vi;  //name of initial vertice
    private int	             replica; //Replica number, related to input replicas

  
    /**
     * Creates a new instance of Arco
     */
    public Arco(String v1, String v2) {
        vi   = v1;
        vf   = v2;
        name = getCanonicalName(v1, v2);
        replica = 0;
    }

    public Arco(String v1, String v2, int r) {
        vi   = v1;
        vf   = v2;
        name = getCanonicalName(v1, v2);
        replica = r;
    }

    public Arco clone() {
        Arco n = new Arco(vi, vf);
        n.vi = new String(vi);
        n.vf = new String(vf);
        n.name = new String(name);        n.replica = replica;

        return n;
    }

    public static String getCanonicalName(String v1, String v2) {
        return v1 + "->" + v2;
    }

    public String getName() {
        return name;
    }

    public String  getVerticeIni() {
        return vi;
    }

    public String getVerticeFin() {
        return vf;
    }

    public int getReplica() {
        return replica;
    }

    public void setReplica(int r) {
        replica = r;
    }

    public void setVerticeIni(String v) {
        vi = v;
        name = getCanonicalName(vi, vf);
    }

    public void setVerticeFin(String v) {      
        vf = v;
        name = getCanonicalName(vi, vf);
    }

    public String toString() {
        return "(" + vi.trim().replaceAll("http.*#", "#") + ", " + vf.trim().replaceAll("http.*#", "#") + ")";
    }
}
