/*
 * Camino.java
 *
 * Created on 20 de enero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.PetriNets;

import java.util.ArrayList;

public class Camino {
    ArrayList<Vertice> lista; // list of vertices in the Path (Camino = Path :)
    
    /** Creates a new instance of Camino */
    
    public Camino() {
        lista = new ArrayList<Vertice>();
    }
         
    public Camino(Vertice v) {
        this();
        lista.add(v);
    }
     
    public void add(Vertice v) {
        lista.add(v);
    }

    public boolean equals(Camino c) {
        if (size() != c.size())
            return false;
        
        for (int i = 0; i < size(); i++) {
            if ((lista.get(i).compareTo(c.lista.get(i))) != 0)
                return false;
        }
        
        return true;
    }
    
    public Vertice getLast() {
        if (lista.size() == 0)
            return null;
        
        return lista.get(lista.size()-1);
    }
    
    public Vertice getFirst() {
        if (lista.size() == 0)
            return null;
        
        return lista.get(0);
    }

    public int size() {
        return lista.size();
    }
    
    public Camino clone() {
        Camino c = new Camino();
        
        c.lista.addAll(lista);
        
        return c;
    }

    
    public Camino expand(Vertice v) {
        Camino c = this.clone();
        
        c.add(v);
        
        return c;
    }
    
    public String toString() {
        String ret = new String();
        
        for (Vertice v : this.lista) {
            ret += v + ", ";
        }
        
        return ret;
    } 
}
