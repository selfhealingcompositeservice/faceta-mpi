/*
 * MarkedPN.java
 *
 * Created on 29 de enero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.PetriNets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ve.usb.Composer.Query;

/**
 * Implement a Marked PetriNet according to transaction properties and inputs and outputs of the Query
 * All Composer actions are implemented.
 *
 */

public class MarkedPN extends PetriNet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*************************** Atributos ************************************/

	// Contain the URIS of the attributtes (we are not using uris for the moment.
	// private TreeSet<String> inputs;   /*Inputs */
	//private TreeSet<String> outputs;  /*Outputs */

	private int colorMarked;          /* Current Color of the PN state  

    /************************** Metodos ***************************************/   

	/** Creates a new instance of MarkedPN */
	public MarkedPN(String name) {
		//   this.inputs = new TreeSet<String>();
		//   this.outputs = new TreeSet<String>(); 
		super(name);
		this.colorMarked = 0;  
	}

	public MarkedPN(MarkedPN mpn) {
		//   this.inputs = new TreeSet<String>();
		//   this.outputs = new TreeSet<String>(); 
		super(mpn.getName());
		this.colorMarked = mpn.getMarked();
		this.transitions  = mpn.getTransitions();
		this.places       = mpn.getPlaces();
		this.arcos        = mpn.getArcos();   

	}

	/** Creates a new instance of MarkedPN */
	public MarkedPN(String name, ArrayList<Vertice> outinit) {
		//   this.inputs = new TreeSet<String>();
		//   this.outputs = new TreeSet<String>(); 
		super(name);
		this.colorMarked = 0;  
		setPlaces(outinit);
	}

	/** Initilize the Marking with Query Outputs (for yellow coloring) */ 
	public void setIniOuts(ArrayList<Vertice> o) {
		setPlaces(o);
	}

	public void setMarkedColor(int color) {
		colorMarked = color;
	}

	public int getMarked() {
		return colorMarked;
	}

	/**
	 * Implementation of Algorithm 3: Identification of the Potentially useful WSs (inspired by SAM Yellow coloring)
	 **/

	public void yellowColoring(Query q, PetriNet wsdn) {
		ArrayList<Vertice> predec = new ArrayList<Vertice>();
		ArrayList<Vertice> temp = new ArrayList<Vertice>();
		Vertice a;
		ArrayList<Vertice> pda = new ArrayList<Vertice>();
		ArrayList<Vertice> sda = new ArrayList<Vertice>();
		ArrayList<Vertice> pdatoadd = null;
		Arco ar;
		ArrayList<Vertice> srq = q.getSRq();

		if (q.getRisk() == Query.R0) { //only transition c and cr are considered
			if (places.size() == 0) setIniOuts(q.getOuts()); //The yellow coloring start with Query outputs
			//Adding Transitions which produce Query Outputs

			for (int i=0;i<places.size();i++) {
				temp = wsdn.predecesores(places.get(i));
				for (int j=0;j<temp.size();j++) {
					if (srq.contains(temp.get(j))) {		
						addT(temp.get(j));
						ar = new Arco(temp.get(j).getName(),places.get(i).getName());
						add(ar);//Add arc F(Si,o)
						if (!predec.contains(temp.get(j)))
							predec.add(temp.get(j));   //Transitions which produce Query outputs are the first predecesors
					}
				}
			}

			while (predec.size()!=0) {
				pda.clear();
				a = predec.get(0);
				pda = wsdn.predecesores(a);

				if (pda.size()!=0) {
					if (a.getType() == Vertice.TRANSITION) {
						if (srq.contains(a)) { // a is a Transitions
							pdatoadd=addPlaces(pda,a); // add all its predecessors and its arcs			
						} 
					}
					else { //a is a Place
						pdatoadd=addTransitions(pda,a,srq); // add all its predecessors and its arcs
					}		    
					predec.addAll(pdatoadd);
				}
				predec.remove(a);
				//System.out.println("TODOS LOS PREDEC" + predec);
			}
			//System.out.println(toString());
			//System.out.println("INPUTS " + getInputs());
		}
		else { //risk=R1, all transitions are allowed
			ArrayList<Vertice> RO = new ArrayList<Vertice>();
			ArrayList<Integer> MO = new ArrayList<Integer>();
			RO= (ArrayList<Vertice>) q.getOuts().clone();
			for (int i=0;i<RO.size();i++) {
				temp.clear();
				Vertice o = RO.get(i);

				for (int c = 0; c< places.size();c++) {
					places.get(c).removeColors(); 
				}
				setMarkage(o,"ALL");
				addP(o);
				temp = wsdn.predecesores(o);
				for (int j=0;j<temp.size();j++) {
					if (srq.contains(temp.get(j))) {		
						if (!predec.contains(temp.get(j)))
							predec.add(temp.get(j));
					}
				}
				while (predec.size()!=0) {
					pda.clear();
					a = predec.get(0);
					pda = wsdn.predecesores(a);
					sda = wsdn.sucesores(a);
					if (a.getType() == Vertice.TRANSITION) { // is Transition
						if (checkColor(a,sda)) {
							pdatoadd=addTransitionwithC(sda,pda,a);
						}
						else pdatoadd.clear();
					} 
					else { // is a place
						pdatoadd=addPlacewithC(sda,pda,a);
					}
					predec.addAll(pdatoadd); 
					predec.remove(a);
					//System.out.println(" PLACE  " + a + " TODOS LOS PREDEC" + predec);
					//System.out.println(toString());

				}
			}
		} //Fin del else (RQ=R1)
		//	ArrayList<Vertice> res_inputs = getInputs();
		ArrayList<Vertice> q_inputs = q.getIns();
		boolean ret=false;
		for (int i=0; i< q_inputs.size();i++) {
			//	    for (int j =0; j< res_inputs.size();j++ ) {
			for (int j =0; j< places.size();j++ ) {
				//	    	if (res_inputs.get(j).getName().compareTo(q_inputs.get(i).getName())==0) {
				if (places.get(j).getName().compareTo(q_inputs.get(i).getName())==0) {
					ret=true;
					break;
				}
			}
			if (ret) break;
		}
		if (!ret) {
			places.clear();
			transitions.clear();
			arcos.clear();
		}
	} //Fin metodo


	/**
	 * Implement MarkedPN clonation
	 **/
	public MarkedPN clone(){
		MarkedPN clon = new MarkedPN(this.name);
		clon.clone();
		clon.colorMarked = this.colorMarked;
		return  clon;
	}

	public ArrayList<Vertice> addPlaces(ArrayList<Vertice> ps, Vertice v) {
		ArrayList<Vertice> pret = new ArrayList<Vertice>();
		for (int i=0;i<ps.size();i++) {
			Vertice pl = ps.get(i);
			if (!perteneceP(pl)) {
				pl.setReplica(0);
				addP(pl); 
				//System.out.println("No Replicado Input" + pl.getReplica());
				pret.add(pl);
				Arco a = new Arco(pl.getName(),v.getName(),pl.getReplica());
				add(a);
			} 
			else { //ADD REPLICA
				Vertice pclone= new Vertice(pl.getName(), Vertice.PLACE);
				int r=getLastReplica(pl);
				r++;
				pclone.setReplica(r);
				addP(pclone);	
				pret.add(pclone);
				// add Arcs to Replica;
				Arco a = new Arco(pclone.getName(),v.getName(),pclone.getReplica());
				add(a);

			}

		}
		return pret;
	}  

	public ArrayList<Vertice> addTransitions(ArrayList<Vertice> ps, Vertice v, ArrayList<Vertice> srq) {
		ArrayList<Vertice> pret = new ArrayList<Vertice>();

		for (int i=0;i<ps.size();i++) {
			Vertice tr = ps.get(i);
			if (srq.contains(tr)) {
				Arco a = new Arco(tr.getName(),v.getName(),v.getReplica());
				add(a);
				//System.out.println("Arco en AT" + tr.getName() + " " + v.getName() + " " + ps.size());
				if (!perteneceT(tr)) {
					addT(tr); 
					pret.add(tr);
				}
			}
		}
		return pret;
	}  
	
	
	//RAFA
	public ArrayList<Vertice> addTransitions(ArrayList<Vertice> ps, Vertice v) {
		ArrayList<Vertice> pret = new ArrayList<Vertice>();

		for (int i=0;i<ps.size();i++) {
			Vertice tr = ps.get(i);
			if(tr.isCompensa() || tr.isCompensaRet()) {
				Arco a = new Arco(tr.getCompensationTransition().getName(),v.getName(),v.getReplica());
				add(a);
				
				if (!perteneceT(tr.getCompensationTransition())) {
					addT(tr.getCompensationTransition());  
					pret.add(tr);
				}
			}
	
		}
		return pret;
	}  
	
	//RAFA
	public ArrayList<Vertice> addPlacesInv(ArrayList<Vertice> ps, Vertice v) {
		ArrayList<Vertice> pret = new ArrayList<Vertice>();
		for (int i=0;i<ps.size();i++) {
			Vertice pl = ps.get(i);
			if (!perteneceP(pl)) {
				pl.setReplica(0);
				addP(pl); 
				//System.out.println("No Replicado Input" + pl.getReplica());
				pret.add(pl);
				Arco a = new Arco(pl.getName(), v.getCompensationTransition().getName(), pl.getReplica());
				add(a);
			} 
			else { //ADD REPLICA
				Vertice pclone= new Vertice(pl.getName(), Vertice.PLACE);
				int r=getLastReplica(pl);
				r++;
				pclone.setReplica(r);
				addP(pclone);	
				pret.add(pclone);
				// add Arcs to Replica;
				Arco a = new Arco(pclone.getName(), v.getCompensationTransition().getName(), pclone.getReplica());
				add(a);

			}

		}
		return pret;
	}  
	private Map<String,String> brMapping;
	
	public String getCompensationTransition(String name) {
		return brMapping.get(name);
	}

	//RAFA
	public MarkedPN createBRPN() {
		MarkedPN pn = new MarkedPN("BR_" + this.name);
		brMapping = new HashMap<String, String>();
		for(Vertice t:this.getTransitions()) {
			if(t.isCompensa() || t.isCompensaRet()) { 
				
				String cName = t.getName() + "_COMPENSA";
				Vertice c = new Vertice(cName, t.getType(), t.getTransactionalProperty(), t.qos, t.getQoSReal(), t.getURI());
				//get *t and t* 
				ArrayList<Vertice> pred =  this.predecesores(t);
				ArrayList<Vertice> suc =  this.sucesores(t);
				//invert flow relation
				ArrayList<Vertice> in = new ArrayList<Vertice>();
				for(Vertice i: suc) {
					if(!in.contains(i))
						in.add(i);
						
				}
				ArrayList<Vertice> out = new ArrayList<Vertice>();
				for(Vertice i: pred) {
					if(!out.contains(i))
						out.add(i);
						
				}
				pn.addWebService(c, in, out);
				brMapping.put(t.getName(), c.getName());
			}
		}
		return pn;
	 }
	
	public String getCompensatedTransition(String name) {
		for(String key:brMapping.keySet()) {
			if(brMapping.get(key).equals(name))
				return key;
		}
		return null;
	}
	
	//RAFA
	public  Map<String,Integer> getInputList(Vertice v) {
		return getInputList(v.getName());
	}
	public  Map<String,Integer> getInputList(String v) {
		Map<String,Integer>  ret = new HashMap<String, Integer>();
		
		Arco a;
		for (int i=0;i<getArcos().size();i++) {
			a = getArcos().get(i);
			if (a.getVerticeFin().compareTo(v) == 0) {
				Vertice vi = getPlace(a.getVerticeIni());
				if(!ret.containsKey(vi.getName())) {
					ret.put(vi.getName(), new Integer(predecesores(vi).size()));
				} 
			}
		}
		return ret;
	}

	public ArrayList<Vertice> addTransitionwithC(ArrayList<Vertice> suc_tr, ArrayList<Vertice> pred_tr, Vertice v) {
		ArrayList<Vertice> pret = new ArrayList<Vertice>();
		boolean inclpred=true;

		if (!perteneceT(v)) addT(v);
		else inclpred=false;
		for (int i=0;i<suc_tr.size();i++) {
			Vertice pl = suc_tr.get(i);
			if (perteneceP(pl)) {
				int rep = getLastReplica(pl);		
				Arco a = new Arco(v.getName(),pl.getName(),rep);
				if (!pertenece(a,rep)) add(a);
				//System.out.println("Arco en AT " + v.getName() + " " + pl.getme());
			}
		}
		int colortr = v.getColor().get(0); //transitions have only one color
		if (inclpred) {
			for (int i=0;i<pred_tr.size();i++) {
				Vertice pl = pred_tr.get(i);
				if ((colortr == Vertice.PIVOT) || (colortr == Vertice.ATOMIC) ||(colortr == Vertice.COMPENSA)) 
					setMarkage(pl,"COMPENSA");
				else setMarkage(pl,"ALL");
				pret.add(pl);   
			}
		}
		return pret;
	}  


	public ArrayList<Vertice> addPlacewithC(ArrayList<Vertice> suc_pl, ArrayList<Vertice> pred_pl, Vertice v) {
		int replica;
		ArrayList<Vertice> pret = new ArrayList<Vertice>();

		if (!perteneceP(v)) replica =0;
		else {
			replica =getLastReplica(v)+1;
			Vertice nv = new Vertice(v.getName(),Vertice.PLACE);
			v = nv;
		}	
		v.setReplica(replica);
		addP(v);
		for (int i=0;i<suc_pl.size();i++) {
			Vertice tr = suc_pl.get(i);	    
			if (perteneceT(tr)) {
				Arco a = new Arco(v.getName(),tr.getName(),replica);
				if (!pertenece(a)) { 
					add(a); 
				}
				//System.out.println("Arco en AT" + v.getName() + " " + tr.getName());
			}
		}
		pret.addAll(pred_pl);
		return pret;
	}  



	public void setMarkage(Vertice pl, String type) {

		if (type.compareTo("ALL")==0) { // set all colors
			pl.setPivot();
			pl.setPivotRet();
			pl.setAtomic();
			pl.setAtomicRet();
			pl.setCompensa();
			pl.setCompensaRet();
		}
		else { // set compesas and comperet
			pl.setCompensa();
			pl.setCompensaRet();
		}
	}

	public boolean checkColor(Vertice tr, ArrayList<Vertice> sucec) {
		ArrayList<Integer> color_suc;
		int color_tr = tr.getColor().get(0);
		boolean ret=false;

		for (int i =0;i< sucec.size();i++) {
			Vertice x = sucec.get(i);
			if (perteneceP(x)) {
				x = getPlace(x.getName());
				color_suc = x.getColor();
				if (color_suc.contains(color_tr)){
					ret =true;		    
					break;
				}
				color_suc.clear();
			}	    
		}

		return ret;
	}

	public void setInitialMarking(Query q) {
		ArrayList<Vertice> q_inputs;
		boolean marked;
		setMarkedColor(Vertice.INITIAL);
		q_inputs =  q.getIns();

		for(Vertice place: places) {
			marked=false;	    
			for (Vertice input: q_inputs) {
				if (place.getName().compareTo(input.getName())==0) {
					place.setINITIAL();

					marked=true;
				}
			}
			if (!marked) 
				place.setEmpty();
		}
	}
	
	//RAFA
	public void setInitialMarkingBR(Query q) {

		for(Vertice t: transitions) {
			t.setState(Vertice.INITIAL);
		}
	}



	public ArrayList<Vertice> getFirables() {
		ArrayList<Vertice> pdtr = new ArrayList<Vertice>();
		ArrayList<Vertice> ret = new ArrayList<Vertice>();

		for (int i = 0; i< transitions.size();i++) {
			pdtr = predecesoresWithRep(transitions.get(i));
			if (isFirable(transitions.get(i),pdtr)) 
				ret.add(transitions.get(i));
		}
		return ret;
	}

	public ArrayList<Vertice> predecesoresWithRep(Vertice v) {
		ArrayList<Vertice> ret = new ArrayList<Vertice>();
		Arco a;
		if (v == null)
			return ret;

		for (int i=0;i<arcos.size();i++) {
			a=arcos.get(i);
			if (a.getVerticeFin().compareTo(v.getName()) == 0 && (!ret.contains(getTransition(a.getVerticeIni())))) {
				Vertice vi = this.getTransition(a.getVerticeIni());
				if (vi != null) 
					ret.add(vi);
				else {
					vi = this.getPlace(a.getVerticeIni(),a.getReplica());
					//System.out.println("PREDEC WITH REP " + vi + " replica " + vi.getReplica());
					if (vi != null)  
						ret.add(vi);
				}
			}
		}
		return ret;        
	}


	public boolean isFirable(Vertice tr,ArrayList<Vertice> ptr) {
		boolean ret = false;
		ArrayList<Vertice> p_sinPred = new ArrayList<Vertice>();
		//System.out.println("is fira tr " + tr + " pred " + ptr);

		int com =0;
		for (int j=0;j< ptr.size();j++) {
			//	 System.out.println(" PRED " +	ptr.get(j) + " REPLICA " + ptr.get(j).getReplica());
			if (ptr.get(j).getColor().size()!=0) {
				com++;
				//System.out.println("EN FIRABLE PRED " + ptr.get(j) + " Color size " + ptr.get(j).getColor().size() + "Transition " + tr);
			}
		}
		//System.out.println(" COM " +	com);
		if (com==ptr.size()) { //Condition 0

			int m=places.size();
			for (int i=0 ; i < places.size(); i++) // Condition 1
				if ((places.get(i).getColor().size()==0) || (places.get(i).isInitial())) 
					m --;
			if (m==0) 
				return true;

			if (tr.isCompensaRet()) return true; // Condition 2
			else {
				p_sinPred = (ArrayList<Vertice>) places.clone();
				for (int i=0;i<ptr.size();i++) 
					while (p_sinPred.remove(ptr.get(i))); //to eliminate replicated places
				//System.out.println("places sin predec " + p_sinPred);
				if ((tr.isPivotRet()) || (tr.isAtomicRet())) { // Condition 3
					//System.out.println("es pivot ret ");
					int j = p_sinPred.size();
					for (int k=0;k< p_sinPred.size();k++) {
						if ((p_sinPred.get(k).getColor().size()==0) || (p_sinPred.get(k).isInitial()) || (p_sinPred.get(k).isAtomicRet()) || (p_sinPred.get(k).isCompensaRet())) j--;
					}
					if (j == 0) return true;
				} //Condition 3

				else if (tr.isCompensa()) { // Condition 4 
					int j = p_sinPred.size();
					//System.out.println("ES COMPENSA y J " + j);
					for (int k=0;k< p_sinPred.size();k++) {
						if ((p_sinPred.get(k).getColor().size()==0) || (p_sinPred.get(k).isInitial()) || (p_sinPred.get(k).isCompensa()) || (p_sinPred.get(k).isCompensaRet())) j--;
					}
					if (j == 0) {
						//System.out.println("j == 0 pred " + ptr);
						int m1=ptr.size();
						for (int x=0;x< ptr.size();x++) {
							if ((ptr.get(x).isInitial()) || (ptr.get(x).isCompensa()) || (ptr.get(x).isCompensaRet())) m1--;
							//System.out.println("place color " + ptr.get(x).getColor() + "place replica " + ptr.get(x).getReplica());
						}
						//System.out.println("M == 0? " + m );
						if (m1==0) 
							return true;
					}
				} //Condition 4

				else if ((tr.isPivot()) || (tr.isAtomic())) { // Condition 5
					int j = p_sinPred.size();
					for (int k=0;k< p_sinPred.size();k++) {
						if ((p_sinPred.get(k).getColor().size()==0) || (p_sinPred.get(k).isInitial()) || (p_sinPred.get(k).isCompensaRet())) 	j--;
					}
					if (j == 0) {
						int m1=ptr.size();
						for (int x=0;x< ptr.size();x++) 
							if ((ptr.get(x).isInitial()) || (ptr.get(x).isCompensa()) || (ptr.get(x).isCompensaRet())) m1--;
						if (m1==0) 
							return true;
					}
				}    //Condition 5
			} // Condition 2
		}//Condition 0
		//System.out.println("Firable?? " + ret);
		return ret;
	} // Fin de metodo

	public void setQuality(Query q) {
		ArrayList<Vertice> outs = q.getOuts();	
		for (int i=0;i<transitions.size();i++) {
			Vertice tr = transitions.get(i);
			double score =0;
			double w[] = q.getWeights();
			for (int j=0;j<w.length;j++)
				score= score + (w[j]*tr.getQoS()[j]);
			//System.out.println("SET QUALITY " + tr + "score " + score);
			double qlt=score;
			if (tr.isPivotRet() || tr.isAtomicRet())
				qlt=qlt*2;
			else if (tr.isCompensa())
				qlt=qlt*3;
			else if (tr.isCompensaRet())
				qlt=qlt*4;
			ArrayList<Vertice> suc = sucesores(tr);
			//System.out.println("SET QUALITY " + tr + "qlt " + qlt);

			int n_outs = 0;
			for (int j=0;j<outs.size();j++)
				if (suc.contains(outs.get(j))) n_outs++;
			n_outs++;
			qlt=qlt*n_outs;
			int suc_suc=0;
			//System.out.println("SET QUALITY " + tr + "qlt " + qlt);

			for (int j=0;j<suc.size();j++) 
				suc_suc=suc_suc + sucesores(suc.get(j)).size(); 

			qlt = qlt * (1 + (suc_suc/transitions.size()));
			tr.setQuality(qlt);
		}
	}
	//TODO Preguntar a Yudith sobre esto
	//Cost esta realcionado solo con los valores reales, sin tomar en cuenta pesos
	public void setCost(Query q) {	
		//		double w[] = q.getWeights();
		for (int i=0;i<transitions.size();i++) {
			Vertice tr = transitions.get(i);
			double score =0;
			score = score + tr.getQoSReal()[0];  //The exec. time is the first position in qos_real array. 
			// When we have more qos, this value should be an aggregated one (based on real values)
			//			score = score + w[0]*tr.getQoS()[0];  //The first weight correspond to execution time, exec. time is the fifth position in qos array
			//score = score + w[1]*tr.getQoS()[1]; //The second weight correspond to price, price is the sixth position in qos array
			//score = score + w[2]*tr.getQoS()[2];
			//score = score + w[3]*tr.getQoS()[3];
			tr.setCost(score);
		}
	} 
}
