/*
 * PetriNet.java
 *
 * Created on 20 de enero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.PetriNets;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.ArrayList;

public class PetriNet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*************************** Attributes ************************************/
	//Name of PN
	String                       name;

	ArrayList<Vertice> transitions;
	ArrayList<Vertice> places;
	ArrayList<Arco>    arcos;


	/************************** Methodes ****************************************/

	/**
	 * Implement clone of object
	 * @return PetriNet Clon
	 **/
	public PetriNet clone() {
		PetriNet pn = new PetriNet(new String(this.name));


		for (int i=0; i < transitions.size();i++) {
			Vertice v=transitions.get(i);
			pn.addT(v.clone());
		}

		for (int i=0; i < places.size();i++) {
			Vertice v=places.get(i);
			pn.addP(v.clone());
		}

		for (int i=0; i < arcos.size();i++) {
			Arco a=arcos.get(i);
			pn.add(a.clone());
		}
		return pn;
	}


	private int getPos(ArrayList<String> list, String data) {
		int pos = 0;

		while (pos < list.size()) {
			if (list.get(pos).equalsIgnoreCase(data)) {
				break;
			}

			pos++;
		}

		return pos;
	}


	/**
	 * Creates a new instance of PetriNet
	 */
	 public PetriNet(String name) {
		this.name = name;
		transitions  = new ArrayList<Vertice>();
		places       = new ArrayList<Vertice>();
		arcos        = new ArrayList<Arco>();   

	 }

	 /**
	  * Assign Name
	  **/
	 public void setName(String name){
		 this.name=name;
	 }


	 public String getName() {
		 return name;
	 }

	 public void setTransitions(ArrayList<Vertice> v){
		 this.transitions= (ArrayList<Vertice>) v.clone();
	 }

	 public void setPlaces(ArrayList<Vertice> v){
		 this.places= (ArrayList<Vertice>) v.clone();
	 }


	 /* 
	  * Implementation of Alg. 1: Create the WSDN: Adding a WS into it
	  */

	 public void addWebService(Vertice t, ArrayList<Vertice> in, ArrayList<Vertice> ou) {
		 Vertice i, o, s,sw;

		 if (!perteneceT(t)) { /* transition (WS) does not exist: item 1 Alg. 1 */
			 addT(t);
		 }
		 for (int pos=0; pos < in.size(); pos++) {  /* adding input places */
			 i = in.get(pos);
			 if (!perteneceP(i)) { /* input place does not exist: items 2 Alg. 1 */
				 addP(i); /* input is included */		
				 Arco a = new Arco(i.getName(),t.getName()); /* F(i,t)=1: item 2 */
				 add(a); 
			 }
			 else {/* input place exists */
				 if (sucesores(i).size() !=0) { /* but has successors */
					 int r=getLastReplica(i);
					 r++;
					 i.setReplica(r);
					 addP(i); /* input is replicated: item 2 Alg 1 */		
					 Arco a = new Arco(i.getName(),t.getName(),i.getReplica()); /* F(i,t)=1: item 2 */
					 add(a); 
					 //System.out.println("Replicado Input "+ i.getName() + " " + i.getReplica());
				 } 
				 else {/* but has no successors: item 4 Alg. 1 */
					 Arco a = new Arco(i.getName(),t.getName()); /* F(i,t)=1: item 4 */
					 add(a); 
					 //	    System.out.println("No Replicado Input agregado arco (i,t)");
				 }
				 for (int j=0;j < transitions.size();j++) {

					 sw = transitions.get(j);
					 ArrayList<Vertice> suc = sucesores(sw);
					 for (int pos_s=0; pos_s < suc.size(); pos_s++) {
						 s = suc.get(pos_s);
						 //System.out.println("Names " + s.getName() + " " + i.getName());
						 if (s.getName().compareTo(i.getName()) == 0) {
							 //System.out.println("Revisando Agregado Arco por otras t");
							 Arco a = new Arco(sw.getName(),i.getName()); /* F(s,i)=1: item 6 */
							 if (!pertenece(a,i.getReplica())) {
								 a.setReplica(i.getReplica());
								 add(a);
								 //System.out.println("Agregado Arco por otras t");
							 }
						 }
					 }
				 }
			 }
		 } 
		 for (int pos=0; pos < ou.size(); pos++) {  /* adding output places */
			 o = ou.get(pos);
			 if (!perteneceP(o)) { /* output place does not exist: items 3 Alg. 1 */
				 addP(o);	/* output is included */	
				 Arco a = new Arco(t.getName(),o.getName()); /* F(t,o)=1: item 3 */
				 add(a); 
			 }
			 else {/* output place exists */
				 int lastrep = getLastReplica(o);
				 for (int j=0;j<=lastrep;j++) {
					 Arco a = new Arco(t.getName(),o.getName(),j); /* F(t,o)=1: item 5 */
					 if (!pertenece(a,j)) add(a); 
				 }
			 }
		 }
	 } 

	 public void addT(Vertice v) {
		 if (!transitions.contains(v)) { /* vertice is not included */
			 transitions.add(v);
		 } 
	 }

	 public void addP(Vertice v) {
		 places.add(v);
	 }

	 public void add(Arco l) {

		 arcos.add(l);

	 }

	 public Vertice getTransition(String name) {
		 for (int i=0;i<transitions.size();i++){
			 Vertice v = transitions.get(i);
			 if (v.getName().compareTo(name) == 0) 
				 return v;		
		 }
		 return null;
	 }

	 public Vertice getPlace(String name) {
		 for (int i=0;i<places.size();i++){
			 Vertice v = places.get(i);
			 if (v.getName().compareTo(name) == 0) 
				 return v;		
		 }
		 return null;
	 }

	 public Vertice getPlace(String name, int rep) {
		 for (int i=0;i<places.size();i++){
			 Vertice v = places.get(i);
			 if (v.getName().compareTo(name) == 0 && v.getReplica()==rep) 
				 return v;		
		 }
		 return null;
	 }

	 public Arco getArco(String name) {
		 for (int i=0;i<arcos.size();i++){
			 Arco a = arcos.get(i);
			 if (a.getName().compareTo(name) == 0) 
				 return a;		
		 }
		 return null;
	 }

	 public Arco getArco(Vertice v1, Vertice v2) {		 String name = Arco.getCanonicalName(v1.getName(), v2.getName());
		 for (int i=0;i<arcos.size();i++){
			 Arco a = arcos.get(i);
			 if (a.getName().compareTo(name) == 0) 
				 return a;		
		 }
		 return null;
	 }


	 public int getLastReplica(Vertice v) {
		 int ret=0;   
		 for (int i=0;i< places.size();i++) {
			 Vertice vt =places.get(i);
			 if (v.getName().compareTo(vt.getName()) == 0) {
				 if (vt.getReplica() > ret) ret=vt.getReplica();
			 }
		 }
		 return ret;
	 }

	 public int numTransitions() {
		 return transitions.size();
	 }

	 public int numPlaces() {
		 return places.size();
	 }

	 public boolean pertenece(Arco a) {
		 boolean ret = false;


		 for (int i=0;i< arcos.size();i++) {
			 if (a.getName().compareTo(arcos.get(i).getName()) == 0) {
				 ret = true;
				 break;
			 }
		 }
		 return ret;        
	 }

	 public boolean pertenece(Arco a, int replica) {
		 boolean ret = false;

		 for (int i=0;i< arcos.size();i++) {
			 Arco at = arcos.get(i);
			 if (a.getName().compareTo(at.getName()) == 0) {
				 if (at.getReplica() == replica) {
					 ret = true;
					 break;
				 }
			 }
		 }
		 return ret;        
	 }

	 public boolean perteneceT(Vertice verProc) {
		 boolean ret = false;

		 for (int i=0;i< transitions.size();i++) {
			 if (verProc.getName().compareTo(transitions.get(i).getName()) == 0) {
				 ret = true;
				 break;
			 }
		 }
		 return ret;
	 }


	 public boolean perteneceP(Vertice verProc) {
		 boolean ret = false;

		 for (int i=0;i< places.size();i++) {
			 if (verProc.getName().compareTo(places.get(i).getName()) == 0) {
				 ret = true;
				 break;
			 }
		 }
		 return ret;
	 }



	 public ArrayList<String> incidentes(String name) {
		 ArrayList<String> ret = new ArrayList<String>();

		 return ret;
	 }

	 public ArrayList<Vertice> sucesores(Vertice v) {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();
		 Arco a;
		 if (v == null)
			 return ret;

		 for (int i =0; i< arcos.size();i++) {
			 a = arcos.get(i);
			 if (a.getVerticeIni().compareTo(v.getName()) == 0) {
				 Vertice vi = this.getTransition(a.getVerticeFin());
				 if (vi != null) ret.add(vi);
				 else {
					 vi = this.getPlace(a.getVerticeFin());
					 if (vi != null)  ret.add(vi);
				 }
			 }
		 }
		 return ret;        
	 }
	 
	 //RAFA
	 public ArrayList<Vertice> getSucTransitions(Vertice v) {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();
		 Arco a;
		 if (v == null)
			 return ret;

		 for (int i =0; i< arcos.size();i++) {
			 a = arcos.get(i);
			 if (a.getVerticeIni().compareTo(v.getName()) == 0) {
				 Vertice output = this.getPlace(a.getVerticeFin());
				 for(Arco ac:arcos) {
					 if (ac.getVerticeIni().compareTo(output.getName()) == 0) {
						 Vertice suc = this.getTransition(ac.getVerticeFin());
						 if(suc!=null && !ret.contains(suc))
							 ret.add(suc);
					 }
				 }
			 }
		 }
		 return ret;
	 }
	 //RAFA
	 public ArrayList<Vertice> getPredTransitions(Vertice v) {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();
		 Arco a;
		 if (v == null)
			 return ret;

		 for (int i=0;i<arcos.size();i++) {
			 a=arcos.get(i);
			 if (a.getVerticeFin().compareTo(v.getName()) == 0) {
				 Vertice output = this.getPlace(a.getVerticeIni());
				 for(Arco ac:arcos) {
					 if (ac.getVerticeFin().compareTo(output.getName()) == 0) {
						 Vertice suc = this.getTransition(ac.getVerticeIni());
						 if(suc!=null && !ret.contains(suc))
							 ret.add(suc);
					 }
				 }
			 }
		 }
		 return ret;             
	 }

	 public ArrayList<Vertice> predecesores(Vertice v) {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();
		 Arco a;
		 if (v == null)
			 return ret;

		 for (int i=0;i<arcos.size();i++) {
			 a=arcos.get(i);
			 if (a.getVerticeFin().compareTo(v.getName()) == 0 && (!ret.contains(getTransition(a.getVerticeIni())))) {
				 Vertice vi = this.getTransition(a.getVerticeIni());
				 if (vi != null) 
					 ret.add(vi);
				 else {
					 vi = this.getPlace(a.getVerticeIni());
					 if (vi != null)  
						 ret.add(vi);
				 }
			 }
		 }
		 return ret;        
	 }

	 public ArrayList<String> sucesores(String name) {
		 ArrayList<String> ret = new ArrayList<String>();
		 Arco a;
		 Vertice v = null;
		 for (int i=0;i<arcos.size();i++) {
			 a=arcos.get(i);
			 if (a.getVerticeIni().compareTo(name) == 0)
				 ret.add(a.getVerticeFin());
		 }

		 return ret;
	 }

	 public ArrayList<String> predecesores(String name) {
		 ArrayList<String> ret = new ArrayList<String>();
		 Arco a;
		 Vertice v = null;
		 for (int i=0;i<arcos.size();i++) {
			 a=arcos.get(i);
			 if ((a.getVerticeFin().compareTo(name) == 0) && (!ret.contains(a.getVerticeIni())))
				 ret.add(a.getVerticeIni());
		 }

		 return ret;
	 }

	 public int grado(String name) {
		 return gradoInt(name) + gradoExt(name);
	 }

	 public int gradoInt(String name) {
		 int ret = 0;
		 Arco a;
		 Vertice v = null;

		 for (int i=0;i<arcos.size();i++) {
			 a=arcos.get(i);
			 if (a.getVerticeFin().compareTo(name) == 0)
				 ret++;
		 }

		 return ret;    
	 }

	 // number of arcs that go out from the vertice
	 public int gradoExt(String name) {
		 int ret = 0;
		 Arco a;
		 Vertice v = null;

		 for (int i=0;i<arcos.size();i++) {
			 a=arcos.get(i);
			 if (a.getVerticeIni().compareTo(name) == 0)
				 ret++;
		 }

		 return ret;
	 }

	 public void deleteVertice(String name) {
		 ArrayList<Arco> nomArcs = new ArrayList<Arco>();
		 Arco a;

		 Vertice v = null;



		 for (int i=0;i<transitions.size();i++) {
			 Vertice vt=transitions.get(i);
			 if (vt.getName().compareTo(name) == 0) {
				 v=vt;
				 break;
			 }
		 }

		 if (v == null) {
			 for (int i=0;i<places.size();i++) {
				 Vertice vt=places.get(i);
				 if (vt.getName().compareTo(name) == 0) {
					 v=vt;
					 break;
				 }
			 }
		 }
		 if (v==null)
			 return;

		 for (int i=0;i<arcos.size();i++) {
			 a=arcos.get(i);
			 if ((a.getVerticeIni().compareTo(name) == 0) ||
					 (a.getVerticeFin().compareTo(name) == 0)) {                             
				 nomArcs.add(a);
			 }
		 }

		 for (Arco nam : nomArcs) {
			 arcos.remove(nam);
		 }

		 if (v.getType()==Vertice.TRANSITION)
			 transitions.remove(v); 
		 else
			 places.remove(v);
	 }

	 public void deleteArco(String name) {
		 Arco a= null;

		 for (int i=0;i<arcos.size();i++) {
			 Arco ta=arcos.get(i);
			 if (ta.getName().compareTo(name) == 0) {
				 a=ta;
				 break;
			 }
		 }

		 if (a == null)
			 return;
		 arcos.remove(a);
	 }

	 public ArrayList<Vertice> getTransitions() {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();

		 for (int i=0;i<transitions.size();i++) {
			 ret.add(transitions.get(i));
		 }  
		 return ret;        
	 }

	 public ArrayList<Vertice> getPlaces() {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();

		 for (int i=0;i<places.size();i++) {
			 ret.add(places.get(i));
		 }  
		 return ret;        
	 }

	 public ArrayList<Vertice> getInputs() {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();

		 for (int i=0;i<places.size();i++) {

			 if (predecesores(places.get(i)).size() == 0) {
				 Vertice v = places.get(i).clone();
				 ret.add(v);
			 }
		 }  
		 return ret;        
	 }

	 @Deprecated //Which outputs?
	 public ArrayList<Vertice> getOutputs() {
		 ArrayList<Vertice> ret = new ArrayList<Vertice>();

		 for (int i=0;i<places.size();i++) {
			 ret.add(places.get(i));
		 }  
		 return ret;        
	 }

	 /**
	  * List of trancsitions names
	  **/
	  public ArrayList<String> getTransitionsNames() {
		 ArrayList<String> ret = new ArrayList<String>();

		 for (int i=0;i<transitions.size();i++) {
			 ret.add(transitions.get(i).getName());
		 }  

		 return ret;        
	 }

	 /**
	  * List of places names
	  **/
	  public ArrayList<String> getPlacesNames() {
		  ArrayList<String> ret = new ArrayList<String>();

		  for (int i=0;i<places.size();i++) {
			  ret.add(places.get(i).getName());
		  }  

		  return ret;        
	  }

	  public ArrayList<Arco> getArcos() {
		  ArrayList<Arco> ret = new ArrayList<Arco>();

		  for (int i=0;i<arcos.size();i++) {
			  ret.add(arcos.get(i));
		  }  

		  return ret;
	  }

	  /**
	   * List of arcs names
	   **/
	  public ArrayList<String> getArcosName() {
		  ArrayList<String> ret = new ArrayList<String>();

		  for (int i =0;i< arcos.size();i++) {
			  ret.add(arcos.get(i).getName());
		  }     
		  return ret;
	  }



	  public String toString2() {
		  int numS = 0;
		  int numD = 0;

		  numS=transitions.size();        
		  numD=places.size();        
		  return "Procesos " + numS + " Datos " +numD; 
	  }

	  public String toString() {
		  String time = "";
		  String ret1 = "T = { \n";


		  for (int i =0; i< transitions.size();i++) {
			  ret1 += transitions.get(i).getType() +" " + transitions.get(i).getName() +" " +  transitions.get(i).getReplica() +", \n";
		  }

		  ret1 += "}";        


		  ret1 += "P = { \n";


		  for (int i =0; i< places.size();i++) {
			  ret1 += places.get(i).getType() +" " + places.get(i).getName() +" " +  places.get(i).getReplica() +", \n";
		  }

		  ret1 += "}";        


		  String ret2 = "E = { ";


		  for (int i =0; i< arcos.size();i++) {
			  ret2 += arcos.get(i).getName() +" "+ arcos.get(i).getReplica() + ", \n";
		  }


		  ret2 += "}";

		  int numP = transitions.size();
		  int numD = places.size();

		  String ret4 = "Procesos " + numP + " Datos " +numD;
		  String ret = "Plan: " + name + "\n--------------------------\n" + ret1 + "\n" + ret2 + "\n" + "\n"+ ret4 + "\n";


		  return ret;
	  }

	  /* 
	   * Obtain SRq: transitions satisfying Rq 
	   */

	  public ArrayList<Vertice> getServicesbyRisk(int r) {
		  ArrayList<Vertice> ret = new ArrayList<Vertice>();
		  Vertice v;
		  if (r == 0){ // RISK = R0       
			  for (int i = 0; i< transitions.size();i++) {
				  v = transitions.get(i);
				  if (v.isCompensa() || v.isCompensaRet()) {
					  ret.add(0,v);
				  }
			  }
		  }
		  else   { // RISK = R1
			  for (int i = 0; i< transitions.size();i++) {
				  v = transitions.get(i);
				  ret.add(0,v);
			  }
		  }
		  return ret;
	  }



	  /**
	   * Number of transitions in the PN
	   **/
	  public int getNumProcecsos(){		  return transitions.size();
	  }
	  /**
	   * List of inputs arcs of a vertice
	   **/
	  public ArrayList<Arco> arcosEntrantes(String vertice){
		  ArrayList<Arco> arcos = new ArrayList<Arco>();
		  ArrayList<Arco> arcosTodos = this.getArcos();
		  for (Arco a : arcosTodos){
			  if(a.getVerticeFin().equalsIgnoreCase(vertice)){
				  arcos.add(a);
			  }
		  }

		  return arcos;
	  }


	  /**
	   * List of outputs arcs of a vertice 
	   **/
	  public ArrayList<Arco> arcosSalientes(String vertice){
		  ArrayList<Arco> arcos = new ArrayList<Arco>();
		  ArrayList<Arco> arcosTodos = this.getArcos();

		  for (Arco a : arcosTodos){
			  if(a.getVerticeIni().equalsIgnoreCase(vertice)){
				  arcos.add(a);
			  }
		  }


		  return arcos;
	  }

}
