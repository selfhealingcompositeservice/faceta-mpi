/*
 * Plan.java
 *
 * Created on 20 de enero de 2010
 *
 */
/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.PetriNets;

import java.util.ArrayList;

public class Plan extends PetriNet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Plan(String name) {
        super(name);
    }
    
    public Plan(String name, String planXLM) {
        super(name);
    }
    
    public ArrayList<Vertice> getServiceVertices() {
        ArrayList<Vertice> vers = new ArrayList<Vertice>();
            
        return vers;
    }

    public ArrayList<Vertice> getDataVertices() {
        ArrayList<Vertice> vers = new ArrayList<Vertice>();
            
        return vers;
    }
}
