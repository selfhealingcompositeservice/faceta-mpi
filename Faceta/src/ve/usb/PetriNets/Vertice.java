/*
 * Vertice.java
 *
 * Created on 20 de enero de 2010, 05:33 PM
 *
 */

/**
 *
 * @author Yudith Cardinale
 */

package ve.usb.PetriNets;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.mindswap.owls.service.Service;

public class Vertice implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Type of Vertice
	public static final int PLACE       = 2;
	public static final int TRANSITION  = 4;

	// Transactional Properties
	public static final int INITIAL     = 8;
	public static final int PIVOT       = 16;
	public static final int ATOMIC      = 32;
	public static final int PIVOTRET    = 64;
	public static final int ATOMICRET   = 128;
	public static final int COMPENSA    = 256;
	public static final int COMPENSARET = 512;
	
	public java.net.URI getUri() {
		return uri;
	}

	public void setUri(java.net.URI uri) {
		this.uri = uri;
	}

	//Execution States
	public static final int RUNNING = 1024;
	public static final int EXECUTED = 2048;
	public static final int COMPENSATE = 4096;
	public static final int ABANDONED = 8192;

	private String          name;     	// Vertice's name
	private Service         service; 	// Related to the owl-s description
	private int             type;	// Place or Transition
	private java.net.URI    uri;	// Related to the site of the Ontology
	private int		    n_replica;	// Number of Place Replica (if the Vertice is a Transition, replica = 0)
	private ArrayList<Integer> color; 	// Transactional properties of the Vertice
	public double qos[] = null;		// QoS parameter normalized values (Only for Transitions)
	public double qos_real[] = null;	// QoS parameter real values (Only for Transitions)
	private double 	    quality;	// Quality function value considering normalized values and cost model 
	private double 	    cost;	// Cost considering only the QoS parameters real values (not reachable outputs ...)
	private List<Vertice> equivalents; //name of the equivalents services within the Ontology
	//RAFA
	private int state; //Execution state

	/**
	 * Creates a new instance of Vertice	 **/
	public Vertice(Service s) {
		service = s;
		name    = s.getName();
		type    = TRANSITION;
		color   = new ArrayList<Integer>();
		n_replica = 0;
		quality   = 0;
		cost	  = 0;
	}

	public Vertice(java.net.URI u) {
		name = u.toString();
		uri  = u;
		type = PLACE;
		color   = new ArrayList<Integer>();
		n_replica = 0;
	}


	public Vertice(java.net.URI u, int t) {
		name = u.toString();
		uri  = u;
		type = t;
		color   = new ArrayList<Integer>();
		n_replica = 0;
		quality = 0;
		cost = 0;
	}

	public Vertice(String n, int t) {
		service = null;
		uri = null;
		name    = n;
		type    = t;
		color   = new ArrayList<Integer>();
		n_replica = 0;
		quality = 0;
		cost    = 0;
	}

	public Vertice(String n, int t, int pt) {
		service = null;
		uri = null;
		name    = n;
		type    = t;
		color   = new ArrayList<Integer>();
		color.add(0,pt);
		n_replica = 0;
		quality = 0;
		cost = 0;
	}

	public Vertice(String n, int t, int pt, double q[], double q_r[]) {
		service = null;
		uri = null;
		name    = n;
		type    = t;
		color   = new ArrayList<Integer>();
		color.add(0,pt);
		n_replica = 0;
		qos     = new double[q.length];
		System.arraycopy(q, 0, qos, 0, q.length);

		qos_real     = new double[q_r.length];
		System.arraycopy(q_r, 0, qos_real, 0, q_r.length);
		quality = 0;
		cost=0;
	}
	/*
	 * Agregado por Rafael
	 */
	 public Vertice(String n, int t, int pt, double q[], double q_r[], java.net.URI uri) {
		service = null;
		name    = n;
		type    = t;
		color   = new ArrayList<Integer>();
		color.add(0,pt);
		n_replica = 0;
		qos     = new double[q.length];
		System.arraycopy(q, 0, qos, 0, q.length);

		qos_real     = new double[q_r.length];
		System.arraycopy(q_r, 0, qos_real, 0, q_r.length);
		quality = 0;
		cost=0;
		this.uri = uri;
	 }

	 public Vertice clone() {
		 Vertice n = null;

		 if (type == TRANSITION)
			 n = new Vertice(name,type,color.get(0),qos,qos_real);
		 else
			 n = new Vertice(name,type);
		 n.color = (ArrayList<Integer>) color.clone();
		 n.n_replica = n_replica;
		 n.quality = quality;
		 n.cost = cost;

		 if (qos != null) {
			 n.qos = new double[qos.length];      
			 System.arraycopy(qos, 0, n.qos, 0, qos.length);
		 }

		 if (qos_real != null) {
			 n.qos_real = new double[qos_real.length];      
			 System.arraycopy(qos_real, 0, n.qos_real, 0, qos_real.length);
		 }
		 return n;
	 }


	 public void setReplica(int r) {
		 n_replica=r; 
	 }

	 public int getReplica() {
		 return n_replica;
	 }

	 public void setQuality(double q) {
		 quality=q; 
	 }

	 public void setCost(double c) {
		 cost=c; 
	 }

	 public double getQuality() {
		 return quality;
	 }

	 public double getCost() {
		 return cost;
	 }

	 public void setType(int t) {
		 type = t; 
	 }

	 public void setEmpty() {

		 color.clear();    
	 }
	 public void setINITIAL() {
		 color.add(0, INITIAL);    
	 }

	 public void setPivot() {
		 color.add(0, PIVOT);    
	 }

	 public void setPivotRet() {
		 color.add(0, PIVOTRET);    
	 }

	 public void setAtomic() {
		 color.add(0, ATOMIC);    
	 }

	 public void setAtomicRet() {
		 color.add(0, ATOMICRET);    
	 }

	 public void setCompensa() {
		 color.add(0, COMPENSA);    
	 }

	 public void setCompensaRet() {
		 color.add(0, COMPENSARET);    
	 }

	 public void setColor(int c) {
		 color.add(0, c);    
	 }
	 public void removeColors() {
		 color.clear();
	 }

	 public boolean isInitial() {
		 return this.color.contains(INITIAL);
	 }

	 public boolean isPivot() {
		 return this.color.contains(PIVOT);
	 }

	 public boolean isPivotRet() {
		 return this.color.contains(PIVOTRET);
	 }

	 public boolean isAtomic() {
		 return this.color.contains(ATOMIC);
	 }

	 public boolean isAtomicRet() {
		 return this.color.contains(ATOMICRET);
	 }

	 public boolean isCompensa() {
		 return this.color.contains(COMPENSA);
	 }

	 public boolean isCompensaRet() {
		 return this.color.contains(COMPENSARET);
	 }
	 
	 public int getTransactionalProperty() {
		 int tp = 0;
		 
		 if(isPivot())
			 tp = PIVOT;
		 else if(isPivotRet())
			 tp = PIVOTRET;
		 else if(isAtomic())
			 tp = ATOMIC;
		 else if(isAtomicRet())
			 tp = ATOMICRET;
		 else if(isCompensa())
			 tp = COMPENSA;
		 else if(isCompensaRet())
			 tp = COMPENSARET;
		 
		 return tp;
	 }

	 public void setService(Service service) {
		 this.service = service;
	 }

	 public Service getService() {
		 return service;
	 }

	 public java.net.URI getURI() {
		 return uri;
	 }

	 public int getType() {
		 return type;
	 }

	 public String getName() {
		 return name;
	 }

	 public ArrayList<Integer> getColor() {
		 ArrayList<Integer> ret= new ArrayList<Integer>();
		 for (int i=0;i<color.size();i++)
			 ret.add(color.get(i));
		 return ret;
	 }

	 public double [] getQoS() {
		 double ret[]= new double[qos.length];
		 System.arraycopy(qos, 0, ret, 0, qos.length);
		 return ret;
	 }

	 @Override
	public boolean equals(Object obj) {
		if(obj instanceof Vertice)
			return ((Vertice)obj).getName().equals(this.getName());
		return false;
	}

	public double [] getQoSReal() {
		 double ret[]= new double[qos_real.length];
		 System.arraycopy(qos_real, 0, ret, 0, qos_real.length);
		 return ret;
	 }

	 public String toString() {
		 String ret = "";

		 if (type == PLACE) {
			 ret = "PLACE ";
		 } else {
			 ret = "TRAN ";
		 }

		 for (int i = 0; i < color.size(); i++) {
			 ret = ret + color.get(i) + " ";
		 }     

		 return ret + " " + name.trim().replaceAll("http.*#", "#");
	 }

	 public int compareTo(Vertice v) {
		 return getName().compareTo(v.getName());
	 }
	 
	 public Vertice getCompensationTransition(){
		 Vertice ret = null;
		 if(!(type == TRANSITION) || !(isCompensa() || isCompensaRet()))
			 return ret;
		 
		 ret = new Vertice(name + "_C",type,color.get(0),qos,qos_real);
		 
		 return ret;
		 
	 }

	public void setState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	public void setEquivalents(List<Vertice> equivalents) {
		this.equivalents = equivalents;
	}

	public List<Vertice> getEquivalents() {
		return equivalents;
	}
	
	public void setQoS(int index, double value) {
		this.qos_real[index] = value;
	}

}
