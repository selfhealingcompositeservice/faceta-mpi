package ve.usb.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * Configuration properties of the COMPOSER.
 * Read from the file composer.conf.
 *
 * @author Rafael Angarita
 * 
 */
public class Config {
	
	public static RunningProperties loadProperties() throws Exception {
			
		RunningProperties runningP = new RunningProperties();
		Properties p = new Properties();
				
		//Composer configuration
		String fileName = "composer.conf";
		InputStream is = new FileInputStream(fileName);
		
		p.load(is);
				
		runningP.setRunComposer(new Boolean(p.getProperty("composer.runcomposer")));
		runningP.setLoadPetriNetFromFile(new Boolean(p.getProperty("composer.loadpetrinetfromfile")));
		runningP.setRisk((new Integer(p.getProperty("composer.risk"))).intValue());
		runningP.setRunExhaustive(new Boolean(p.getProperty("composer.run.exhaustive")));
		
		return runningP;
	}

}