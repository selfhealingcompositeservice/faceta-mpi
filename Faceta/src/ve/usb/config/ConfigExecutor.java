package ve.usb.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import ve.usb.executor.FTMechanism.Mechanism;

/**
 * Configuration properties of the EXECUTOR.
 * Read from the file executor.conf.
 *
 *@author Rafael Angarita
 *
 */
public class ConfigExecutor {
	
	public static ExecutorRunningProperties loadProperties() throws Exception {
		
		ExecutorRunningProperties runningP = new ExecutorRunningProperties();
		Properties p = new Properties();
		
		//Composer configuration
		String fileName = "executor.conf";
		InputStream is = new FileInputStream(fileName);
		
		p.load(is);
		
		//service failure probability
		//runningP.setServiceExecutionFailure(new Double(p.getProperty("executor.service.execution.failure")));
		
		//fault tolerant mechanism to be used
		String ftMechanism = new String(p.getProperty("executor.ftmechanism"));
		
		if(ftMechanism.equals("BR"))
			runningP.setFtMechanism(Mechanism.BACKWARD_RECOVERY);
		else if(ftMechanism.equals("FR"))
			runningP.setFtMechanism(Mechanism.FORWARD_RECOVERY);
		else if
		(ftMechanism.equals("CHECKPOINTING"))
			runningP.setFtMechanism(Mechanism.CHECKPOINTING);
		else
			runningP.setFtMechanism(Mechanism.NIL);

		//query to run
		runningP.setQueryResultFileName(new String(p.getProperty("executor.queryresult.file")));
		
		//delta percent
		runningP.setDeltaPercent(new Double(p.getProperty("executor.delta.percent")));
		
		return runningP;
	}
}