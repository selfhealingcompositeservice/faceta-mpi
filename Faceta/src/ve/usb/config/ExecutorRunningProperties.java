package ve.usb.config;

import java.util.ArrayList;
import java.util.List;

import ve.usb.executor.EUtil;
import ve.usb.executor.FTMechanism.Mechanism;

/**
 * Running properties of the EXECUTOR
 * 
 * @author Rafael Angarita
 * 
 */
public class ExecutorRunningProperties {
	
	
	private static final double HOMOGEINITY_LEVEL_HIGH = 1;
	private static final double HOMOGEINITY_LEVEL_MED = 0.5;
	private static final double HOMOGEINITY_LEVEL_LOW = 0.9;
	
	private static final double FAILURE_PROB_HIGH = 0.2;
	private static final double FAILURE_PROB_MED = 0.1;
	private static final double FAILURE_PROB_LOW = 0.05;
	private static final double FAILURE_PROB_FIX = -1;
	
	private static final double FIABILITY_LEVEL_HIGH = 0.1;
	private static final double FIABILITY_LEVEL_MED = 0.5;
	private static final double FIABILITY_LEVEL_LOW = 0.9;
	
	/**
	 * QoS
	 * The failure probability for a WS
	 * serviceExecutionFailure = [0,1]; where 0 means no failure
	 */
	private double failureProbability = 0.05;
	private Mechanism ftMechanism;
	private String queryResultFileName;
	private double deltaPercent;
	
	/**
	 * QoS
	 * Probability that the WS will be executed in the Estimated Execution Time
	 * timeReliability = [0,1]; where 0 means the WS always executes in the Estimated Execution Time
	 */
	@Deprecated
	private double timeReliability = 0.1;
	
	/**
	 * The degree of variation of the Real Execution Time of a WS according to its Estimated Execution Time
	 * timeVariation = [0,1]; where 0 means that there will be no variation; i.e.,
	 * RealExecutionTime(WS) = EstimatedExecutionTime(WS) + EstimatedExecutionTime(WS)*timeVariation
	 */
	private double timeVariation = 0.1;
	
	/** Difference degree between WSs; e.g.; 
	 * high homogeneity level -> all WSs have the same failure probability /\  all WSs have the same time reliability 
	 * homogeneityLevel = [0,1]; where 1 means homogeneous, i.e.; 0% variation between the properties
	 * */
	private double homogeneityLevel = 1;
	
	/**
	 * Replication:
	 * Execute all replicas
	 */
	private boolean replication = true;

	public void setServiceExecutionFailure(double serviceExecutionFailure) {
		this.failureProbability = serviceExecutionFailure;
	}

	public double getServiceExecutionFailure() {
		return failureProbability;
	}

	public void setFtMechanism(Mechanism ftMechanism) {
		this.ftMechanism = ftMechanism;
	}

	public Mechanism getFtMechanism() {
		return ftMechanism;
	}

	public void setQueryResultFileName(String queryResultFileName) {
		this.queryResultFileName = queryResultFileName;
	}

	public String getQueryResultFileName() {
		return queryResultFileName;
	}

	public double getDeltaPercent() {
		return deltaPercent;
	}

	public void setDeltaPercent(double deltaPercent) {
		this.deltaPercent = deltaPercent;
	}

	public double getTimeReliability() {
		return timeReliability;
	}

	public void setTimeReliability(double timeReliability) {
		this.timeReliability = timeReliability;
	}

	public double getHomogeneityLevel() {
		return homogeneityLevel;
	}

	public void setHomogeneityLevel(double homogeneityLevel) {
		this.homogeneityLevel = homogeneityLevel;
	}

	public double getTimeVariation() {
		return timeVariation;
	}

	public void setTimeVariation(double timeVariation) {
		this.timeVariation = timeVariation;
	}
	
	public List<NodeProperties> getQoSNodes(int nodesNumber) {
		
		this.replication = false;
		
		this.homogeneityLevel = HOMOGEINITY_LEVEL_HIGH;
		this.failureProbability = FAILURE_PROB_LOW;
		this.timeVariation = FIABILITY_LEVEL_LOW;
		
		int min = 0;
		int max = (int) (100-this.homogeneityLevel*100);
		
		List<NodeProperties> qosNodes = new ArrayList<NodeProperties>();
		
		
		for(int i=0; i<nodesNumber; i++) {
			NodeProperties node = new NodeProperties();
			
			node.setReplication(this.replication);
			
			double nodeVariation = min + (int)(Math.random() * ((max - min) + 1));

			//System.out.println("nodeVariation=" + nodeVariation);
			
			//failure 
			boolean positive = EUtil.generateRandBoolean(0.5);
			double failure = this.failureProbability*100;
			
			failure += positive ? nodeVariation:-nodeVariation;
			failure = (failure > 100) ? 100 : ((failure < 0) ? 0 : failure);
			
			
			//System.out.println("failure = " + failure);
			node.setFailure(failure/100);
			
			//time variation
			positive = EUtil.generateRandBoolean(0.5);
			double variation = this.timeVariation*100;
			
			variation += positive ? nodeVariation:-nodeVariation;
			variation = (variation > 100) ? 100 : variation;

			
			node.setTimeVariation(variation/100);
			
			qosNodes.add(node);
			
		}
		
		if(this.failureProbability == -1) {
			int serviceToFail = 0 + (int)(Math.random() * ((9 - 0) + 1));
			for(NodeProperties node:qosNodes) {
				if(qosNodes.indexOf(node) == serviceToFail)
					node.setFailure(1);
				else
					node.setFailure(0);
			}
		}
		
		
		
		return qosNodes;
	}
	
	public static void main(String...args) {
		System.out.println("Test");
		
		ExecutorRunningProperties p = new ExecutorRunningProperties();
		
		
		List<NodeProperties> qoSNodes = p.getQoSNodes(10);
		
		for(NodeProperties n: qoSNodes)
			System.out.println("Index=" + qoSNodes.indexOf(n) + "," + n);
		
	}

	public boolean isReplication() {
		return replication;
	}

	public void setReplication(boolean replication) {
		this.replication = replication;
	}
	
}