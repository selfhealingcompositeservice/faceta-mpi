package ve.usb.config;

import java.io.Serializable;

import ve.usb.executor.EUtil;

/**
 * 
 * @author rafa
 *
 */

public class NodeProperties implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double failure;

	private double timeVariation;
	
	private boolean replication;
	
	public double getFailure() {
		return failure;
	}
	public void setFailure(double failureProbability) {
		this.failure = failureProbability;
	}
	public double getTimeVariation() {
		return timeVariation;
	}
	public void setTimeVariation(double timeVariation) {
		this.timeVariation = timeVariation;
	}
	@Override
	public String toString() {

		return "[timeVariation=" + timeVariation + ",failure=" + failure + ",execution=" + EUtil.generateRandBoolean(1-failure) +  "]";
	}
	public boolean isReplication() {
		return replication;
	}
	public void setReplication(boolean replication) {
		this.replication = replication;
	}
	
}