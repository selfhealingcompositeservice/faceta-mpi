package ve.usb.config;

/**
 * Running properties of the COMPOSER
 * 
 * @author Rafael Angarita
 * 
 */
public class RunningProperties {
	
	private Boolean runComposer = false;
	private Boolean loadPetriNetFromFile = false;
	private int risk;
	private Boolean runExhaustive = false;
	private int processesNumber;
	
	public void setRunComposer(Boolean runComposer) {
		this.runComposer = runComposer;
	}
	public Boolean getRunComposer() {
		return runComposer;
	}
	public void setLoadPetriNetFromFile(Boolean loadPetriNetFromFile) {
		this.loadPetriNetFromFile = loadPetriNetFromFile;
	}
	public Boolean getLoadPetriNetFromFile() {
		return loadPetriNetFromFile;
	}
	public void setRisk(int risk) {
		this.risk = risk;
	}
	public int getRisk() {
		return risk;
	}
	public void setRunExhaustive(Boolean runExhaustive) {
		this.runExhaustive = runExhaustive;
	}
	public Boolean getRunExhaustive() {
		return runExhaustive;
	}
	public void setProcessesNumber(int processesNumber) {
		this.processesNumber = processesNumber;
	}
	public int getProcessesNumber() {
		return processesNumber;
	}
	
}