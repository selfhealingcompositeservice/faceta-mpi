package ve.usb.config;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Loads the URIs contained in the file named "uris".
 * Each URI points to set of Services and Queries.
 * 
 * URIs beginning with "#" are not loaded.
 * 
 * @author Rafael Angarita
 * 
 */
public class URILoader {
	
	private final static char COMMENT = '#';
	private final static String FILE_NAME = "uris";
	
	public static List<URI> loadURIs() {
	
		List<URI> uriList = new ArrayList<URI>();
			
		try{
			
			FileInputStream fstream = new FileInputStream(FILE_NAME);
	
			DataInputStream in = new DataInputStream(fstream);
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
	
			while ((line = br.readLine()) != null)	{
				
				if(line.charAt(0) != COMMENT)
					uriList.add(new URI(line));
			}
	
			in.close();
					
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return uriList;
	}
}