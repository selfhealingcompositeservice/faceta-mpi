package ve.usb.executor;

import java.util.Map;

/**
 * Utilitary operations related to the EXECUTOR.
 *
 * @author Rafael Angarita
 * 
 */
public class EUtil {
	
	/**
	 * Returns a random boolean value with probability p
	 * of being true and probability 1-p of being false
	 * p should be in the range 0.0 - 1.0
	 */
	public static boolean generateRandBoolean(double p) {
	    return (Math.random() < p);
	}
	
	/**
	 * Only works in Unix terminals
	 * @throws InterruptedException 
	 */
	public static synchronized void printWaiting(String message) throws InterruptedException{
		
		System.out.print("Waiting for: " + message + "  "); 
    	for (char c :"-|\\/-|/".toCharArray ()) {
		    System.out.print("\b" + c); 
		    Thread.sleep (250);
    	}
    	System.out.println();
	}
	
	
	public static void printInvocationMessage(String message, double time) {
		System.out.println("Invoking: " +  message + ". Expected duration: " + time + " sgs");
	}
	
	public static void printResults(Map<String, String> result) {
		System.out.println("I received all values I wanted: " + result);
	}

}
