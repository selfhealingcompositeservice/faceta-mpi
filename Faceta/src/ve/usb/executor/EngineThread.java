package ve.usb.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import ve.usb.PetriNets.Vertice;
import ve.usb.executor.FTMechanism.Mechanism;
import ve.usb.executor.mpi.Message.Neighbor;

/**
 * Engine Thread of the EXECUTOR
 *
 * @author Rafael Angarita
 * 
 */
public abstract class EngineThread {
	
	protected String name; //The Engine Thread name (The Service name)
	protected Map<String,Integer> inputs;
	protected Map<String, Integer> receivedInputCard;
	protected boolean success = false;
	protected boolean compensate = false;
	@Deprecated
	protected double EXECUTION_SUCCESS;
	//variation of the QoS: Failure Probability /\ Estimated Execution TIme
	protected double failureProbability;
	protected double timeVariation;
	protected double estimatedExecutionTime; //Total estimated execution time of the TCWS
	protected double estimatedExecutionTimeLeft; //Total estimated execution time left from this Engine Thread until the end of the TCWS
	protected double deltaPercent = -1;
	protected List<String> outputDependency;
	
	protected boolean replicate = true;
	
	protected Map<String, Boolean> predecessors;
	
	/*
	 *  ftMechanism = Mechanism.NIL -> any mechanism can be used
	 *  ftMechanism = Mechanism.BACKWARD_RECOVERY -> only backward recovery can used
	 *  ftMechanism = Mechanism.FORWARD_RECOVERY -> only forward recovery can used
	 */
	protected Mechanism ftMechanism = Mechanism.NIL;
	
	protected Vertice wsET;
	
	//collected data
	protected EngineThreadResult results;

	//replacing phase related variables
	protected boolean cantry;
	protected int tries;
	protected List<Vertice> equivalents;
	protected int MAX_TRIES = 2;
	
	protected Logger logger = Logger.getLogger(EngineThread.class);
	
	
	public EngineThread() {
		predecessors = new HashMap<String, Boolean>();
		BasicConfigurator.configure();
	}

	@Deprecated
	protected void saveInput(Map<String, String> i, Map<String, Integer> inputs) {
		for (String k : i.keySet()) {
			if (inputs.containsKey(k)) {
				if (!receivedInputCard.containsKey(k)) {
					receivedInputCard.put(k, new Integer(1));
				} else {
					receivedInputCard.put(k, receivedInputCard.get(k) + 1);
				}
			}
		}
	}

	@Deprecated
	protected boolean isNotFirable(Map<String, Integer> inputs) {
		boolean ret = false;
		if (receivedInputCard == null) {
			receivedInputCard = new HashMap<String, Integer>();
			ret = true;
		} else {
			for (String k : inputs.keySet()) {
				if (!receivedInputCard.containsKey(k)) {
					ret = true;
				} else {
					Integer expected = inputs.get(k);
					Integer received = receivedInputCard.get(k);
					if (expected.compareTo(received) != 0) {
						ret = true;
					}
				}
			}
		}

		return ret;
	}
	
	protected boolean isNotFirable() {
		return this.predecessors.containsValue(false);
	}
	
	protected void setPredecessorFinished(String predecessorName) {
		this.predecessors.put(predecessorName, true);
	}
	
	protected Vertice  getBestQualityService() {
		Vertice best = null;
		
		for(Vertice v:equivalents) {
			if(best == null)
				best = v;
			else if(v.getQuality() > best.getQuality())
				best = v;
		}
		
		return best;
	}

	public Mechanism getFtMechanism() {
		return ftMechanism;
	}

	public void setFtMechanism(Mechanism ftMechanism) {
		this.ftMechanism = ftMechanism;
	}
	
	public EngineThreadResult getResults() {
		return results;
	}

	public void setResults(EngineThreadResult results) {
		this.results = results;
	}
	
	public void setPredecessors(List<Neighbor> predecessorsList) {
		
		for(Neighbor n:predecessorsList) {
			this.predecessors.put(n.getName(), false);
		}
	}

	public double getEstimatedExecutionTime() {
		return estimatedExecutionTime;
	}

	public void setEstimatedExecutionTime(double estimatedExecutionTime) {
		this.estimatedExecutionTime = estimatedExecutionTime;
	}

	public double getEstimatedExecutionTimeLeft() {
		return estimatedExecutionTimeLeft;
	}

	public void setEstimatedExecutionTimeLeft(double estimatedExecutionTimeLeft) {
		this.estimatedExecutionTimeLeft = estimatedExecutionTimeLeft;
	}

	public double getFailureProbability() {
		return failureProbability;
	}

	public void setFailureProbability(double failureProbability) {
		this.failureProbability = failureProbability;
	}

	public double getTimeVariation() {
		return timeVariation;
	}

	public void setTimeVariation(double timeVariation) {
		this.timeVariation = timeVariation;
	}
	
}
