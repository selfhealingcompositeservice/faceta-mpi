package ve.usb.executor;

import java.io.Serializable;

/**
 * Contains data related to a Engine Thread execution
 *
 * @author Rafael Angarita
 * 
 */
public class EngineThreadResult implements Serializable {
	
	private static final long serialVersionUID = 1L;

	//the name of the transition
	private String name;
	
	//total cost of executed wss:
	//- w(ws) + (w(ws_replace) | w(ws'))
	private double totalCost = 0;
	
	//the ft mechanism used to repair the fail
	private FTMechanism.Mechanism ftMechanism = FTMechanism.Mechanism.NIL;
	
	//says if the service was compensated or not
	private boolean compensated = false;
	
	//numbers of executions reintents. Usefull for (r) services
	private int reintents = 0;
	
	//the total time of the replacing phase
	private double replacingTime = 0;
	
	//the Real Execution Time elapsed since the beginning of the execution of the TCWS until the failure of the current node
	private double failureRealExecutionTime;
	
	//int value indicating replication outcome
	//0: no replication
	//1: replication but original WS was successful
	//2: replication and a replica was neccesary
	//3: all services failed
	private int replicationState = 0;
	
	private boolean checkpointingSkip = false;
	
	//indicates whether the ws was executed successfully; independently of it was later compensated
	private boolean successfullyExecuted = false;
	
	//indicates whether the ws failed and FR was not performed
	private boolean failed = false;
	
	private int outputsDependency = 0;

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setFtMechanism(FTMechanism.Mechanism ftMechanism) {
		this.ftMechanism = ftMechanism;
	}

	public FTMechanism.Mechanism getFtMechanism() {
		return ftMechanism;
	}

	@Override
	public String toString() {
		String ret = this.name + " result. \n";
		ret += this.getFtMechanism() + "\n";
		ret += "compensated: " + this.isCompensated() + "\n";
		
		return ret;
	}

	public void setCompensated(boolean compensated) {
		this.compensated = compensated;
	}

	public boolean isCompensated() {
		return compensated;
	}

	@Override
	public boolean equals(Object obj) {
		
		return this.name.equals(((EngineThreadResult)obj).getName());
	}

	public void setExecutions(int executions) {
		this.reintents = executions;
	}

	public int getExecutions() {
		return reintents;
	}

	public void setReplacingTime(double replacingTime) {
		this.replacingTime = replacingTime;
	}

	public double getReplacingTime() {
		return replacingTime;
	}

	public double getFailureRealExecutionTime() {
		return failureRealExecutionTime;
	}

	public void setFailureRealExecutionTime(double failureRealExecutionTime) {
		this.failureRealExecutionTime = failureRealExecutionTime;
	}

	public int getReplicationState() {
		return replicationState;
	}

	public void setReplicationState(int replicationState) {
		this.replicationState = replicationState;
	}

	public boolean isCheckpointingSkip() {
		return checkpointingSkip;
	}

	public void setCheckpointingSkip(boolean checkpointingSkip) {
		this.checkpointingSkip = checkpointingSkip;
	}

	public boolean isSuccessfullyExecuted() {
		return successfullyExecuted;
	}

	public void setSuccessfullyExecuted(boolean successfullyExecuted) {
		this.successfullyExecuted = successfullyExecuted;
	}

	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	public int getOutputsDependency() {
		return outputsDependency;
	}

	public void setOutputsDependency(int outputsDependency) {
		this.outputsDependency = outputsDependency;
	}

	
}