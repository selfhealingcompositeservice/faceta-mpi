package ve.usb.executor;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.executor.FTMechanism.Mechanism;

/**
 * Execution Engine of the EXECUTOR
 *
 * @author Rafael Angarita
 * 
 */
public abstract class ExecutionEngine {
	
	protected Logger logger = Logger.getLogger(ExecutionEngine.class);
	
	protected Query query;
	protected MarkedPN cpn_tcws_q;
	protected MarkedPN br_cpn_tcws_q;
	protected Vertice wsee_f;
	protected Vertice wsee_i;
	protected List<EngineThreadResult> results;
	protected double compensationTime;
	protected double deltaPercent;
	protected Map<String, List<String>> outputDependencies;
	
	
	/*
	 *  ftMechanism = Mechanism.NIL -> any mechanism can be used
	 *  ftMechanism = Mechanism.BACKWARD_RECOVERY -> only backward recovery can used
	 *  ftMechanism = Mechanism.FORWARD_RECOVERY -> only forward recovery can used
	 */
	protected Mechanism ftMechanism = Mechanism.NIL;
	
	public MarkedPN getCpn_tcws_q() {
		return cpn_tcws_q;
	}
	
	public MarkedPN getBr_cpn_tcws_q() {
		return br_cpn_tcws_q;
	}

	public Mechanism getFtMechanism() {
		return ftMechanism;
	}

	public void setFtMechanism(Mechanism ftMechanism) {
		this.ftMechanism = ftMechanism;
	}

	public double getCompensationTime() {
		return compensationTime;
	}

	public void setCompensationTime(double compensationTime) {
		this.compensationTime = compensationTime;
	}

	public double getDeltaPercent() {
		return deltaPercent;
	}

	public void setDeltaPercent(double deltaPercent) {
		this.deltaPercent = deltaPercent;
	}
	
	public void setOutputDependencies(Map<String, List<String>> outputDependencies) {
		this.outputDependencies = outputDependencies;
		
	}
		
}
