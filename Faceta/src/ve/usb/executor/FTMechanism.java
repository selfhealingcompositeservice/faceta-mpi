package ve.usb.executor;

import java.io.Serializable;

/**
 * Fault Tolerant Mechanisms
 *
 * @author Rafael Angarita
 * 
 */
public class FTMechanism implements Serializable {
	
	private static final long serialVersionUID = 1L;

	//Fault-tolerance mechanism
	
	public enum Mechanism {
		NIL,
		BACKWARD_RECOVERY,
		FORWARD_RECOVERY,
		REINTENT_RECOVERY,
		CHECKPOINTING,
		REPLICATION
	}

}