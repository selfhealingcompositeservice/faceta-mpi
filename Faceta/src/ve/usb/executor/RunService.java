package ve.usb.executor;

import java.net.URI;

//import org.mindswap.owl.OWLFactory;
//import org.mindswap.owl.OWLKnowledgeBase;
import org.mindswap.owl.OWLFactory;
import org.mindswap.owl.OWLIndividual;
import org.mindswap.owl.OWLKnowledgeBase;
import org.mindswap.owl.OWLValue;
import org.mindswap.owls.OWLSFactory;
import org.mindswap.owls.process.Process;
import org.mindswap.owls.process.execution.DefaultProcessMonitor;
import org.mindswap.owls.process.execution.ProcessExecutionEngine;
import org.mindswap.owls.process.variable.Input;
import org.mindswap.owls.process.variable.Output;
import org.mindswap.owls.service.Service;
import org.mindswap.query.ValueMap;

//import com.sun.jmx.snmp.Timestamp;

/**
 * Handles the execution of a OWLS Service
 *
 * @author Rafael Angarita
 * 
 */
public class RunService {
	
	Service service;
	Process process;
	String inValue;
	String outValue;
	ValueMap<Input, OWLValue> inputs;
	ValueMap<Output, OWLValue> outputs;
	ProcessExecutionEngine exec;
	double qosExecutionTime = 0;
	
	public RunService() {
		// create an execution engine
		exec = OWLSFactory.createExecutionEngine();

		// Attach a listener to the execution engine
		exec.addMonitor(new DefaultProcessMonitor());
	}
	
	public RunService(double qosExecutionTime) {
		// create an execution engine
		exec = OWLSFactory.createExecutionEngine();

		// Attach a listener to the execution engine
		exec.addMonitor(new DefaultProcessMonitor());
		this.qosExecutionTime = qosExecutionTime;
	}
	
	public void run(URI uri) throws Exception {
		
		
		//final OWLKnowledgeBase kb = OWLFactory.createKB();

		//service = kb.readService(uri);
		//process = service.getProcess();
		
		//EUtil.printInvocationMessage(service.getName(), (EXECUTION_TIME/1000));
		//System.out.println("Executing service '" + uri + "' " + new Timestamp(System.currentTimeMillis()));
		
		
		System.out.println(uri + " Execution time - sleep: " + this.qosExecutionTime);
		Thread.sleep((long) this.qosExecutionTime);

		
		// initialize the input values to be empty
		inputs = new ValueMap<Input, OWLValue>();

		//System.out.println(process.getInputs());
		//inputs.setValue(process.getInput("City"), kb.createDataValue("College Park"));
		//inputs.setValue(process.getInput("State"), kb.createDataValue("MD"));

		//outputs = exec.execute(process, inputs, kb);

		// get the result
		//final OWLIndividual out = outputs.getIndividualValue(process.getOutput());

		// display the results
		System.out.println("Executed service '" + uri + "'");
		/*System.out.println("Grounding WSDL: " + ((AtomicProcess) process).getGrounding().getDescriptionURL());
		System.out.println("City   = " + "College Park");
		System.out.println("State  = " + "MD");
		System.out.println("Output = ");*/
		//System.out.println(Utils.formatRDF(out.toRDF(true, true)));
		System.out.println();
	}

}