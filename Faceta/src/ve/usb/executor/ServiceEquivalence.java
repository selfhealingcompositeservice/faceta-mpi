package ve.usb.executor;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.mindswap.owls.process.variable.Input;
import org.mindswap.owls.process.variable.Output;
import org.mindswap.owls.service.Service;

import ve.usb.KnowledgedeBase.KnowledegeBase;
import ve.usb.PetriNets.Vertice;

/**
 * Gets the Equivalent Services of a given Service
 *
 * @author Rafael Angarita
 * 
 */
public class ServiceEquivalence {
	
	private Vertice s;
	private URI baseUri;
	private List<String> sInputs;
	private List<String> sOutputs;
	
	public ServiceEquivalence(Vertice s, URI baseUri) {
		this.baseUri = baseUri;
		this.s = s;
		this.sInputs = new ArrayList<String>();
		this.sOutputs = new ArrayList<String>();
	}
	
	public List<Vertice> getEquivalents() {
		List<Vertice> ret = new ArrayList<Vertice>();
		
		KnowledegeBase kb = new KnowledegeBase(baseUri, 0);
		
		Service s2 = kb.getServiceByName(this.s.getName());
		
		for(Input i:s2.getProcess().getInputs())
			sInputs.add(i.getParamType().getURI().getFragment());
		
		for(Output o:s2.getProcess().getOutputs())
			sOutputs.add(o.getParamType().getURI().getFragment());
		
		System.out.println("Equivalentes de: " + this.s.getName());
		for(Service sOwls:kb.getServices()) {
			 List<Input> serviceInputs = sOwls.getProcess().getInputs();
			 List<String> inputs = new ArrayList<String>();
			 for(Input i:serviceInputs)
				 inputs.add(i.getParamType().getURI().getFragment());
			 
			 List<Output> serviceOutputs = sOwls.getProcess().getOutputs();
			 List<String> outputs = new ArrayList<String>();
			 for(Output o:serviceOutputs)
				 outputs.add(o.getParamType().getURI().getFragment());
			 
			 if(areExactFunctionallyEquivalent(this.sInputs, this.sOutputs, inputs, outputs)) {
				 System.out.println("- " + sOwls.getName());
			 }
		}

		return ret;
	}
	
	private boolean areExactFunctionallyEquivalent(List<String> ins1, List<String> outs1,  List<String> ins2, List<String> outs2) {
		boolean ret = false;
		
		
		//if(ins1.containsAll(ins2) && ins2.containsAll(ins1) && outs1.containsAll(outs2) && outs2.containsAll(outs1))
		if(ins1.containsAll(ins2) & outs2.containsAll(outs1))
			ret = true;
		
		return ret;
	}

}