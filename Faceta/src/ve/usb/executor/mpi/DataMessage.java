package ve.usb.executor.mpi;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DataMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String senderName;
	private Map<String, String> data;
	private double executedTime;
	private boolean checkpoint = false;
	
	public DataMessage(String senderName) {
		this.senderName = senderName;
		data = new HashMap<String, String>();
		
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	
	public void addData(String name, String value) {
		this.data.put(name, value);
	}
	
	public Map<String, String> getAllData() {
		return this.data;
	}

	public double getExecutedTime() {
		return executedTime;
	}

	public void setExecutedTime(double executedTime) {
		this.executedTime = executedTime;
	}

	public boolean isCheckpoint() {
		return checkpoint;
	}

	public void setCheckpoint(boolean checkpoint) {
		this.checkpoint = checkpoint;
	}

}
