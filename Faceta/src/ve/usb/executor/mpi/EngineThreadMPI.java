package ve.usb.executor.mpi;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hp.hpl.jena.reasoner.rulesys.builtins.LessThan;

import mpi.MPI;
import mpi.Request;
import ve.usb.PetriNets.Vertice;
import ve.usb.executor.EUtil;
import ve.usb.executor.EngineThread;
import ve.usb.executor.EngineThreadResult;
import ve.usb.executor.FTMechanism.Mechanism;
import ve.usb.executor.RunService;
import ve.usb.executor.mpi.Message.Neighbor;

/**
 * MPI implementation of the Engine Thread
 *
 * @author Rafael Angarita
 * 
 */
public class EngineThreadMPI extends EngineThread {
	
	private Message message;
	private int executionState = Vertice.INITIAL;
	private int me;
	private double initialTime = 0;
	private int totalOutputsCws = 0;
	
	public void run() throws Exception {
		java.util.Date elapsedTime = null;
		me = MPI.COMM_WORLD.Rank();
		//read initialization values
		Object [] o = new Object[1];
		
		
		/*
		 * Engine Thread initialization
		 */
		
		//wait for initialization information: this step is necessary due to the distributed memory model (only MPI)
		Request rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, ExecutionEngineMPI.MASTER, 0);
		rreq.Wait();
		
		this.message = (Message)o[0];
		name = message.getName();
		this.estimatedExecutionTime = message.getEstimatedExecutionTime();
		this.estimatedExecutionTimeLeft = message.getEstimatedExecutionTimeLeft();
		this.deltaPercent = message.getDeltaPercent();
		this.failureProbability = message.getQosNode().getFailure();
		this.timeVariation = message.getQosNode().getTimeVariation();
		this.replicate = message.getQosNode().isReplication();
		this.outputDependency = message.getOutputDependencies();
		this.totalOutputsCws = message.getTotalOutputsCws();
		//ftMechanism = m.getFtMechanism();
		
		results = new EngineThreadResult();
		results.setName(name);
		results.setOutputsDependency(this.outputDependency.size());
		
		/*
		 * End of Engine Thread initialization
		 */
		
		
		//logger.info("ET started: " + name + "; rank=" + me);
		
		success = false;
		compensate = false; 
		boolean checkpoint = false;
		//Get the inputs required by this transition
		inputs = message.getInputs();
		
		//wait for the following predecessors to finish
		setPredecessors(message.getPredeessors());
		
		//wait for this transition to be firable
		while(isNotFirable()) {
			//at this stage, each Engine Thread can receive two possible messages: an input or the "compensate" string. 
			rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
			rreq.Wait();
			
			logger.debug("Input received: " + o[0] + "; " + name + "; rank=" + me);
			System.out.println(name + "; rank=" + me);
			
			
			if(o[0] instanceof DataMessage) {
				System.out.println("Input received from: " + ((DataMessage) o[0]).getSenderName() + "; " + name + "; rank=" + me);
				DataMessage dataMessage = (DataMessage) o[0];
				setPredecessorFinished(dataMessage.getSenderName());
				
				//get the maximum of the real executed times of predecessors
				if(dataMessage.getExecutedTime() > initialTime)
					initialTime = ((DataMessage) o[0]).getExecutedTime();
				
				checkpoint = checkpoint ? true:dataMessage.isCheckpoint();
				
			} else if(o[0] instanceof String && ((String)o[0]).equals("compensate")) {
				//Another WS failed while I was waiting for my inputs, therefore the Execution Engine sent me the "compensate" message
				//to stop my execution and start compensation
				executeCompensation();
				return;
			}
		}
		elapsedTime = new java.util.Date();
		System.out.println("ETIT " + this.name + ": " + initialTime);
		//the transition has become firable!

		Vertice ws = message.getTransition();
		wsET = ws; 
		//set to running the state of its corresponding transition in BRCPN-TCWS_Q
		executionState = Vertice.RUNNING;
		

		//get the equivalent transitions
		equivalents = ws.getEquivalents(); 
		//execute ws
		do {
			boolean execution;
			if(!checkpoint) {
				
				execution = execute(ws);
				cantry = true;
				tries = 0;
				System.out.println("execution: " + execution + " ft: " + ftMechanism);
			} else {
				results.setCheckpointingSkip(true);
				System.out.println("SKIP");
				execution = false;
			}
			java.util.Date t1Retry = new java.util.Date();
			
			if(!execution) {
				
				this.replicate = false;
				
				Integer tp = message.getTransition().getColor().get(0);
				
				double timeAfterFailure = new java.util.Date().getTime()- elapsedTime.getTime();
				results.setFailureRealExecutionTime(timeAfterFailure);

				if (this.deltaPercent != -1) {
					
					System.out.println("DELTA WS " + this.name);
					System.out.println("DELTA " + this.deltaPercent);
					System.out.println("DELTA EET " + this.estimatedExecutionTime);
					System.out.println("DELTA EETL " + this.estimatedExecutionTimeLeft);
					System.out.println("DELTA RET " + this.initialTime);
					System.out.println("DELTA T(WS) " + (new java.util.Date().getTime()- elapsedTime.getTime()));
					
					System.out.println("DELTA RET + T(WS) " + (this.initialTime + timeAfterFailure));
					System.out.println("DELTA TIME " + (this.estimatedExecutionTime*this.deltaPercent)/100);
					double estimatedExecutionTimeDelta = ((this.estimatedExecutionTime*this.deltaPercent)/100) + this.estimatedExecutionTime;
					System.out.println("DELTA EETDELTA " + estimatedExecutionTimeDelta);
					System.out.println("DELTA NEW EET " + (timeAfterFailure + this.estimatedExecutionTimeLeft));
					
					
					if((this.initialTime + timeAfterFailure + this.estimatedExecutionTimeLeft) > estimatedExecutionTimeDelta) {
						System.out.println("DELTA FR FALSE");
						results.setFtMechanism(Mechanism.BACKWARD_RECOVERY);
						execute(ws);
					} else {
						System.out.println("DELTA FR TRUE");
						results.setFtMechanism(Mechanism.FORWARD_RECOVERY);
						
						//execute the ws again
						execute(ws);
					}

					sendDataToSuccessors(elapsedTime, ws, false);
					
					//this transition did its job correctly
					success = true;
					execution = true;
					
					
				} else if(/*(tp == Vertice.ATOMICRET || tp == Vertice.COMPENSARET || tp == Vertice.PIVOTRET) &&*/ (ftMechanism == Mechanism.NIL)) {
					//logger.info("Retrying Service (r) " + name + "; rank=" + me);
					//reinvoke
					java.util.Date t2Retry = new java.util.Date();
					results.setReplacingTime(results.getReplacingTime() + (t2Retry.getTime()-t1Retry.getTime()));
					results.setFtMechanism(Mechanism.REINTENT_RECOVERY);
					results.setExecutions(results.getExecutions() + 1);
					
					//logger.info("ET cost: " + name + "; rank=" + me + "; " +  + results.getTotalCost() + " + " + ws.getQoSReal()[0]);

					//results.setTotalCost(results.getTotalCost() + ws.getQoSReal()[0]);
					continue;
				} else if(ftMechanism == Mechanism.FORWARD_RECOVERY){
					//the transition is not retriable, therefore we proceed to replace it by an equivalent transition
					java.util.Date t1 = new java.util.Date();
					executeReplacingPhase();
					java.util.Date t2 = new java.util.Date();
					results.setReplacingTime(results.getReplacingTime() + (t2.getTime()-t1.getTime()));
					results.setFtMechanism(Mechanism.FORWARD_RECOVERY);
					results.setExecutions(results.getExecutions() + 1);
					
				} else if(ftMechanism == Mechanism.CHECKPOINTING) { 
					double w1 = 0.8;
					double w2 = 0.2;
					checkpoint = true;
					//Decide here whether to perform checkpointing or backward recovery
					System.out.println("BRVSCKP");
					double realElapsedTime = this.initialTime + timeAfterFailure;
					//percentage of total estimated execution time
					double percentage = (realElapsedTime*100)/this.estimatedExecutionTime;
					System.out.println("BRVSCKP-timepercentage=" + percentage);
					double percentageDependency = (this.outputDependency.size()*100)/this.totalOutputsCws;
					System.out.println("BRVSCKP-outputdependency=" + percentageDependency);
					double finalNormalizedValue =  percentage*w1 + (100-percentageDependency)*w2;
					System.out.println("BRVSCKP-finalNormalizedValue=" + finalNormalizedValue);
					
					compensate = finalNormalizedValue > 50 ? false : true;
					
					cantry = false;
				} else {
					cantry = false;
				}
			} else {
				sendDataToSuccessors(elapsedTime, ws, false);
				//this transition did its job correctly
				success = true;
			}
		} while(!success && cantry);
		
		
		if(!success && compensate) {
			//logger.info("Sending compensate to EE");
			//send "compensate" to Execution Engine
			o[0] = new String("compensate");
			MPI.COMM_WORLD.Isend(o, 0, 1, MPI.OBJECT, 0, 0);

			//set to compensate the state of its corresponding transition in BRCPN-TCWS_Q
			executionState = Vertice.COMPENSATE;
			results.setFailed(true);
			
			executeCompensation();
		} else if(!success && checkpoint) {
			results.setFtMechanism(Mechanism.CHECKPOINTING);
			results.setFailed(true);
			System.out.println("ENVIO CHECKPOINTING SKIP");
			sendDataToSuccessors(elapsedTime, ws, true);
			executeFinalPhase();
		} else {
			this.results.setSuccessfullyExecuted(true);
			executeFinalPhase();
		}

		return;

	}
	
	private boolean execute(Vertice ws) {
		
		boolean execution = false;
		
		try {
			
			if(!isInCriticalPath(ws) || !this.replicate || this.ftMechanism == Mechanism.CHECKPOINTING) {
				RunService runService = new RunService(ws.getQoSReal()[0] + ((ws.getQoSReal()[0]*this.timeVariation)/100));
				runService.run(ws.getURI());
				
				//TODO:encapsular esto en RunService
				execution = EUtil.generateRandBoolean(1-this.getFailureProbability());
			} else {
				
				int numberOfReplicas = 1;
				
				//para cada replica aumento el Tiempo Estimado de Ejecucion y la probabilidad de falla
				Map<Integer, Boolean> replicas = new HashMap<Integer, Boolean>();
				double originalTime = ws.getQoSReal()[0] + ((ws.getQoSReal()[0]*this.timeVariation)/100);
				
				boolean originalExecution = EUtil.generateRandBoolean(1-this.getFailureProbability());
				System.out.println("replication:originalExecution=" + originalExecution);
				replicas.put(0, originalExecution);
				double baseFailyre = this.getFailureProbability() == 1.0 ? 0.2 : this.getFailureProbability();
				for(int i=1;i<=numberOfReplicas;i++) {

					
					int overChargePercentage = 5*i;
					double qosReal = ws.getQoSReal()[0] + ((overChargePercentage/100)*ws.getQoSReal()[0]);
					
					
					double failure = baseFailyre + ((overChargePercentage/100)*baseFailyre);
					
					replicas.put(i, EUtil.generateRandBoolean(1-failure));
				}
				System.out.println("replicasinfo=" + replicas);
				//choose the one with less time and execution true
				double lesserTime = 0;
				if(!originalExecution) {
						
					if(replicas.containsValue(true)) {
						lesserTime = originalTime;
						execution = true;
					}

				} else {
					execution = true;
					lesserTime = originalTime;
				}
				
				if(execution) {
					Thread.sleep((long) lesserTime);
					System.out.println("replication=true,original=" + (originalExecution ? true : false));
					results.setReplicationState(originalExecution ? 1 : 2);
				} else {
					Thread.sleep((long) originalTime);
					System.out.println("replication=false");
					results.setReplicationState(3);
				}
				results.setFtMechanism(Mechanism.REPLICATION);

			}
			
		} catch(Exception e) {
			
			e.printStackTrace();
		}
		
		return execution;
	}
	
	private boolean isInCriticalPath(Vertice ws) {
		
		if (this.deltaPercent != -1) {
			
			double estimatedExecutionTimeDelta = ((this.estimatedExecutionTime*this.deltaPercent)/100) + this.estimatedExecutionTime;
			
			double wsExecutionTime = ws.getQoSReal()[0] + (ws.getQoSReal()[0]*this.timeVariation)/100;
					
			if((this.initialTime + wsExecutionTime*2 + this.estimatedExecutionTimeLeft) > estimatedExecutionTimeDelta) {
				System.out.println("REPLICATE=" + this.name + ",time=" + (this.initialTime + wsExecutionTime*2 + this.estimatedExecutionTimeLeft
						+ ",failure=" + this.failureProbability));
				//results.setFtMechanism(Mechanism.REPLICATION);
				return true;
			} 
		}
		
		return false;
	}
	

	private void sendDataToSuccessors(Date elapsedTime, Vertice ws, boolean checkpoint) {
		//the execution was successful
		
		//add the cost of the executed WS to the total cost
		//logger.info("ET cost: " + name + "; rank=" + me + "; " +  + results.getTotalCost() + " + " + ws.getQoSReal()[0]);

		results.setTotalCost(results.getTotalCost() + ws.getQoSReal()[0]);
		
		//set to executed the state of its corresponding transition in BRCPN-TCWS_Q
		executionState = Vertice.EXECUTED;
		//send results to (ws*)*
		//logger.info("Sending my outputs to ("+ name +"*)*");
		
		for(Neighbor n:message.getSuccessors())  { //for each successor transition
			
			Object [] objectArray = new Object[1];
			DataMessage data = new DataMessage(name);
			//send only one message for each successor
			for(Vertice place:n.getData()) { //for each input place of the successor transition
				
				//logger.debug("sending output: " + place.getName() + "="+ "Output_"+name + " to " + n.getName());
				data.addData(place.getName(), "Output_"+name);
				
			}
			data.setCheckpoint(checkpoint);
			//real execution time elapsed
			data.setExecutedTime(this.initialTime + (new java.util.Date().getTime()- elapsedTime.getTime()));
			System.out.println("ETIT FINISHED" + this.name + ": " + data.getExecutedTime());
			objectArray[0] = data;
			
			MPI.COMM_WORLD.Isend(objectArray, 0, 1, MPI.OBJECT, n.getIndex(), 0);

			
		}
	}
	
	private Vertice executeReplacingPhase() {
		if(equivalents == null) {
			//logger.info("The equivalents list of " + name + " is null");
			cantry = false;
			return null;
		}
			
		//logger.info("Replacig phase of " + name + ". Equivalents: " + equivalents.size());
		Vertice wse = null;
		if(equivalents.size() != 0 && tries < MAX_TRIES) {
			
			wse = getBestQualityService();
			
			
			Integer tp = message.getTransition().getColor().get(0);
			
			if(tp == Vertice.COMPENSA || tp == Vertice.COMPENSARET) {
				
				//ws' is replaced by ws'* on BRCPN
					
			}
			
			tries++;
			equivalents.remove(wse);
		} else {
			cantry = false;
		}
		
		return wse;
	}
	
	private void sendSimpleMessage(List<Neighbor> neighborList, String message) {
		//send control tokens to successors
		for(Neighbor n:neighborList)  {
			Object [] objectArray = new Object[1];
			objectArray[0] = message;
			MPI.COMM_WORLD.Isend(objectArray, 0, 1, MPI.OBJECT, n.getIndex(), 0);
		}
	}

	private void executeCompensation() throws Exception {
		
		//it is necessary to wait for all the Engine Threads to reach the compensation state to prevent deadlocks.
		//If not, it is possible to one Engine Thread to be waiting for its input, then receive a compensation input,
		// and finally, receive the compensation message.
		//Note that the compensation message could be received after the actual compensation input message.
		//logger.info("Compensation of " + name + " executionState:" + executionState);
		
		
		//logger.info("Passing the compensation barrier of " + name);
		
		//get the compensation transition
		Message brMessage = message.getBr();
		Vertice wsc = brMessage.getTransition();
		MPI.COMM_WORLD.Barrier();
		if(executionState == Vertice.ABANDONED || executionState == Vertice.COMPENSATE) { //this is the Service that failed
			if(executionState == Vertice.COMPENSATE) {
				//FIXME:borrar
				//logger.info("aqui" + name);
				results.setFtMechanism(Mechanism.BACKWARD_RECOVERY);
			}
			sendSimpleMessage(message.getBr().getSuccessors(), "compensate_token");
		} else {
			//get inputs needed
			receivedInputCard = null;
			inputs = new HashMap<String, Integer>();
			int neededInputs = brMessage.getPredeessors().size();

			while(neededInputs != 0) {
				
				Object[] o = new Object[1];
				Request rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
				rreq.Wait();
				System.out.println("(o[0]=" + (o[0]));
				//logger.debug(wsc.getName() + " compensation input received: " +  o[0]
				//		+ ".\n I have : " + receivedInputCard
				//		+ "\n Required : " + neededInputs);
				if(o[0] instanceof String && ((String)o[0]).compareTo("compensate_token")==0) {
					neededInputs--;
				}	
				
			}
			//ws' has become fireable
			//logger.info(wsc.getName() + " has become fireable (compensation) ");
			
			if(executionState == Vertice.INITIAL) { 
				//it was never executed, no need for compensation
				executionState = Vertice.ABANDONED;
			} else {
				
				if(executionState == Vertice.RUNNING) {
					//wait for ws to finish
					
					//invoke ws'
					//logger.info("Invoking (compensation) " + wsc.getName());
					RunService runService = new RunService(wsET.getQoSReal()[0]/2);
					runService.run(wsc.getURI());
					executionState = Vertice.COMPENSATE;
					
					results.setCompensated(true);
					//add the cost of the executed WS to the total cost
					results.setTotalCost(results.getTotalCost() + wsc.getQoSReal()[0]);
					
				} else if(executionState == Vertice.EXECUTED) {
					
					//invoke ws'
					//logger.info("Invoking (compensation) " + wsc.getName());
					RunService runService = new RunService(wsET.getQoSReal()[0]/2);
					runService.run(wsc.getURI());
					executionState = Vertice.COMPENSATE;
					results.setCompensated(true);
					//add the cost of the executed WS to the total cost
					results.setTotalCost(results.getTotalCost() + wsc.getQoSReal()[0]);
				}
				
			}
			//send results to (ws'*)*
			//logger.info("Sending my outputs to ("+ wsc.getName() +"'*)*: " + brMessage.getSuccessors());
			sendSimpleMessage(message.getBr().getSuccessors(), "compensate_token");
		}
		System.out.println("Finishing compensation of " + name + "; rank: " + me + " FTM: " + results.getFtMechanism());
		
		
		//wait for finish message and send results
		MPI.COMM_WORLD.Barrier();
		Object[] o = new Object[1];
		while(true) {
			//logger.info("Final Phase Compensation - " + name + "; rank=" + me);
			Request rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
			rreq.Wait();
			//System.out.println("Msg received in final phase: " + name);
			if(o[0] instanceof String) {
				//FIXME:borrar
				//logger.info("Final Phase Compensation received - " + o[0] );
				if(((String)o[0]).equals("finish")) {
					//System.out.println("finishing: " + name);
					//send finish message to my predecessors
					sendSimpleMessage(this.message.getPredeessors(), "finish");
					sendResults();
					//System.out.println("returning: " + name);
					return;
				}
				
			}
		}

	}
	
	private void sendResults() {
		Object [] f = new Object[1];
		f[0] = results;
		MPI.COMM_WORLD.Isend(f, 0, 1, MPI.OBJECT, 0, 0);
	}
	
	private void executeFinalPhase() throws Exception {
		//Final phase
		//wait for message: finish or compensate
		//System.out.println("Final Phase - " + name + "; rank=" + me);
		Object[] o = new Object[1];
		while(true) {
			System.out.println("waiting in final phase");
			//logger.info("Final Phase - " + name + "; rank=" + me);
			Request rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
			rreq.Wait();
			//System.out.println("Msg received in final phase: " + name);
			if(o[0] instanceof String) {
				if(((String)o[0]).equals("finish")) {
					//System.out.println("finishing: " + name);
					//send finish message to my predecessors
					sendSimpleMessage(this.message.getPredeessors(), "finish");
					sendResults();
					//System.out.println("returning: " + name);
					return;
				} else if(((String)o[0]).equals("compensate")) {
					System.out.println("NO waiting in final phase");
					executeCompensation();
					
					return;
				} 
			}
		}
	}
	

	public void setExecutionState(int executionState) {
		this.executionState = executionState;
	}

	public int getExecutionState() {
		return executionState;
	}

}