package ve.usb.executor.mpi;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mpi.MPI;
import mpi.Request;
import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.config.NodeProperties;
import ve.usb.executor.EUtil;
import ve.usb.executor.EngineThreadResult;
import ve.usb.executor.ExecutionEngine;
import ve.usb.executor.mpi.Message.Neighbor;

/**
 * MPI implementation of the Execution Engine
 *
 * @author Rafael Angarita
 * 
 */
public class ExecutionEngineMPI extends ExecutionEngine {
	
	@SuppressWarnings("unused")
	private String  [] args;
	public static final int MASTER = 0;
	private Map<String,Integer> transitionIndex;
	private Map<String,String> output;
	private int outputReceived = 0;
	private double initialTime;


	public int getOutputReceived() {
		return outputReceived;
	}

	public void setOutputReceived(int outputReceived) {
		this.outputReceived = outputReceived;
	}

	private double executionFailure; //double executionFailure de un servicio
	private int faultyNodeIndex;
	private double elapsedTimeNodes;
	private Map<String, Double> costFromNodes;
	private int maximumCostPath;
	private List<NodeProperties> qosNodes;
	
	public ExecutionEngineMPI(double executionFailure) {
		this.executionFailure = executionFailure;
	}
	
	public ExecutionEngineMPI(Query query, MarkedPN cpn_tcws_q, MarkedPN br_cpn_tcws_q) {
		this.query = query;
		this.cpn_tcws_q = cpn_tcws_q;
		this.br_cpn_tcws_q = br_cpn_tcws_q;
		this.results = new ArrayList<EngineThreadResult>();
	}
	
	public ExecutionEngineMPI(Query query, MarkedPN cpn_tcws_q , String[] args, double initialTime, 
		int maximumCostPath, Map<String, Double> costFromNodes, List<NodeProperties> qosNodes) {
		
		if(MPI.COMM_WORLD.Rank()==MASTER) {
			//System.out.println(cpn_tcws_q.toString());
			this.query = query;
			this.cpn_tcws_q = cpn_tcws_q;
			this.br_cpn_tcws_q = this.cpn_tcws_q.createBRPN();
			this.args = args;
			this.results = new ArrayList<EngineThreadResult>();
			this.initialTime = initialTime;
			String output = this.br_cpn_tcws_q.toString();
			//System.out.println("-------BR CPN ------");
			//System.out.println(output);
			this.maximumCostPath = maximumCostPath;
			this.costFromNodes = costFromNodes;
			this.qosNodes = qosNodes;
		}
	}
	
	public ExecutionEngineMPI(Query query, MarkedPN cpn_tcws_q , String[] args) {
		
		if(MPI.COMM_WORLD.Rank()==MASTER) {
			//System.out.println(cpn_tcws_q.toString());
			this.query = query;
			this.cpn_tcws_q = cpn_tcws_q;
			this.br_cpn_tcws_q = this.cpn_tcws_q.createBRPN();
			this.args = args;
			this.results = new ArrayList<EngineThreadResult>();
			String output = this.br_cpn_tcws_q.toString();
			//System.out.println("-------BR CPN ------");
			//System.out.println(output);
		}
	}
	
	public ExecutionEngineMPI() {
		// TODO Auto-generated constructor stub
	}

	public synchronized List<EngineThreadResult> run() throws Exception {
		
		java.util.Date t1 = new java.util.Date();
		
		int me = MPI.COMM_WORLD.Rank();
		
		if(me == MASTER) {
			
			//Algorithm 1:Execution Engine
			
			//task i: add dummy transitions
			
			logger.info("Q_I: " + query.getIns());
			logger.info("Q_O: " + query.getOuts());
			
			//WSee_i
			wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
			cpn_tcws_q.addWebService(wsee_i, new ArrayList<Vertice>(), query.getIns());
			br_cpn_tcws_q.addWebService(wsee_i,  query.getIns(),new ArrayList<Vertice>());
			
			//WSee_f
			wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
			cpn_tcws_q.addWebService(wsee_f, query.getOuts(), new ArrayList<Vertice>());
			br_cpn_tcws_q.addWebService(wsee_f, new ArrayList<Vertice>(),query.getOuts());
			
			/*System.out.println("-----CPPN with dummy places-------");
			System.out.println(cpn_tcws_q.toString());
			System.out.println("-----BRCPPN with dummy places-------");
			System.out.println(br_cpn_tcws_q.toString());*/
			
			//task ii: mark the cpn with the Initial state and
			//mark all transitions in brcpn in initial state
			cpn_tcws_q.setInitialMarking(query);
			br_cpn_tcws_q.setInitialMarkingBR(query);
			
			//set the ranks of each transition
			transitionIndex = setRanks(cpn_tcws_q.getTransitions());
			
			
			//task iii. start engine threads: send initialization values
			for(int i=0; i<cpn_tcws_q.getTransitions().size(); i++) {
				
				Vertice v = cpn_tcws_q.getTransitions().get(i);
				
				//Start Execution Engine for all transitions except WSee_i and WSee_f
				if(!v.getName().equals("WSee_f") && !v.getName().equals("WSee_i")) {
					
					List<Neighbor> successors = getSuccessors(v, cpn_tcws_q);
					List<Neighbor> predecessors =getPredecessors(v, cpn_tcws_q);

					//le envio la informacion de sus (ws*)* y cuales son sus ws*
					//de esta manera, el proceso sabra por que inputs debe esperar y a quien enviarle su output
					//FIXME: el valor de "i" enviado  al constructor de Message deberia ser i+1 ?
					Message message = new Message(v.getName(), v, i, successors ,  predecessors
												, cpn_tcws_q.getInputList(v), cpn_tcws_q.sucesores(v.getName())
												, ftMechanism , this.qosNodes.get(i)
												, this.outputDependencies.get(v.getName())
												, query.getOuts().size()
												);
					//br information
					message.setBr(getBRTransition(v));
					message.setFaultyNodeIndex(faultyNodeIndex);
					//total estimated execution time
					message.setEstimatedExecutionTime(this.maximumCostPath);
					//estimatedExecutionTime from node
					message.setEstimatedExecutionTimeLeft(this.costFromNodes.get(v.getName()));
					//delta percent
					message.setDeltaPercent(this.deltaPercent);
					
					Object [] objectArray = new Object[1];
					objectArray[0] = message;
					
					Request sreq = MPI.COMM_WORLD.Isend(objectArray, 0, 1, MPI.OBJECT, i+1, 0);
					sreq.Wait();
					
				}
			}
			
			//task iv: send values I_Q to Execution Threads representing (WSee_i*)*
			logger.info("task iv: send values I_Q to Execution Threads representing (WSee_i*)*");
			/*System.out.println("WSee_i*: ");
			System.out.println(cpn_tcws_q.sucesores(wsee_i));
			System.out.println("(WSee_i*)*: ");
			System.out.println(cpn_tcws_q.getSucTransitions(wsee_i));
			
			
			//I'm the father of everything
			System.out.println("(wsee_i*)*: " + cpn_tcws_q.getSucTransitions(wsee_i));*/
			for(Vertice v:cpn_tcws_q.getSucTransitions(wsee_i))  {
				
				Object [] objectArray = new Object[1];
				DataMessage data = new DataMessage(wsee_i.getName());
				
				int inputNumber=0; //inputNumber is used only for visualization effects
				for(String input:cpn_tcws_q.predecesores(v.getName())) {
					
					data.addData(input, input+"_"+wsee_i.getName()+"_"+(inputNumber++));

					
					//logger.debug("Sending I_Q to: " + v.getName() + ". Input: " + inputs);
					
				}
				int index = transitionIndex.get(v.getName());
				
				//real execution time elapsed
				data.setExecutedTime(this.initialTime + (new java.util.Date().getTime()- t1.getTime()));
				
				objectArray[0] = data;
				Request sreq = MPI.COMM_WORLD.Isend(objectArray, 0, 1, MPI.OBJECT, index, 0);
				sreq.Wait();
				
			}
			logger.info("Done sending I_Q values");
			
			executeFinalPhase();
			logger.info("Done executing final phase");
		} else {
			EngineThreadMPI et = new EngineThreadMPI();
			et.setFtMechanism(ftMechanism);
			et.run();	
		}
		//System.out.println("Done " + me);
		return results;
	}
	
	@SuppressWarnings("unchecked")
	private void executeFinalPhase() {
		int size = MPI.COMM_WORLD.Size();
		
		//real execution time elapsed from the point of view of nodes
		double elapsedTimeNodes = 0;

		//wait for *(*wseef)
		output = new HashMap<String, String>();
		
		Map<String,Boolean> predecessorsFinished = new HashMap<String, Boolean>();
		
		for(Vertice p:cpn_tcws_q.getPredTransitions(wsee_f)) {
			predecessorsFinished.put(p.getName(), false);
		}
		
		int outputsSize = query.getOuts().size();
		System.out.println("EE: waiting for outputs");
		while(outputsSize != 0 && predecessorsFinished.containsValue(false)) {
			System.out.println("predecessorsFinished: " + predecessorsFinished);
			Object [] o = new Object[1];
			Request rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
			rreq.Wait();
			//System.out.println("Received output master final phase: " + o[0]);
			if(o[0] instanceof String && o[0].equals("compensate") ) {
				logger.info("have to compensate");
				System.out.println("have to compensate");
				executeCompensation();
				MPI.COMM_WORLD.Barrier();
				logger.info("Done compensation");
				//kill all idle processes
				//killIdleProcesses(cpn_tcws_q, size);
				//return;
				break;
			} else if (o[0] instanceof DataMessage) {
				
				predecessorsFinished.put(((DataMessage)o[0]).getSenderName(), true);
				
				//get the maximum of the real executed times of predecessors
				if(((DataMessage) o[0]).getExecutedTime() > elapsedTimeNodes)
					elapsedTimeNodes = ((DataMessage) o[0]).getExecutedTime();
			
			} else {

				Map<String,String> test =  (Map<String,String>)o[0];
				logger.info("Query answer: " + (Map<String,String>)o[0]);
				
				for(String s:test.keySet()) {
					if(query.getOuts().contains(new Vertice(s, 0))) {
						if(!output.containsKey((Map<String,String>)o[0])) {
							output.putAll((Map<String,String>)o[0]);
							outputsSize--;
							outputReceived++;
						}
					}
				}
				
			}
		}
		System.out.println("EE: nodes elapsed time: " + elapsedTimeNodes);
		this.setElapsedTimeNodes(elapsedTimeNodes);
		System.out.println("EE: waiting for outputs finished");
		EUtil.printResults(output);
		
		int totalOutputs = output.values().size();
		int ckpOoutputs = 0;
		for(String o:output.values())
			if(o.contains("CHECKPOINT"))
				ckpOoutputs++;
		
		//double pCorrectOutputs = 100-(ckpOoutputs*100)/totalOutputs;
		//System.out.println("TOTAL OUTPUTS PERCENTAGE: " + pCorrectOutputs);
		
		//Send finish message to *(*wsee_f) 
		logger.info("Sending finish message to *(*wsee_f) ");
		//System.out.println("Sending finish message to *(*wsee_f) " + cpn_tcws_q.getPredTransitions(wsee_f));
		for(Vertice v:cpn_tcws_q.getPredTransitions(wsee_f))  {
		//for(int i=1; i<MPI.COMM_WORLD.Size(); i++) {
			int index = transitionIndex.get(v.getName());
			Object [] o = new Object[1];
			o[0] = new String("finish");
			Request sreq = MPI.COMM_WORLD.Isend(o, 0, 1, MPI.OBJECT, index, 0);
			//sreq.Wait();
		}
		logger.info("Execution Engine end sending finish message");
		//killIdleProcesses(cpn_tcws_q, size);
		//wait for (wsee_i*)* to finish
		//for(Vertice v:cpn_tcws_q.getSucTransitions(wsee_i))  {
		while(true) {
			Object [] o = new Object[1];
			Request rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
			rreq.Wait();
			//System.out.println("Execution Engine received end of final phase: " + o[0]);
			if(o[0] instanceof EngineThreadResult && !results.contains((EngineThreadResult)o[0]))
				results.add((EngineThreadResult)o[0]);
			
			if(results.size() == cpn_tcws_q.getNumProcecsos()-2)
				break;
		}
		logger.info("Execution Engine end of final phase");
	}
	
	private void executeCompensation() {
		
		//send compensate to all Engine Threads
		//logger.info("send compensate to all Engine Threads");
		
		java.util.Date t1 = new java.util.Date();
		
		for(int i=1; i<MPI.COMM_WORLD.Size(); i++) {
			Object [] o = new Object[1];
			o[0] = new String("compensate");
			//System.out.println("sending compensate to " + i);
			//Request sreq = MPI.COMM_WORLD.Isend(o, 0, 1, MPI.OBJECT, transitionIndex.get(v.getName()), 0);
			MPI.COMM_WORLD.Isend(o, 0, 1, MPI.OBJECT,  i , 0);
			//sreq.Wait();
		}
		//logger.info("Setting the barrier");
		MPI.COMM_WORLD.Barrier();
		
		//logger.info("All transititions received compensate. send control values to *(*wsee_f')");
		//send control values to *(*wsee_f')
		for(Vertice v:cpn_tcws_q.getPredTransitions(wsee_f))  {
			Object [] objectArray = new Object[1];
			
			/*int inputNumber=0;
			for(String input:br_cpn_tcws_q.predecesores(cpn_tcws_q.getCompensationTransition(v.getName()))) {
				if(cpn_tcws_q.predecesores(wsee_f.getName()).contains(input)) {
					Map<String,String> inputs = new HashMap<String, String>();
					inputs.put(input, input+"_"+wsee_i.getName()+"_"+(inputNumber++));
					//System.out.println("Sending control values to *(*wsee_f'): " + v.getName() + " - " + inputs);
					objectArray[0] = inputs;
					int index = transitionIndex.get(v.getName());
					MPI.COMM_WORLD.Isend(objectArray, 0, 1, MPI.OBJECT, index, 0);
					//sreq.Wait();
				}
			}*/
			int index = transitionIndex.get(v.getName());
			objectArray[0] = "compensate_token";
			MPI.COMM_WORLD.Isend(objectArray, 0, 1, MPI.OBJECT, index, 0);
			
		}
		
		//logger.info("Done sending I_Q compensation values");
		
		//wait for control values from (wsee_i'*)*
		output = new HashMap<String, String>();
		
		
		int outputsSize = cpn_tcws_q.getSucTransitions(wsee_i).size();
		
		
		while(outputsSize != 0) {
			Object [] o = new Object[1];
			Request rreq = MPI.COMM_WORLD.Irecv(o, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
			rreq.Wait();

			if(o[0] instanceof String && ((String)o[0]).compareTo("compensate_token")==0)
				outputsSize--;

		}
		java.util.Date t2 = new java.util.Date();
	    
	    compensationTime = t2.getTime() - t1.getTime();
		logger.info("Compensation output: " + output);
	}
	
	private Map<String,Integer> setRanks(List<Vertice> l) {
		Map<String,Integer> ret = new HashMap<String, Integer>();
		
		int i=1;
		for(Vertice v:l) {
			//System.out.println("** " + v.getName() + " - " + (i+1));
			if(v.getName().equals("WSee_i") ||(v.getName().equals("WSee_f")))
					ret.put(v.getName(), 0);
			else
					ret.put(v.getName(), i);
			i++;
		}
		//System.out.println(ret);
		return ret;
	}
	
	private List<Neighbor> getSuccessors(Vertice v, MarkedPN pn) {
		List<Neighbor> ret = new ArrayList<Neighbor>();
		List<Vertice> out = new ArrayList<Vertice>();
		List<Vertice> listOutpuPlaces = pn.sucesores(v);
		for(Vertice s:pn.getSucTransitions(v)) {
			//System.out.println("transicion succ: " + s.getName());
			Neighbor n = (new Message()).new Neighbor();
			out = new ArrayList<Vertice>();
			n.setName(s.getName());
			if(transitionIndex.get(s.getName())!=null)
					n.setIndex(transitionIndex.get(s.getName()));
			else
				n.setIndex(transitionIndex.get(cpn_tcws_q.getCompensatedTransition(s.getName())));
			for(Vertice pre :pn.predecesores(s)) {
				//System.out.println("input succ: " + pre.getName());
				if(listOutpuPlaces.contains(pre) && !out.contains(pre)) {
					out.add(pre);
				}
			}
			//System.out.println(v.getName() + ": " + out + " de " + s.getName());
			n.setData(out);
			ret.add(n);
		}
		return ret;
	}
	
	private List<Neighbor> getPredecessors(Vertice v, MarkedPN pn) {

		List<Neighbor> ret = new ArrayList<Neighbor>();
		List<Vertice> out = new ArrayList<Vertice>();
		List<Vertice> listInputPlaces = pn.predecesores(v);
		for(Vertice s:pn.getPredTransitions(v)) {
			//System.out.println("transicion pred: " + s.getName());
			out = new ArrayList<Vertice>();
			Neighbor n = (new Message()).new Neighbor();
			out = new ArrayList<Vertice>();
			n.setName(s.getName());
			if(transitionIndex.get(s.getName())!=null)
				n.setIndex(transitionIndex.get(s.getName()));
			else
				n.setIndex(transitionIndex.get(cpn_tcws_q.getCompensatedTransition(s.getName())));
			for(Vertice pre :pn.sucesores(s)) {
				
				if(listInputPlaces.contains(pre) && !out.contains(pre)) {
					out.add(pre);
				}
			}
			//System.out.println(v.getName() + ": " + out + " de " + s.getName());
			n.setData(out);
			ret.add(n);
		}
		
		return ret;
	}
	

	private Message getBRTransition(Vertice v) {
		Message m = new Message();
		
		if(v.getTransactionalProperty() == Vertice.COMPENSA
				|| v.getTransactionalProperty() == Vertice.COMPENSARET) {
		
			String brName = cpn_tcws_q.getCompensationTransition(v.getName());
			
			Vertice br = br_cpn_tcws_q.getTransition(brName);
			m.setName(brName);
			m.setIndex(transitionIndex.get(v.getName()));
			
			//it has the same RANK of the transition it compensates
			transitionIndex.put(brName, m.getIndex());
			
			m.setTransition(br);
			m.setPredeessors(getPredecessors(br, br_cpn_tcws_q));
			m.setSuccessors(getSuccessors(br, br_cpn_tcws_q));
			m.setInputs(br_cpn_tcws_q.getInputList(br));
			m.setOutputs(br_cpn_tcws_q.sucesores(v.getName()));
		}

		return m;
	}
	
	public Map<String, String> getOutput() {
		return output;
	}

	public void setOutput(Map<String, String> output) {
		this.output = output;
	}

	@Deprecated
	public void setFaultyNode(int nodeIndex) {
		this.faultyNodeIndex = nodeIndex;
		
	}

	public double getInitialTime() {
		return initialTime;
	}

	public void setInitialTime(double initialTime) {
		this.initialTime = initialTime;
	}

	public double getElapsedTimeNodes() {
		return elapsedTimeNodes;
	}

	public void setElapsedTimeNodes(double elapsedTimeNodes) {
		this.elapsedTimeNodes = elapsedTimeNodes;
	}
	
	/*private void killIdleProcesses(MarkedPN pn, int size) {
		//kill all idle processes
		int idleProcesses = size - pn.getTransitions().size()+1;
		if(size>0) {
			for(int i=0, j=size-1; i<idleProcesses;i++,j--) {
				System.out.println("Killing " + j);
				Object [] objectArray = new Object[1];
				objectArray[0] = "finish";
				Request sreq = MPI.COMM_WORLD.Isend(objectArray, 0, 1, MPI.OBJECT, j, 0);
				sreq.Wait();
			}
		}
	}*/
	
}
