package ve.usb.executor.mpi;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import ve.usb.PetriNets.Vertice;
import ve.usb.config.NodeProperties;
import ve.usb.executor.FTMechanism.Mechanism;

/**
 * Message to be sent to MPI Engine Threads
 *
 * @author Rafael Angarita
 * 
 */
public class Message implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private Vertice transition;
	private int index;
	private List<Neighbor> successors;
	private List<Neighbor> predeessors;
	private Map<String,Integer> inputs;
	private List<String> outputs;
	private Message br;
	private double estimatedExecutionTime;
	private double deltaPercent;
	private List<String> outputDependencies;
	private int totalOutputsCws;
	/*
	 *  ftMechanism = Mechanism.NIL -> any mechanism can be used
	 *  ftMechanism = Mechanism.BACKWARD_RECOVERY -> only backward recovery can used
	 *  ftMechanism = Mechanism.FORWARD_RECOVERY -> only forward recovery can used
	 */
	protected Mechanism ftMechanism = Mechanism.NIL;

	private int faultyNodeIndex;

	private Double estimatedExecutionTimeLeft;

	private NodeProperties qosNode;

	public Message() {
		super();
	}
	
	public Message(String name, Vertice transition, int index, List<Neighbor> successors, List<Neighbor> predeessors,  Map<String,Integer> inputs,
			List<String> outputs) {
		this.name = name;
		this.transition = transition;
		this.index = index;
		this.successors = successors;
		this.predeessors = predeessors;
		this.inputs = inputs;
		this.outputs = outputs;
	}
	
	public Message(String name, Vertice transition, int index, List<Neighbor> successors, List<Neighbor> predeessors,  Map<String,Integer> inputs,
			List<String> outputs, Mechanism ftMechanism, NodeProperties qosNode, List<String> outputDependencies, int totalOutputsCws) {
		this(name, transition, index, successors, predeessors, inputs,  outputs);
		this.ftMechanism = ftMechanism;
		this.setQosNode(qosNode);
		this.setOutputDependencies(outputDependencies);
		this.totalOutputsCws = totalOutputsCws;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Neighbor> getSuccessors() {
		return successors;
	}

	public void setSuccessors(List<Neighbor> successors) {
		this.successors = successors;
	}

	public List<Neighbor> getPredeessors() {
		return predeessors;
	}

	public void setPredeessors(List<Neighbor> predeessors) {
		this.predeessors = predeessors;
	}

	public void setTransition(Vertice transition) {
		this.transition = transition;
	}

	public Vertice getTransition() {
		return transition;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public void setInputs( Map<String,Integer> inputs) {
		this.inputs = inputs;
	}

	public  Map<String,Integer> getInputs() {
		return inputs;
	}

	public void setOutputs(List<String> outputs) {
		this.outputs = outputs;
	}

	public List<String> getOutputs() {
		return outputs;
	}
	
	public void setBr(Message br) {
		this.br = br;
	}

	public Message getBr() {
		return br;
	}
	
	

	public Mechanism getFtMechanism() {
		return ftMechanism;
	}

	public void setFtMechanism(Mechanism ftMechanism) {
		this.ftMechanism = ftMechanism;
	}



	public class Neighbor implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String name;
		private int index;
		private List<Vertice> data;
		
		public Neighbor() {
			super();
		}
		
		public Neighbor(String name, int index, List<Vertice> data) {
			super();
			this.name = name;
			this.index = index;
			this.data = data;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		public int getIndex() {
			return index;
		}
		public void setData(List<Vertice> data) {
			this.data = data;
		}
		public List<Vertice> getData() {
			return data;
		}

		@Override
		public String toString() {
			String ret;
			ret = this.name + " ("+this.data+ ")";
			return ret;
		}
	}
	
	@Override
	public String toString() {
		
		String ret;
		ret = this.name + " suc("+this.successors+")";
		return ret;
	}

	public void setFaultyNodeIndex(int faultyNodeIndex) {
		this.faultyNodeIndex = faultyNodeIndex;
		
	}

	public int getFaultyNodeIndex() {
		return faultyNodeIndex;
	}

	public void setEstimatedExecutionTimeLeft(Double estimatedExecutionTimeLeft) {
		this.estimatedExecutionTimeLeft = estimatedExecutionTimeLeft;
		
	}
	
	public Double getEstimatedExecutionTimeLeft() {
		return this.estimatedExecutionTimeLeft;
		
	}

	public double getDeltaPercent() {
		return deltaPercent;
	}

	public void setDeltaPercent(double deltaPercent) {
		this.deltaPercent = deltaPercent;
	}

	public double getEstimatedExecutionTime() {
		return estimatedExecutionTime;
	}

	public void setEstimatedExecutionTime(double estimatedExecutionTime) {
		this.estimatedExecutionTime = estimatedExecutionTime;
	}

	public NodeProperties getQosNode() {
		return qosNode;
	}

	public void setQosNode(NodeProperties qosNode) {
		this.qosNode = qosNode;
	}

	public List<String> getOutputDependencies() {
		return outputDependencies;
	}

	public void setOutputDependencies(List<String> outputDependencies) {
		this.outputDependencies = outputDependencies;
	}

	public int getTotalOutputsCws() {
		return totalOutputsCws;
	}

	public void setTotalOutputsCws(int totalOutputsCws) {
		this.totalOutputsCws = totalOutputsCws;
	}
	
	
}
