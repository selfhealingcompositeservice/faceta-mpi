package ve.usb.executor.thread;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;

import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.executor.EUtil;
import ve.usb.executor.EngineThread;
import ve.usb.executor.EngineThreadResult;
import ve.usb.executor.FTMechanism.Mechanism;
import ve.usb.executor.RunService;

/**
 * Threads implementation of the Engine Thread
 *
 * @author Rafael Angarita
 * 
 */
public class EngineThreadThread extends EngineThread  implements Callable<EngineThreadResult>  {
	
	private static MarkedPN cpn_tcws_q;
	private BlockingQueue<Map<String, String>> queue;
	private static MarkedPN br_cpn_tcws_q;
	public static int reachedCompensation = 0;
	private Map<String,LinkedBlockingDeque<Map<String, String>>> queues;

	
	public EngineThreadThread(MarkedPN pn, MarkedPN brpn, Map<String,LinkedBlockingDeque<Map<String, String>>> queues, Mechanism ftMechanism, String name) {
		cpn_tcws_q = pn;
		br_cpn_tcws_q = brpn;
		this.queue = null;
		this.queues = queues;
		this.results = new EngineThreadResult();
		this.name = name;
		this.ftMechanism = ftMechanism;
	}
	
	public EngineThreadThread(MarkedPN pn, MarkedPN brpn,
			Map<String, LinkedBlockingDeque<Map<String, String>>> queues,
			Mechanism ftMechanism, String name, double executionFailure) {
		cpn_tcws_q = pn;
		br_cpn_tcws_q = brpn;
		this.queue = null;
		this.queues = queues;
		this.results = new EngineThreadResult();
		this.name = name;
		this.ftMechanism = ftMechanism;
		this.EXECUTION_SUCCESS = executionFailure;
	}

	public EngineThreadResult call() {
		
		try {
			logger.info("ET started: " + name);
			//name = Thread.currentThread().getName();
			this.results.setName(name);
			//get the corresponding queue
			this.queue = queues.get(name);
			
			success = false;
			compensate = false;
			
			//Get the inputs required by this transition
			inputs = cpn_tcws_q.getInputList(name);
			
			//wait for this transition to be firable
			while(isNotFirable(inputs)) {
				//at this stage, each Engine Thread can receive two possible messages: an input or the "compensate" string.
					Map<String,String> inputReceived = (Map<String,String>)queue.take();
					
					logger.debug("Input received: " + inputReceived + ", I am " + name);
					if(inputReceived.get("compensate") == null)
						saveInput(inputReceived, inputs);
					else {
						//Another WS failed while I was waiting for my inputs, therefore the Execution Engine sent me the "compensate" message
						//to stop my execution and start compensation
						executeCompensation();
						return results;
					}
				
			}
			
			//the transition has become firable!
			
			Vertice ws = cpn_tcws_q.getTransition(name);
			
			//set to running the state of its corresponding transition in BRCPN-TCWS_Q
			br_cpn_tcws_q.getTransition(cpn_tcws_q.getCompensationTransition(name)).setState(Vertice.RUNNING);
	
	
			//get the equivalent transitions
			equivalents = ws.getEquivalents(); 
			//execute ws
			do {
				//execute
				RunService runService = new RunService();
				runService.run(ws.getURI());
	
				boolean execution = EUtil.generateRandBoolean(1-EXECUTION_SUCCESS);
				cantry = true;
				tries = 0;
				
				if(!execution) {
					Integer tp = cpn_tcws_q.getTransition(name).getColor().get(0);
					if((tp == Vertice.ATOMICRET || tp == Vertice.COMPENSARET || tp == Vertice.PIVOTRET) && (ftMechanism == Mechanism.NIL)) {
						logger.info("Retrying Service (r) " + name);
						//reinvoke ws
						continue;
					} else if((ftMechanism == Mechanism.NIL) || (ftMechanism == Mechanism.FORWARD_RECOVERY)){
						//the transition is not retriable, therefore we proceed to replace it by an equivalent transition
						ws = executeReplacingPhase();
						results.setFtMechanism(Mechanism.FORWARD_RECOVERY);
					} else {
						cantry = false;
					}
						
				} else {
					//the execution was successful
					
					//add the cost of the executed WS to the total cost
					logger.info("ET cost: " + name + "; " +  + results.getTotalCost() + " + " + ws.getQoSReal()[0]);

					results.setTotalCost(results.getTotalCost() + ws.getQoSReal()[0]);
					
					//set to executed the state of its corresponding transition in BRCPN-TCWS_Q
					br_cpn_tcws_q.getTransition(cpn_tcws_q.getCompensationTransition(name)).setState(Vertice.EXECUTED);
					//send results to (ws*)*
					logger.info("Sending my outputs to ("+ name +"*)*");
					
					for(Vertice s:cpn_tcws_q.getSucTransitions(cpn_tcws_q.getTransition(name)))  { //for each successor transition
						for(Vertice place:cpn_tcws_q.predecesores(s)) { //for each input place of the successor transition
							
							if(cpn_tcws_q.sucesores(name).contains(place.getName())) {
								Map<String,String> inputs = new HashMap<String, String>();
								logger.debug("sending output: " + place.getName() + "="+ "Output_"+name + " to " + s.getName());
								inputs.put(place.getName(), "Output_"+name);
								
								queues.get(s.getName()).put(inputs);
							}
						}
					}
					//this transition did its job correctly
					success = true;
				}
			} while(!success && cantry);
			
			   
			if(!success) {
				//send "compensate" to Execution Engine
				System.out.println(name + ": sendig compensate to EE. Queue cap: " + queues.get("WSee_f").remainingCapacity());
				Map<String,String> inputs = new HashMap<String, String>();
				inputs.put("compensate", "compensate");
		
				queues.get("WSee_f").put(inputs);
				
				//set to compensate the state of its corresponding transition in BRCPN-TCWS_Q
				br_cpn_tcws_q.getTransition(cpn_tcws_q.getCompensationTransition(name)).setState(Vertice.COMPENSATE);
				executeCompensation();
			} else {
				executeFinalPhase();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}
	
	private Vertice executeReplacingPhase() {
		if(equivalents == null) {
			logger.info("The equivalents list of " + name + " is null");
			cantry = false;
			return null;
		}
		logger.info("Replacing phase of " + name + ". Equivalents: " + equivalents.size());
		Vertice wse = null;
		if(equivalents.size() != 0 && tries < MAX_TRIES) {
			
			wse = getBestQualityService();
			
			
			Integer tp = cpn_tcws_q.getTransition(name).getColor().get(0);
			
			if(tp == Vertice.COMPENSA || tp == Vertice.COMPENSARET) {
				
				//ws' is replaced by ws'* on BRCPN
				
			}
			
			
			tries++;
			equivalents.remove(wse);
		} else {
			cantry = false;
		}
		
		return wse;
	}
	
	private void executeCompensation() throws Exception {
		
		
		
		queue.clear();
		//it is necessary to wait for all the Engine Threads to reach the compensation state to prevent deadlocks.
		//If not, it is possible to one Engine Thread to be waiting for its input, then receive a compensation input,
		// and finally, receive the compensation message.
		//Note that the compensation message could be received after the actual compensation input message.
		logger.info("Compensation of " + name);
		
		reachedCompensation = reachedCompensation + 1;
		//System.out.println("reachedCompensation: " + reachedCompensation + " of " + name);
		
		while(reachedCompensation!=cpn_tcws_q.getTransitions().size()-1);
		logger.info("Passing the compensation barrier of " + name);
		
		//get the compensation transition
		Vertice wsc = br_cpn_tcws_q.getTransition(cpn_tcws_q.getCompensationTransition(name));
		
		if(wsc.getState() == Vertice.ABANDONED || wsc.getState() == Vertice.COMPENSATE) { //this is the Service that failed
			if(wsc.getState() == Vertice.COMPENSATE)
				results.setFtMechanism(Mechanism.BACKWARD_RECOVERY);
			//send control tokens to successors
			for(Vertice s:br_cpn_tcws_q.getSucTransitions(wsc))  {
				for(Vertice place:br_cpn_tcws_q.predecesores(s)) {
					
					if(br_cpn_tcws_q.sucesores(wsc.getName()).contains(place.getName())) { 
						Map<String,String> inputs = new HashMap<String, String>();
						System.out.println("sending output: " + place.getName() + "="+ "Output_"+name + " to " + s.getName());
						inputs.put(place.getName(), "Output_"+name);
						//FIXME
						if(s.getName().equals("WSee_i"))
							//ExecutionEngineThread.queues.get("WSee_f").put(inputs);
							queues.get("WSee_f").put(inputs);
						else
							//ExecutionEngineThread.queues.get(cpn_tcws_q.getCompensatedTransition(s.getName())).put(inputs);
							queues.get(cpn_tcws_q.getCompensatedTransition(s.getName())).put(inputs);
					}
				}
			}
		} else {
			//get inputs needed
			receivedInputCard = null;
			inputs = new HashMap<String, Integer>();
			Map<String,Integer> neededInputs = br_cpn_tcws_q.getInputList(wsc.getName());
			while(isNotFirable(neededInputs)) {
				try {
					Map<String,String> inputReceived = (Map<String,String>)queue.take();
					logger.debug(wsc.getName() + " compensation input received: " + inputReceived
							+ ".\n I have : " + receivedInputCard
							+ "\n Required : " + neededInputs);
					saveInput(inputReceived, neededInputs);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			//ws' has become fireable
			logger.info(wsc.getName() + " has become fireable (compensation) ");
			
			if(wsc.getState() == Vertice.INITIAL) {
				//it was never executed, no need for compensation
				wsc.setState(Vertice.ABANDONED);
			} else {
				if(wsc.getState() == Vertice.RUNNING) {
					//wait ws finishes
					
					//invoke ws'
					logger.info("Invoking (compensation) " + wsc.getName());
					RunService runService = new RunService();
					runService.run(wsc.getURI());
					wsc.setState(Vertice.COMPENSATE);
					results.setCompensated(true);
					//add the cost of the executed WS to the total cost
					results.setTotalCost(results.getTotalCost() + wsc.getQoSReal()[0]);
				} else if(wsc.getState() == Vertice.EXECUTED) {
					//invoke ws'
					logger.info("Invoking (compensation) " + wsc.getName());
					RunService runService = new RunService();
					runService.run(wsc.getURI());
					wsc.setState(Vertice.COMPENSATE);
					results.setCompensated(true);
					//add the cost of the executed WS to the total cost
					results.setTotalCost(results.getTotalCost() + wsc.getQoSReal()[0]);
				}
				
				
			}
			//send results to (ws'*)*
			logger.info("Sending my outputs to ("+ wsc.getName() +"'*)*: " + br_cpn_tcws_q.getSucTransitions(wsc));
			for(Vertice s:br_cpn_tcws_q.getSucTransitions(wsc))  {
				for(Vertice place:br_cpn_tcws_q.predecesores(s)) {
					
					if(br_cpn_tcws_q.sucesores(wsc.getName()).contains(place.getName())) {
						Map<String,String> inputs = new HashMap<String, String>();
						logger.debug("sending output: " + place.getName() + "="+ "Output_"+name + " to " + s.getName() + "'");
						inputs.put(place.getName(), "Output_"+name+"'");
						//FIXME
						if(s.getName().equals("WSee_i"))
							//ExecutionEngineThread.queues.get("WSee_f").put(inputs);
							queues.get("WSee_f").put(inputs);
						else
							//ExecutionEngineThread.queues.get(cpn_tcws_q.getCompensatedTransition(s.getName())).put(inputs);
							queues.get(cpn_tcws_q.getCompensatedTransition(s.getName())).put(inputs);
					}
				}
			}
		}
		logger.info("Finishing compensation of " + name);
	}
	
	private void executeFinalPhase() throws Exception {
		//Final phase
		//wait for message: finish or compensate
		while(true) {
			logger.info("Final Phase - " + name);
			Map<String,String> message = (Map<String,String>) queues.get(name).take();
			//System.out.println("Msg received in final phase: " + name);
			if(message.get("finish") != null) {
				//System.out.println("finishing: " + name);
				//send finish message to my predecessors
				for(Vertice s:cpn_tcws_q.getPredTransitions(cpn_tcws_q.getTransition(name)))  {
					Map<String,String> inputs = new HashMap<String, String>();
					inputs.put("finish", "finish");
					logger.debug("Sending finish message to: "  + s.getName());
					if(s.getName().equals("WSee_i"))
						//ExecutionEngineThread.queues.get("WSee_f").put(inputs);
						queues.get("WSee_f").put(inputs);
					else
						//ExecutionEngineThread.queues.get(s.getName()).put(inputs);
						queues.get(s.getName()).put(inputs);
				}
			
				//System.out.println("returning: " + name);
				return;
			} else if(message.get("compensate") != null) {
				executeCompensation();
				//System.out.println("returning: " + name);
				return;
			}

		}
	}

}