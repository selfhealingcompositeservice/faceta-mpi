package ve.usb.executor.thread;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.executor.EUtil;
import ve.usb.executor.EngineThreadResult;
import ve.usb.executor.ExecutionEngine;

/**
 * Threads implementation of the Execution Engine
 *
 * @author Rafael Angarita
 * 
 */
public class ExecutionEngineThread extends ExecutionEngine {
	
	public Map<String, LinkedBlockingDeque<Map<String, String>>> queues;

	
	private List<Thread> engineThreads = new ArrayList<Thread>();

	public ExecutionEngineThread(Query query, MarkedPN pn, MarkedPN br_cpn_tcws_q) {
		this.query = query;
		cpn_tcws_q = pn;
		br_cpn_tcws_q = pn.createBRPN();
		queues = null;
		this.results = new ArrayList<EngineThreadResult>();
		
	}
	
	public ExecutionEngineThread(Query query, MarkedPN pn) {
		
		System.out.println(pn.toString());
		this.query = query;
		cpn_tcws_q = pn;
		br_cpn_tcws_q = pn.createBRPN();
		String output = br_cpn_tcws_q.toString();
		System.out.println("-------BR CPN ------");
		System.out.println(output);
		queues = new HashMap<String, LinkedBlockingDeque<Map<String,String>>>();
		this.results = new ArrayList<EngineThreadResult>();
	}
	
	private double executionFailure; //double executionFailure de un servicio
	
	public ExecutionEngineThread(Query query, MarkedPN pn,
			double serviceExecutionFailure) {
		System.out.println(pn.toString());
		this.query = query;
		cpn_tcws_q = pn;
		br_cpn_tcws_q = pn.createBRPN();
		String output = br_cpn_tcws_q.toString();
		System.out.println("-------BR CPN ------");
		System.out.println(output);
		queues = new HashMap<String, LinkedBlockingDeque<Map<String,String>>>();
		this.results = new ArrayList<EngineThreadResult>();
		
		this.executionFailure = serviceExecutionFailure;
	}

	public synchronized List<EngineThreadResult> run() throws InterruptedException, ExecutionException {
		
		//Algorithm 1:Execution Engine
		
		//task i: add dummy transitions
		
		System.out.println(query.getIns());
		System.out.println(query.getOuts());
		
		//WSee_i
		wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
		cpn_tcws_q.addWebService(wsee_i, new ArrayList<Vertice>(), query.getIns());
		br_cpn_tcws_q.addWebService(wsee_i,  query.getIns(),new ArrayList<Vertice>());
		queues.put(wsee_i.getName(), new LinkedBlockingDeque<Map<String, String>>());
		
		//WSee_f
		wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
		cpn_tcws_q.addWebService(wsee_f, query.getOuts(), new ArrayList<Vertice>());
		br_cpn_tcws_q.addWebService(wsee_f, new ArrayList<Vertice>(),query.getOuts());
		queues.put(wsee_f.getName(), new LinkedBlockingDeque<Map<String, String>>());
		
		System.out.println("-----CPPN with dummy places-------");
		System.out.println(cpn_tcws_q.toString());
		System.out.println("-----BRCPPN with dummy places-------");
		System.out.println(br_cpn_tcws_q.toString());
		
		//task ii: mark the cpn with the Initial state and
		//mark all transitions in brcpn in initial state
		cpn_tcws_q.setInitialMarking(query);
		br_cpn_tcws_q.setInitialMarkingBR(query);
		
		//task iii. start engine threads: send initialization values
		
		ExecutorService pool = Executors.newFixedThreadPool(cpn_tcws_q.getNumProcecsos()-2);
		Set<Future<EngineThreadResult>> set = new HashSet<Future<EngineThreadResult>>();
		
		for(Vertice tr : cpn_tcws_q.getTransitions()) {
			
			//Start Execution Engine for all transitions except WSee_i and WSee_f
			if(!tr.getName().equals("WSee_f") && !tr.getName().equals("WSee_i")) {
				
				//create the queue for the new Engine Thread
				queues.put(tr.getName(), new LinkedBlockingDeque<Map<String, String>>());
				
				//each Engine Thread is initialized with the reference to the cpn_tcws_q, br_cpn_tcws_q, and queues objects
				//this is only possible within the shared memory model
				EngineThreadThread engineThread = new EngineThreadThread(cpn_tcws_q, br_cpn_tcws_q, queues, ftMechanism, tr.getName(), executionFailure); 
				//Thread t = new Thread(engineThread);
				Callable<EngineThreadResult> callable = engineThread;
				
				Future<EngineThreadResult> future = pool.submit(callable);
				set.add(future);
				
				//the name of the thread is the name of the original service of the cpn_tcws_q
				//(the service could be replaced afterwards)
				//t.setName(tr.getName());
				//t.start();
				
			}
		}
		
		//task iv: send values I_Q to Execution Threads representing (WSee_i*)*
		
		System.out.println("WSee_i*: ");
		System.out.println(cpn_tcws_q.sucesores(wsee_i));
		System.out.println("(WSee_i*)*: ");
		System.out.println(cpn_tcws_q.getSucTransitions(wsee_i));
		
		
		//I'm the father of everything
		System.out.println("(wsee_i*)*: " + cpn_tcws_q.getSucTransitions(wsee_i));
		for(Vertice v:cpn_tcws_q.getSucTransitions(wsee_i))  {
			
			int inputNumber=0; //inputNumber is used only for visualization effects
			for(String input:cpn_tcws_q.predecesores(v.getName())) {
				Map<String,String> inputs = new HashMap<String, String>();
				inputs.put(input, input+"_"+wsee_i.getName()+"_"+(inputNumber++));
				System.out.println("Sending I_Q to: " + v.getName() + ". Input: " + inputs);
				queues.get(v.getName()).put(inputs);
			}
		}
		System.out.println("Done sending I_Q values");
		
		executeFinalPhase();
		System.out.println("MASTER: Done executing final phase");
		//Wait for all thread to finish
		/*for(Thread t:engineThreads) {
			t.join();
			
		}*/
		for(Future<EngineThreadResult> f: set) {
			results.add(f.get());
		}
		pool.shutdownNow();
		return results;
	}
	
	private void executeFinalPhase() throws InterruptedException {
		
		//wait for *(*wseef)
		Map<String, String> output = new HashMap<String, String>();
		int outputsSize = query.getOuts().size();
		while(outputsSize != 0) {
			//check the WSee_f queue
			Map<String,String> outputReceived = queues.get("WSee_f").take();
			if(outputReceived.get("compensate") == null) {
				List<String> l =new ArrayList<String>(((Map<String,String>)outputReceived).keySet());
				System.out.println("Received output master final phase: " + l.get(0));
				//check if the message received is actually one of the required output,
				//and if it is a requiered output, check if it was not received and saved before.
				if(query.getOuts().contains(cpn_tcws_q.getPlace(l.get(0))) && !output.containsKey(l.get(0))) {
					System.out.println("Query answer: " + outputReceived);
					output.putAll((Map<String,String>)outputReceived);
					outputsSize--;
				}
			} else {
				//the "compensate" message was received
				executeCompensation();
				System.out.println("Done compensation");
				
				return;
			}
		}
		
		EUtil.printResults(output);
		
		//Send finish message to *(*wsee_f) 
		
		System.out.println("Sending finish message to *(*wsee_f) " + cpn_tcws_q.getPredTransitions(wsee_f));
		for(Vertice v: cpn_tcws_q.getPredTransitions(wsee_f))  {
			Map<String,String> inputs = new HashMap<String, String>();
			inputs.put("finish", "finish");
			queues.get(v.getName()).put(inputs);
		}
		
		//wait for finish message of (wsee_i*)*
		for(Vertice v: cpn_tcws_q.getSucTransitions(wsee_i))  {
			Map<String,String> outputReceived = queues.get("WSee_f").take();
			System.out.println("Execution Engine received end of final phase: " + outputReceived);
		}
		System.out.println("Execution Engine end of final phase");
	}
	
	private void executeCompensation() throws InterruptedException {
		
		//send compensate to all Engine Threads
		for(Vertice v:cpn_tcws_q.getTransitions()) {
			if(!v.getName().equals("WSee_f") && !v.getName().equals("WSee_i")) {
				Map<String,String> inputs = new HashMap<String, String>();
				inputs.put("compensate", "compensate");
				queues.get(v.getName()).put(inputs);
			}
		}
		
		//emptying the WSee_f
		queues.get("WSee_f").clear();
		
		System.out.println("Master setting the barrier");
		EngineThreadThread.reachedCompensation++;
		while(EngineThreadThread.reachedCompensation != cpn_tcws_q.getTransitions().size() - 1);
		System.out.println("All transititions received compensate");
		
		//send control values to *(*wsee_f')
		for(Vertice v:cpn_tcws_q.getPredTransitions(wsee_f))  {
			
			int inputNumber=0;
			for(String input:br_cpn_tcws_q.predecesores(cpn_tcws_q.getCompensationTransition(v.getName()))) {
				
				if(cpn_tcws_q.predecesores(wsee_f.getName()).contains(input)) {
					Map<String,String> inputs = new HashMap<String, String>();
					inputs.put(input, input+"_"+wsee_i.getName()+"_"+(inputNumber++));
					System.out.println("Sending control values to *(*wsee_f'): " + v.getName() + " - " + inputs);
					queues.get(v.getName()).put(inputs);
				}
			}
			
		}
		System.out.println("Done sending I_Q compensation values");
		
		//wait for control values from (wsee_i'*)*
		Map<String, String> output = new HashMap<String, String>();
		int outputsSize = query.getIns().size();
		//debuggin purposes
		/*List<String> remains = new ArrayList<String>();
		for(Vertice i:query.getIns())
			remains.add(i.getName());*/
		while(outputsSize != 0) {
			
			
			
			
			//System.out.println("EE esperando por control value. Cola tamano: " +  queues.get("WSee_f").size() + " , remains: " 
					//+  outputsSize + ", of " + query.getIns().size() + ". i: " +  remains);
			Map<String,String> outputReceived = queues.get("WSee_f").take();

				
			for(Vertice i:query.getIns()) {
				System.out.println("Compensation output received: " + outputReceived);
				if((outputReceived).containsKey(i.getName()) && !output.containsKey(i.getName())) {
					System.out.println("Compensation query answer: " + outputReceived);
					output.putAll(outputReceived);
					outputsSize--;
					
					//debugging
					//remains.remove(i.getName());
				} else {
					//System.out.println("Lost message: " + outputReceived);
				}
			}

		}
		System.out.println("I received all compensation values I wanted: " + output);
	}
	
}
