package ve.usb.serialization;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ve.usb.PetriNets.Arco;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;

public class XMLPNSerializer  {

	private static final String TAG_NAME = "NAME";
	private static final String TAG_DATO = "Dato";
	private static final String TAG_TYPE = "TYPE";
	private static final String TAG_URI = "URI";
	private static final String TAG_TP = "TP";
	private static final String TAG_PROCESO = "Proceso";
	private static final String TAG_ARCO = "Arco";
	private static final String TAG_INI = "INI";
	private static final String TAG_FIN = "FIN";

	public MarkedPN XMLtoPN(String fileName) {
		MarkedPN mpn = null;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(fileName);
			doc.getDocumentElement().normalize();
			Element ele = doc.getDocumentElement();
			
			String name = ele.getAttribute(TAG_NAME);
			mpn = new MarkedPN(name);
			//Places
			
			NodeList placesList = doc.getElementsByTagName(TAG_DATO);
			
			for (int i = 0; i < placesList.getLength(); i++) {
				 
			   Node nNode = placesList.item(i);

			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
			      URI type = new URI(eElement.getAttribute(TAG_TYPE));
			      
			      Vertice vertice = new Vertice(type.getFragment(),Vertice.PLACE);
			      vertice.setUri(type);
                  mpn.addP(vertice);
			   }
			   
			}
			
			//Transitions
			
			NodeList transitionsList = doc.getElementsByTagName(TAG_PROCESO);
			
			for (int i = 0; i < transitionsList.getLength(); i++) {
				 
			   Node nNode = transitionsList.item(i);

			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
			      URI uri = new URI(eElement.getAttribute(TAG_URI));
			      int tp = new Integer(eElement.getAttribute(TAG_TP));
			      
			      
			      double qos[] = {0};
				  double qos_real[] = {0};
			      Vertice vertice = new Vertice(uri.getFragment(), Vertice.TRANSITION, tp,qos,qos_real, uri);
                  mpn.addT(vertice);
			   }
			   
			}
			
			
			//Transitions
			
			NodeList arcsList = doc.getElementsByTagName(TAG_ARCO);
			
			for (int i = 0; i < arcsList.getLength(); i++) {
				 
			   Node nNode = arcsList.item(i);

			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
			      URI ini = new URI(eElement.getAttribute(TAG_INI));
			      URI fin = new URI(eElement.getAttribute(TAG_FIN));
			      
			      Arco a = new Arco(ini.getFragment(), fin.getFragment());
                  mpn.add(a);
			   }
			   
			}
			
		
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return mpn;
	}
	
	public static void main(String...args) {
		 MarkedPN mpn = (new XMLPNSerializer()).XMLtoPN("Q-Transac-Manual-R1-SF20_4_4-NP-02-IND-02.xml");
		 
		 System.out.println(mpn);
	 }
	
}
