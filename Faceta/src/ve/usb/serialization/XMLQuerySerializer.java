package ve.usb.serialization;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.Vertice;

public class XMLQuerySerializer {
	
	private static final String TAG_NAME = "NAME";
	
	private static final String TAG_INPUT = "Input";
	private static final String TAG_TYPE = "TYPE";
	private static final String TAG_VALUE = "Value";
	
	private static final String TAG_OUTPUT = "Output";
	
	private static final String TAG_RISK = "risk";
	
	private static final String TAG_CRITERIA = "criteria"; 
	private static final String TAG_WEIGHT= "qosweight";
	
	public Query XMLtoPN(String queryFile) {
		Query query = null;
		
		List<Vertice> ins = new ArrayList<Vertice>();
		List<Vertice> outs = new ArrayList<Vertice>();
		
		try {
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(queryFile);
			doc.getDocumentElement().normalize();
			Element ele = doc.getDocumentElement();
			
			String name = ele.getAttribute(TAG_NAME);
			
			//Inputs
			Map<Vertice, Object> inputValues = new HashMap<Vertice, Object>();
			NodeList inputList = doc.getElementsByTagName(TAG_INPUT);
			
			for (int i = 0; i < inputList.getLength(); i++) {
				 
				   Node nNode = inputList.item(i);

				   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					  Element eElement = (Element) nNode;
				      URI type = new URI(eElement.getAttribute(TAG_TYPE));

				      
				      Vertice vertice = new Vertice(type.getFragment(),Vertice.PLACE);
				      vertice.setUri(type);
	                  ins.add(vertice);
				      
				      Element value = (Element)eElement.getElementsByTagName(TAG_VALUE).item(0);
				      
				      inputValues.put(vertice, value.getAttribute("Val"));
				      
				   }
				   
				   
				}
			
			
			
			//Outputs
			System.out.println("*** OUTPUTS ***");
			NodeList outputList = doc.getElementsByTagName(TAG_OUTPUT);
			
			for (int i = 0; i < outputList.getLength(); i++) {
				 
			   Node nNode = outputList.item(i);

			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
			      URI type = new URI(eElement.getAttribute(TAG_TYPE));
			      System.out.println("Type : " +  type.getFragment());
			      
			      Vertice vertice = new Vertice(type.getFragment(),Vertice.PLACE);
			      vertice.setUri(type);
                  outs.add(vertice);
			   }
			   
			   
			}
			
			//Risk
	
			Element risk = (Element) doc.getElementsByTagName(TAG_RISK).item(0);
			int riskValue = 1;
			if(risk != null)
				riskValue = new Integer(risk.getAttribute(TAG_VALUE)); 

			
			
			//Outputs

			NodeList qosList = doc.getElementsByTagName(TAG_CRITERIA);
			
			List<Double> weights = new ArrayList<Double>();
			
			for (int i = 0; i < qosList.getLength(); i++) {
				 
			   Node nNode = qosList.item(i);

			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
	 

			      weights.add(new Double(eElement.getAttribute(TAG_WEIGHT)));
			   }
			   
			   
			   
			}
			
			double[] tempArray = new double[weights.size()];
			int i = 0;
			for(Double d : weights) {
			  tempArray[i] = (double) d;
			  i++;
			}

			
			query = new ve.usb.Composer.Query(-1, -1, ins, outs, riskValue, tempArray);
			query.setName(name);
			query.setInputValues(inputValues);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return query;
	}
	 
	 public static void main(String...args) {
		 Query q = (new XMLQuerySerializer()).XMLtoPN("testingRead.xml");
		 
		 System.out.println(q);
	 }
	
}
