package ve.usb.test.data;

import java.util.ArrayList;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.PetriNet;
import ve.usb.PetriNets.Vertice;

public class ExamplePetriNets {
	
	public static PetriNet getPNSimple() {
		
		PetriNet mipn = new PetriNet("My PN");
		double qos[] = {0};
		double qos_r[] = {1};
		// S1 
		Vertice s= new Vertice("S1",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		//Vertice s= new Vertice("S1",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		ArrayList<Vertice> in = new ArrayList<Vertice>();
		ArrayList<Vertice> ou = new ArrayList<Vertice>();
		//Inputs
		Vertice v = new Vertice("AuthorCode",Vertice.PLACE);
		in.add(v);
		v = new Vertice("Inst",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("PubCod",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S2 
			s= new Vertice("S2",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		//s= new Vertice("S2",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("AuthorName",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("PubCod",Vertice.PLACE);
		ou.add(v);
	
		//Agregar
		mipn.addWebService(s,in,ou);
		// S3 
		s= new Vertice("S3",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		//s= new Vertice("S3",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("PubCod",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("Title",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S4 
		s= new Vertice("S4",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		//s= new Vertice("S4",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("PubCod",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("ConfCod",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S5 
		s= new Vertice("S5",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		//s= new Vertice("S5",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("PubCod",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("ConfCod",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("ConfName",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S6 
		s= new Vertice("S6",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S6",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("ConfCod",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("ConfName",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("ConfDate",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S7 
		s= new Vertice("S7",Vertice.TRANSITION, Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S7",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("Inst",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("AuthorCode",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S8 
		s= new Vertice("S8",Vertice.TRANSITION,Vertice.PIVOTRET,qos,qos_r);	
		//s= new Vertice("S8",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("ConfCod",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("ConfPlace",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S9
		s= new Vertice("S9",Vertice.TRANSITION, Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S9",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("AuthorCode",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("ConfCod",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		return mipn;
	}
	
	public static Query getQuerySimple() {
		ArrayList<Vertice> inp = new ArrayList<Vertice>();
		Vertice v = new Vertice("Inst",Vertice.PLACE);
		inp.add(v);
		
		ArrayList<Vertice> out = new ArrayList<Vertice>();
		v = new Vertice("ConfName",Vertice.PLACE);
		//out.add(v);
		v = new Vertice("ConfDate",Vertice.PLACE);
		out.add(v);
		//v = new Vertice("ConfPlace",Vertice.PLACE);
		//out.add(v);
		//v = new Vertice("Title",Vertice.PLACE);
		//out.add(v);
		double weights[] = {1};
		Query myq = new Query(0,0,inp,out,1,weights);
		return myq;
	}
	
	public static PetriNet getPNSimpleFT() {
		
		PetriNet mipn = new PetriNet("My PN");
		double qos[] = {0};
		double qos_r[] = {1};
		// S1 
		Vertice s= new Vertice("WS1",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		ArrayList<Vertice> in = new ArrayList<Vertice>();
		ArrayList<Vertice> ou = new ArrayList<Vertice>();
		//Inputs
		Vertice v = new Vertice("a1",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a2",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a4",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a5",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S2 	
		s= new Vertice("WS2",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a3",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a5",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a6",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S3 
		s= new Vertice("WS3",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a4",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a7",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S4 
		//s= new Vertice("WS4",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		s= new Vertice("WS4",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a5",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a7",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a8",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S5 
		s= new Vertice("WS5",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);		
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a6",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a9",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S6 
		//s= new Vertice("WS6",Vertice.TRANSITION,Vertice.PIVOTRET ,qos,qos_r);	
		s= new Vertice("WS6",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a7",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a10",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S7 
		s= new Vertice("WS7",Vertice.TRANSITION, Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S7",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a8",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a11",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S8 
		//s= new Vertice("WS8",Vertice.TRANSITION,Vertice.PIVOTRET,qos,qos_r);	
		s= new Vertice("WS8",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a8",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a9",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a12",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		return mipn;
	}
	
	public static Query getQuerySimpleFT() {
		//inputs
		ArrayList<Vertice> inp = new ArrayList<Vertice>();
		Vertice v = new Vertice("a1",Vertice.PLACE);
		inp.add(v);
		v = new Vertice("a2",Vertice.PLACE);
		inp.add(v);
		v = new Vertice("a3",Vertice.PLACE);
		inp.add(v);
		//outputs
		ArrayList<Vertice> out = new ArrayList<Vertice>();
		v = new Vertice("a10",Vertice.PLACE);
		out.add(v);
		v = new Vertice("a11",Vertice.PLACE);
		out.add(v);
		v = new Vertice("a12",Vertice.PLACE);
		out.add(v);
		double weights[] = {1};
		Query myq = new Query(0,0,inp,out,1,weights);
		return myq;
	}
	
	public static PetriNet getSubstitutionPN() {
		
		PetriNet mipn = new PetriNet("SubstitutionExample");
		double qos[] = {0};
		double qos_r[] = {1};
		// S1 
		Vertice s= new Vertice("WS1",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		ArrayList<Vertice> in = new ArrayList<Vertice>();
		ArrayList<Vertice> ou = new ArrayList<Vertice>();
		//Inputs
		Vertice v = new Vertice("a1",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a2",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a3",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a4",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S2 	
		s= new Vertice("WS2",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a1",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a3",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a4",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a5",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S3 
		s= new Vertice("WS3",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a1",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a2",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a3",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a4",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S4 
		//s= new Vertice("WS4",Vertice.TRANSITION,Vertice.PIVOT,qos,qos_r);	
		s= new Vertice("WS4",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a3",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a6",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a7",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S5 
		s= new Vertice("WS5",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);		
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a4",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a7",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a8",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S6 
		//s= new Vertice("WS6",Vertice.TRANSITION,Vertice.PIVOTRET ,qos,qos_r);	
		s= new Vertice("WS6",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a5",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a9",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a10",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S7 
		s= new Vertice("WS7",Vertice.TRANSITION, Vertice.COMPENSARET,qos,qos_r);	
		//s= new Vertice("S7",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a6",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a11",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		// S8 
		//s= new Vertice("WS8",Vertice.TRANSITION,Vertice.PIVOTRET,qos,qos_r);	
		s= new Vertice("WS8",Vertice.TRANSITION,Vertice.COMPENSA,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a7",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a11",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a12",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		
		// S9
		//s= new Vertice("WS8",Vertice.TRANSITION,Vertice.PIVOTRET,qos,qos_r);	
		s= new Vertice("WS9",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a10",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a12",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a13",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		
		// S10
		//s= new Vertice("WS8",Vertice.TRANSITION,Vertice.PIVOTRET,qos,qos_r);	
		s= new Vertice("WS10",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a11",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a14",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a15",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		
		// S11
		//s= new Vertice("WS8",Vertice.TRANSITION,Vertice.PIVOTRET,qos,qos_r);	
		s= new Vertice("WS11",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a11",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a12",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a14",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a16",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		
		// S12
		//s= new Vertice("WS8",Vertice.TRANSITION,Vertice.PIVOTRET,qos,qos_r);	
		s= new Vertice("WS12",Vertice.TRANSITION,Vertice.COMPENSARET,qos,qos_r);	
		in = new ArrayList<Vertice>();
		ou = new ArrayList<Vertice>();
		//Inputs
		v = new Vertice("a12",Vertice.PLACE);
		in.add(v);
		v = new Vertice("a13",Vertice.PLACE);
		in.add(v);
		//Outputs
		v = new Vertice("a17",Vertice.PLACE);
		ou.add(v);
		v = new Vertice("a18",Vertice.PLACE);
		ou.add(v);
		//Agregar
		mipn.addWebService(s,in,ou);
		
		return mipn;
	}

	
	public static Query getSubstitutionQuery() {
		//inputs
		ArrayList<Vertice> inp = new ArrayList<Vertice>();
		Vertice v = new Vertice("a1",Vertice.PLACE);
		inp.add(v);
		v = new Vertice("a2",Vertice.PLACE);
		inp.add(v);
		//outputs
		ArrayList<Vertice> out = new ArrayList<Vertice>();
		v = new Vertice("a14",Vertice.PLACE);
		out.add(v);
		v = new Vertice("a15",Vertice.PLACE);
		out.add(v);
		v = new Vertice("a16",Vertice.PLACE);
		out.add(v);
		double weights[] = {1};
		Query myq = new Query(0,0,inp,out,1,weights);
		return myq;
	}
	
}
