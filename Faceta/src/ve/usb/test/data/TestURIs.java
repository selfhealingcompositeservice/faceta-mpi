package ve.usb.test.data;

import java.net.URI;

/**
 * Utility interface that contains often used URIs of exemplary OWL
 * ontologies as well as OWL-S services.
 *
 * @author Rafael Angarita
 */
public interface TestURIs
{
	/* ---------------------------------------------------------------- */
	/* OWL-S Services                                                   */
	/* ---------------------------------------------------------------- */
	public static final URI PAPER = URI.create("http://localhost:8080/mindswap/ontology/small/test/");
	
	public static final URI BA100_3_2 = URI.create("http://localhost:8080/mindswap/ontology/small/BA100_3_2/");
	
	public static final URI SF100_2_1 = URI.create("http://localhost:8080/mindswap/ontology/exp/SF100_2_1/");
	
	public static final URI SF100_2_1_LDC = URI.create("http://www.ldc.usb.ve/~rangarita/ontology/SF100_2_1/");
	
	public static final URI BA100_3_2_LDC = URI.create("http://www.ldc.usb.ve/~rangarita/Ontologies/Graphs/Stress/Small/BA100_3_2/");
	
	public static final URI SERVICES = URI.create("MyServices.owl");
	
	public static final URI QUERIES = URI.create("MyQueries.owl");
	
	public static final URI CONCEPTS = URI.create("MyConcepts.owl");

	public static final URI STATISTICS = URI.create("MyStatis.qos");

}
