package ve.usb.ui.jung;
/*
 * SimpleGraphView.java
 *
 * Created on March 8, 2007, 7:49 PM
 *
 * Copyright March 8, 2007 Grotto Networking
 */


import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import org.apache.commons.collections15.Transformer;

import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;


class MyNode {
	int id; 
	private String name;
	private int status = 0;
	public MyNode(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public String toString() { // Always a good idea for debuging
		return "V"+id;
	// JUNG2 makes good use of these.
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getStatus() {
		
		return this.status;
	}
	
	public String getName() {
		return this.name;
	}
}

class MyLink {
	public String toString() { // Always a good idea for debuging
		return "";
	// JUNG2 makes good use of these.
	}
}

class SimpleGraph {
	
	Graph<MyNode, MyLink> g;
	public SimpleGraph(MarkedPN pn) {
    	
    	Vertice wsee_i, wsee_f;
    	
    	//WSee_i
		wsee_i = new Vertice("WSee_i", Vertice.TRANSITION);
		pn.addWebService(wsee_i, new ArrayList<Vertice>(), pn.getInputs());
		
		//WSee_f
		wsee_f = new Vertice("WSee_f", Vertice.TRANSITION);
		pn.addWebService(wsee_f, pn.getOutputs(), new ArrayList<Vertice>());
		
        // Graph<V, E> where V is the type of the vertices and E is the type of the edges
        // Note showing the use of a SparseGraph rather than a SparseMultigraph
        g = new SparseGraph<MyNode, MyLink>();
        // Add some vertices. From above we defined these to be type Integer.

//        n1 = new MyNode(1, "primero");
//        n2 = new MyNode(1, "segundo");
//        n3 = new MyNode(1, "tercero");
//        
//        // g.addVertex((Integer)1);  // note if you add the same object again nothing changes
//        // Add some edges. From above we defined these to be of type String
//        // Note that the default is for undirected edges.
//        g.addEdge(new MyLink(),n1, n2, EdgeType.DIRECTED); // This method
//        g.addEdge(new MyLink(),n1, n3, EdgeType.DIRECTED); // This method
//        g.addEdge(new MyLink(),n3, n2, EdgeType.DIRECTED); // This method
        
        traverseGraph(wsee_i, pn);
  
    }
    
	Map<String, MyNode> nodes = new HashMap<String, MyNode>();
	
    private void traverseGraph(Vertice ini, MarkedPN pn) {
    	
    	if(nodes.get(ini.getName()) == null)
    		nodes.put(ini.getName(), new MyNode(0, ini.getName()));
    	
    	for(Vertice v:pn.getSucTransitions(ini)) {
    		System.out.println(ini.getName() + " --> " +  v.getName());
    		
    		if(nodes.get(v.getName()) == null)
    			nodes.put(v.getName(), new MyNode(0, v.getName()));
    		
        	addEdge(new MyLink(), ini, v);
        	traverseGraph(v, pn);
        }
    }
    
    private void addEdge(MyLink link, Vertice ini, Vertice end) {
    	g.addEdge(new MyLink(), nodes.get(ini.getName()), nodes.get(end.getName()), EdgeType.DIRECTED);
    }
}

/**
 *
 * @author Dr. Greg M. Bernstein
 */
public class SimpleGraphView2 {

    /** Creates a new instance of SimpleGraphView 
     * @param pn */
    
    
    /**
     * @param args the command line arguments
     */
    /**
     * @param args
     */
    /**
     * @param args
     */
    public  void run(MarkedPN pn) {
    	
    	
    			
    	//Build the graph
        SimpleGraph sgv = new SimpleGraph(pn); // This builds the graph
        // Layout<V, E>, VisualizationComponent<pnV,E>
        Layout<MyNode, MyLink> layout = new CircleLayout(sgv.g);
        layout.setSize(new Dimension(650,650));
        BasicVisualizationServer<MyNode,MyLink> vv = new BasicVisualizationServer<MyNode,MyLink>(layout);
        vv.setPreferredSize(new Dimension(700,700));       
        // Setup up a new vertex to paint transformer...
        Transformer<MyNode,Paint> vertexPaint = new Transformer<MyNode,Paint>() {
            public Paint transform(MyNode i) {
            	if(i.getStatus() == 1)
            		return Color.RED;
            	else if (i.getName().compareTo("WSee_i") == 0 || i.getName().compareTo("WSee_f") == 0)
            		return Color.MAGENTA;
            	else
            		return Color.GREEN;
            }
            
        };  
        // Set up a new stroke Transformer for the edges
        float dash[] = {10.0f};
        final Stroke edgeStroke = new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
             BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
        Transformer<MyLink, Stroke> edgeStrokeTransformer = new Transformer<MyLink, Stroke>() {
            public Stroke transform(MyLink s) {
                return edgeStroke;
            }
        };
        vv.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
        vv.getRenderContext().setEdgeStrokeTransformer(edgeStrokeTransformer);
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
        vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);        
        
        
        JFrame frame = new JFrame("Simple Graph View 2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(vv);
        frame.pack();
        frame.setVisible(true);     
        
        try {
            Thread.sleep(5000);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        
        /*vv.getRenderContext().setVertexFillPaintTransformer(new Transformer<Integer,Paint>() {
            public Paint transform(Integer i) {
            	System.out.println(i);
                return Color.BLUE;
            }
        }  );*/
        
        
        //vv.getRenderContext().getVertexFillPaintTransformer().transform(arg0)

        vv.updateUI();
        
      

    }
    
}