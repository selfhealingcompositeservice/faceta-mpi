package ve.usb.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.mindswap.owl.OWLIndividualList;
import org.mindswap.owls.process.variable.Input;
import org.mindswap.owls.process.variable.Output;
import org.mindswap.owls.profile.ServiceParameter;
import org.mindswap.owls.service.Service;

import ve.usb.Composer.Query;
import ve.usb.KnowledgedeBase.KnowledegeBase;
import ve.usb.KnowledgedeBase.ProcessStatistic;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.PetriNet;
import ve.usb.PetriNets.Vertice;
import ve.usb.config.RunningProperties;

public class LoadUtils {
	
	private RunningProperties properties;
	private KnowledegeBase mykb;
	private String petriNetURLName;
	
	public LoadUtils(KnowledegeBase mykb, RunningProperties properties, URI uri) {
		this.properties = properties;
		this.mykb = mykb;
		
		//fixing the URI if it does not have a forward slash as a last character
		String url =uri.toString();
		
		if(url.charAt(url.length()-1) != '/') {
			try {
				uri = new URI(url+'/');
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		
		petriNetURLName = uri.toString().replaceFirst(".*/([^/?]+).*", "$1");
	}
	

	public PetriNet loadPetriNet() {
		PetriNet pn = null;
		//load PetriNet
		if(properties.getLoadPetriNetFromFile())
			pn = loadPetriNetFromFile(petriNetURLName);
		else {
			pn = loadPetriNet(mykb);
		}
		return pn;
	}
	
	public static PetriNet loadPetriNetFromFile(String name) {
    	
    	PetriNet petriNet = null;
    	FileInputStream fis = null;
    	ObjectInputStream in = null;
        try {
	        fis = new FileInputStream("output/"+name);
	        in = new ObjectInputStream(fis);
	        petriNet = (PetriNet)in.readObject();
	        in.close();
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    	
        return petriNet;
    
	}
	
	public static MarkedPN loadMarkedPetriNetFromFile(String name) {
    	
		MarkedPN petriNet = null;
    	FileInputStream fis = null;
    	ObjectInputStream in = null;
        try {
	        fis = new FileInputStream(name);
	        in = new ObjectInputStream(fis);
	        petriNet = (MarkedPN)in.readObject();
	        in.close();
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    	
        return petriNet;
    }
	
	@SuppressWarnings("unchecked")
	public static List<MarkedPN> loadMarkedPetriNetListFromFile(String name) {
    	
		List<MarkedPN> petriNet = null;
    	FileInputStream fis = null;
    	ObjectInputStream in = null;
        try {
	        fis = new FileInputStream(name);
	        in = new ObjectInputStream(fis);
	        petriNet = (List<MarkedPN>)in.readObject();
	        in.close();
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    	
        return petriNet;
    }
	
	public static Query loadQueryFromFile(String name) {
    	
		Query petriNet = null;
    	FileInputStream fis = null;
    	ObjectInputStream in = null;
        try {
	        fis = new FileInputStream(name);
	        in = new ObjectInputStream(fis);
	        petriNet = (Query)in.readObject();
	        in.close();
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    	
        return petriNet;
    }
	
	
	
    private PetriNet loadPetriNet(KnowledegeBase mykb) {
    	PetriNet petriNet = new PetriNet(petriNetURLName);

		java.util.Hashtable<String, ProcessStatistic> statisticsList = mykb.getProcessesStatistics();
		System.out.println("Number of services: " + mykb.getServices().size());
		int i_s = 0;
		for(Service s: mykb.getServices()) {
			//if(s.getName().contains("REPLICA-2") || s.getName().contains("REPLICA-7")) { //PN SIN REPLICAS
				System.out.println("-Services number: " + ++i_s);
				final org.mindswap.owls.profile.Profile profile = s.getProfile();
				
				ProcessStatistic statistics = statisticsList.get(profile.getProcess().getURI().toString());
				int transactionalProperty = 0;
				List<Vertice> eq = new ArrayList<Vertice>();
				final OWLIndividualList<ServiceParameter> params = profile.getServiceParameters();
				for (ServiceParameter param : params)
				{
					
					if(param.getName().equals("TransactionalProperty")) {
						transactionalProperty = getTransactionalProperty(param.getParameter().getLocalName());
					} else if(param.getName().equals("Equivalent")) {
						//transactionalProperty = getTransactionalProperty(param.getParameter().getLocalName());
						String eqName = param.getParameter().getLocalName();
						Service eqService = null;
						for(Service seq:mykb.getServices()) {
							if(seq.getName().equals(eqName)) {
								eqService = seq;
								break;
							}
						}
						
						ServiceParameter eqTP = eqService.getProfile().getServiceParameters().getIndividual("TransactionalProperty");
						ProcessStatistic eqStat = statisticsList.get( eqService.getProfile().getProcess().getURI().toString());
						int eqTPint = 0;
						if(eqTP != null)
							eqTPint = getTransactionalProperty(eqTP.getLocalName());
						if(eqTPint == 0)
							eqTPint = eqStat.getTransactionalProperty();
						
						
						double qos[] = {statistics.getExecutionCost()};
						double qos_real[] = {statistics.getExecutionCostReal()};
						
						Vertice eqVertice = new Vertice(eqService.getName(), Vertice.TRANSITION,eqTPint, qos,  qos_real, eqService.getURI());
						eq.add(eqVertice);
						System.out.println(s.getName() + " equivalent: " + param.getParameter().getLocalName());
					}
				}
				
				if(transactionalProperty == 0)
					transactionalProperty = statistics.getTransactionalProperty();
				
				//Creating the Vertice that represents the service
				
				//TODO: revisar bien que va aqui (en qos[])
				
				double qos[] = {statistics.getExecutionCost()};
				double qos_real[] = {statistics.getExecutionCostReal()};
	
				Vertice v = new Vertice(s.getName(), Vertice.TRANSITION, transactionalProperty, qos, qos_real, s.getURI());
				v.setEquivalents(eq);
				ArrayList<Vertice>  inputs = new ArrayList<Vertice>();
	
	            ArrayList<Vertice>  outputs = new ArrayList<Vertice>();
	
				org.mindswap.owls.process.Process p = s.getProcess();
				
				//Inputs
	            List<Input> serviceInputs = p.getInputs();
	
	            for(Input i : serviceInputs) {
	
	                inputs.add(new Vertice(i.getParamType().getURI().getFragment(), Vertice.PLACE));
	            }
	
	            //Outputs
	         	List<Output> serviceOutputs = p.getOutputs();
	
	            for(Output o : serviceOutputs) {
	
	            	outputs.add(new Vertice(o.getParamType().getURI().getFragment(), Vertice.PLACE));
	            }
	            
	            //Adding the new web service to the Petri Net
	
	            petriNet.addWebService(v, inputs, outputs);
			//}//FIN PN SIN REPLICAS

		}
		
		//The creation of the PetriNet has been completed
		savePetriNet(petriNet);
		return petriNet;
    }
    
    private int getTransactionalProperty(String name) {
    	int ret = 0;
    	
    	if(name.equals("compensatable"))
    		ret = Vertice.COMPENSA;
    	else if(name.equals("compensatableRetriable"))
    		ret = Vertice.COMPENSARET;
    	else if(name.equals("pivot"))
    		ret = Vertice.PIVOT;
    	else if(name.equals("pivotRetriable"))
    		ret = Vertice.PIVOTRET;
    	else if(name.equals("atomic"))
    		ret = Vertice.ATOMIC;
    	else if(name.equals("atomicRetriable"))
    		ret = Vertice.ATOMICRET;
    	return ret;
    	
    }
    
    public static void savePetriNet(PetriNet petriNet) {
    	System.out.println("Saving PN... " + petriNet.getName());
    	String fname = "output/" + petriNet.getName();
    	String filenameSer = fname;
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try	{
        	fos = new FileOutputStream(filenameSer);
        	out = new ObjectOutputStream(fos);
        	out.writeObject(petriNet);
        	out.close();
        	System.out.println("Done.");
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    }
    
    public static void saveQueryList(List<Query> list, String name) {
    	System.out.println("Saving Query List... ");
    	String fname = "results/" + name;
    	String filenameSer = fname;
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try	{
        	fos = new FileOutputStream(filenameSer);
        	out = new ObjectOutputStream(fos);
        	out.writeObject(list);
        	out.close();
        	System.out.println("Done.");
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    }
    
    @SuppressWarnings("unchecked")
	public static List<Query> loadQueryList(String name) {

    	List<Query> petriNet = null;
    	FileInputStream fis = null;
    	ObjectInputStream in = null;
        try {
	        fis = new FileInputStream(name);
	        in = new ObjectInputStream(fis);
	        petriNet = (List<Query>)in.readObject();
	        in.close();
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    	
        return petriNet;
    }
    
    public static void saveResult(MarkedPN petriNet) {
    	System.out.println("Saving PN... " +  petriNet.getName());
    	String fname = "results/" +  petriNet.getName();
    	String filenameSer = fname;
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try	{
        	fos = new FileOutputStream(filenameSer);
        	out = new ObjectOutputStream(fos);
        	out.writeObject(petriNet);
        	out.close();
        	System.out.println("Done.");
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
        //save result size
        fname = fname + "_size";
        int i = petriNet.getTransitions().size();
        try {
			PrintWriter pw = new PrintWriter(new FileWriter(fname));
			pw.write(new Integer(i).toString());
			pw.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
    }
    
    public static void saveMarkedPetriNetList(List<MarkedPN> petriNet, String name, int tam) {
    	System.out.println("Saving PN List... " + name);
    	String fname = "results/" +  name + "_list_" + tam;
    	String filenameSer = fname;
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try	{
        	fos = new FileOutputStream(filenameSer);
        	out = new ObjectOutputStream(fos);
        	out.writeObject(petriNet);
        	out.close();
        	System.out.println("Done.");
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
 
    }
    
    public static void saveQuery(Query query) {
    	System.out.println("Saving Query... " +  query.getName());
    	String fname = "results/" +  query.getName();
    	String filenameSer = fname;
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try	{
        	fos = new FileOutputStream(filenameSer);
        	out = new ObjectOutputStream(fos);
        	out.writeObject(query);
        	out.close();
        	System.out.println("Done.");
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    }
    
	public static List<String>  loadQueryResultList(String fileName) {
		List<String> ret = new ArrayList<String>();
		
		
		 try{
			  // Open the file that is the first 
			  // command line parameter
			  FileInputStream fstream = new FileInputStream(fileName);
			  // Get the object of DataInputStream
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  String strLine;
			  //Read File Line By Line
			  while ((strLine = br.readLine()) != null)   {
				  ret.add(strLine);
			  }
			  //Close the input stream
			  in.close();
		    }catch (Exception e){//Catch exception if any
		    	System.err.println("Error: " + e.getMessage());
		  }
		return ret;
	}
	
	/**
	 * Save the result of all executed queries
	 * @param queries
	 */
   
	public static void saveQueries(List<Query> queries) {
		
    	String fname = "output/" + "queries.faceta";
    	String filenameSer = fname;
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try	{
        	fos = new FileOutputStream(filenameSer);
        	out = new ObjectOutputStream(fos);
        	out.writeObject(queries);
        	out.close();
        	System.out.println("Done.");
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
	}
	
	
}
