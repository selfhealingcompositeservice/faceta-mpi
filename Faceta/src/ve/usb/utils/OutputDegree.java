package ve.usb.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ve.usb.Composer.Query;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;

public class OutputDegree {

	private MarkedPN mpn;
	private Map<String, List<String>> dependencies;
	private List<Vertice> outputsList;
	
	public OutputDegree(MarkedPN mpn, List<Vertice> outputsList) {
		this.mpn = mpn;
		this.outputsList = outputsList;
		dependencies = new HashMap<String, List<String>>();
		
		traverse();
	}
	
	private void traverse() {
		
		System.out.println(outputsList);
		for(Vertice output:outputsList) {
			System.out.println(mpn.predecesores(output));
			for(Vertice t:mpn.predecesores(output)) {
			
				traverseTransitions(t, output);
			}
		}
		System.out.println(dependencies);
	}
	
	private void traverseTransitions(Vertice t, Vertice output) {

			if(t.getType() == Vertice.TRANSITION) {
				if(dependencies.get(t.getName()) != null) {
					dependencies.get(t.getName()).add(output.getName());
				} else {
					List<String> dep = new ArrayList<String>();
					dep.add(output.getName());
					dependencies.put(t.getName(), dep);
				}
			}
			for(Vertice v:mpn.predecesores(t))
				traverseTransitions(v, output);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		List<Query> queryResultList = LoadUtils.loadQueryList("output/queries.faceta");
		System.out.println("Query List: " + queryResultList.size());
		
		for(Query query:queryResultList) {
			if(query.getName().equals("Q-Transac-Escenario2-BA800_3_8-NP-10-IND-09")){
				MarkedPN mp = query.getResults().getWSDN();

				System.out.println(mp);
				
				OutputDegree od = new OutputDegree(mp, query.getOuts());
				System.out.println("********************************************");
				System.out.println("Total = " + query.getOuts().size());
				for(String s:od.getDependencies().keySet()) {
					System.out.println(s + " = " + od.getDependencies().get(s).size() + ", eet=" + mp.getTransition(s).getQoSReal()[0]);
					
				}
				System.out.println("********************************************");
				
				for(Vertice v:mp.getTransitions()) {
					
					boolean outputFinal = false;
					int outputs = 0;
					
					for(Vertice o:query.getOuts())
						if(mp.sucesores(v.getName()).contains(o.getName())) {
							outputFinal=true;
							outputs++;
						}

					System.out.println("outputFinal=" + outputFinal + " for " + v.getName() + ", nro=" + outputs);
					if(mp.getPredTransitions(v).size() == 0)
						System.out.println("input: " + v.getName());
				
					if(mp.getSucTransitions(v).size() == 0) {
						System.out.println("output: " + v.getName() + ", pred: " + mp.getPredTransitions(v));
					}
					
					
				}
			}
		}

		
		
	}

	public Map<String, List<String>> getDependencies() {
		return dependencies;
	}


}
