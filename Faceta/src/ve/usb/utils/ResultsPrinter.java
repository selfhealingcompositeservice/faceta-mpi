package ve.usb.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import ve.usb.Composer.Query;
import ve.usb.Main.ExecutorDefaultEdge;
import ve.usb.PetriNets.MarkedPN;
import ve.usb.PetriNets.Vertice;
import ve.usb.executor.EngineThreadResult;
import ve.usb.executor.FTMechanism;
import ve.usb.executor.FTMechanism.Mechanism;

/**
 * Utility class to print results
 *
 * @author Rafael Angarita
 * 
 */
public class ResultsPrinter {
	
	public void printExecutionResults(Query query, double delta, double executionTime, double estimatedExecutionTime, 
			double elapsedTimeNodes, String faultyWs, String mechanism, boolean previousMechanism, 
			int forwardRecovery, int compensated, int replication , double replicationNecessity
			, int failedReplication, int checkpointingNumber, int successfullyExecuted, int failedNoForwardRecovery
			, double generatedOutput, double earliestFailureTime, double outputsDependencyFailedWS
			, double estimatedExecutionTimeLeft) {
		
		int widSize = 5;
		int widTim = 10;
		int widRime = 4;
		int widRN = 6;
		boolean appendTitle = true;
		
		String line = "";
		File f = new File("output/executor_out_" + query.getName());
		
		appendTitle = !f.exists();
		
		FileWriter pw;
		try {
			pw = new FileWriter(f, true);
			
			line = "\n" 
					//+ widthen("Model", widMod)
					+ widthen("Size", widSize)
					+ widthen("D", widSize)
		            //+ widthen("ETime  ", widTim)
		            + widthen("EETime", widTim)
		            + widthen("RETime", widTim)
		            //+ widthen("FWS  ", widTim)
		            + widthen("FR", widRime)
		            + widthen("CS", widRime) //number of compensated ws
		            + widthen("RE", widRime)
		            + widthen("RN", widRN)
		            + widthen("RF", widRime) //failed replication
		            + widthen("CP", widRime) //number of checkpointed ws
		            + widthen("SE", widRime) //number of successfully executed ws
		            + widthen("FE", widRime) //number of failed ws no forward recovered
		            + widthen("GO", widRN) //percentage of generated user outputs
		            + widthen("EFTime", widTim) //failure time of the first failed ws
		            + widthen("OD", widRN) //output dependency of the first failed ws
		            + widthen("LETime", widTim) //estimated execution time left
		        ; 
				
				if(appendTitle)
					pw.append(line);
				
				line = "\n";
				
				int planSize = query.getResults().getWSDN().getNumProcecsos()-2;
				line += widthen(planSize, widSize);
				
				line += widthen(delta, widSize);
				
				line += widthen(estimatedExecutionTime, widTim);
				
				line += widthen(executionTime, widTim);
				
				faultyWs = faultyWs == null ? "None":faultyWs.substring(faultyWs.indexOf("-")-3, faultyWs.indexOf("-"));
				
				mechanism = mechanism.compareTo(Mechanism.FORWARD_RECOVERY.toString()) == 0 ?
						"FR" : mechanism.compareTo(Mechanism.BACKWARD_RECOVERY.toString()) == 0? "BR":
							mechanism.compareTo(Mechanism.REPLICATION.toString()) == 0 ? "RE" : "X";
				
				line += widthen(forwardRecovery, widRime);
				
				line += widthen(compensated, widRime); 
				
				line += widthen(replication, widRime); 
				
				line += widthen(replicationNecessity, widRN);
				
				line += widthen(failedReplication , widRime);
				
				line += widthen(checkpointingNumber , widRime);
				
				line += widthen(successfullyExecuted , widRime);
				
				line += widthen(failedNoForwardRecovery , widRime);
				
				line += widthen(generatedOutput, widRN);
				
				line += widthen(earliestFailureTime, widTim);
				
				line += widthen(outputsDependencyFailedWS, widRN);
				
				line += widthen(estimatedExecutionTimeLeft, widTim);
				
				//fin de linea
				pw.append(line);
				
				pw.close();
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private String widthen(String val, int width) {
        String ret = val;

        while (ret.length() < width) {
            ret = " " + ret;
        }

        return ret;
	}
	    
    private String widthen(double val, int width) {
        return widthen(val + "", width);
    }

    private String widthen(int val, int width) {
        return widthen(val + "", width);
    }
	
	public static Graph<String, DefaultEdge> getGraph(MarkedPN pn) {
		 Graph<String, DefaultEdge> g = new DefaultDirectedGraph<String, DefaultEdge>(ExecutorDefaultEdge.class);

		 for(Vertice v:pn.getTransitions()) {
			 
			 if(!g.containsVertex(getVertexName(v)))
				 g.addVertex(getVertexName(v));
			
			 List<Vertice> p = pn.getPredTransitions(v);
			 
			 for(Vertice pv:p) {
				 if(g.getEdge(getVertexName(pv), getVertexName(v)) == null) {
					 if(!g.containsVertex(getVertexName(pv)))
						 g.addVertex(getVertexName(pv));
				
			
					
					 g.addEdge(getVertexName(pv), getVertexName(v));
				 }
			 }
			 
			 List<Vertice> t = pn.getSucTransitions(v);
			 
			 for(Vertice pt:t) {
				 if(g.getEdge(getVertexName(v), getVertexName(pt)) == null) {
					 
					 if(!g.containsVertex(getVertexName(pt)))
						 g.addVertex(getVertexName(pt));
					 
					 g.addEdge(getVertexName(v), getVertexName(pt));
				 }
			 }
		 }
		 
		
		 
		 return g;
	}
	
	public static String getVertexName(Vertice v) {
		String ret = null;
		
		ret = v.getName();
		
		return ret;
	}
	
	public static String getVertexNameNumber(Vertice v) {
		String ret = null;
		
		if(v.getName().equals("WSee_i") || v.getName().equals("WSee_f"))
			return v.getName();
		
		//patron RF
		//Pattern p = Pattern.compile("^*[-]([0-9]+)[-].*");
		
		//patron BA
		Pattern p = Pattern.compile("^*([0-9]+)[-].*");
		Matcher m = p.matcher( v.getName());
		String name = "";
		if (m.find()) {
		    System.out.println(m.group(1));
		    name = m.group(1);
		}

		
		ret = getTransactionalProperty(v.getTransactionalProperty())+"_"+ name;
		
		
		return ret;
	}
	
	public static String getTransactionalProperty(int tp) {
		String s = "";
		
		switch (tp) {
			case Vertice.PIVOT:
				s = "p";
			break;
			
			case Vertice.ATOMIC:
				s = "a";
				break;
				
			case Vertice.PIVOTRET:
				s = "pr";
				break;
				
			case Vertice.ATOMICRET:
				s = "ar";
				break;
				
			case Vertice.COMPENSA:
				s = "c";
				break;
				
			case Vertice.COMPENSARET:
				s = "cr";
				break;
				
			default:
				break;
		}
		
		return s;
	}

}