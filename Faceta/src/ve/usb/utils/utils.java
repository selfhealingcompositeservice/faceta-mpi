/*
 * utils.java
 *
 * Created on Sat Feb 27 06:13:29 VET 2010
 *
 */

package ve.usb.utils;

import java.util.ArrayList;
import java.util.StringTokenizer;

import ve.usb.PetriNets.Vertice;

/**
 *
 * @author Yudith Cardinale
 */
public class utils {
    
    /** Creates a new instance of utils */
    public utils() {
    }
    
    public static String [] sort(String ret[]){
         String temp;
        try {
        for(int i=0; i < ret.length - 1; i++) {
           for(int j= i + 1; j < ret.length; j++) {
               if (ret[i].compareTo(ret[j]) > 0) {
                   temp = ret[i];
                   ret[i] = ret[j];
                   ret[j] = temp;
               }
           }            
        }
        } catch (Exception e) {    // aqui deberia retornar null
        }
        
         return ret;
    }
    
    public static int [] sort(int ret[]){
         int temp;
    try {    
        for(int i=0; i < ret.length - 1; i++) {
           for(int j= i + 1; j < ret.length; j++) {
               if (ret[i] > ret[j]) {
                   temp = ret[i];
                   ret[i] = ret[j];
                   ret[j] = temp;
               }
           }            
        }
    } catch (Exception e) {}
         return ret;
    }    
           
    public static String[] getClon(String[] s) {
        String[] ret = new String[s.length];
        
        System.arraycopy(s, 0, ret, 0, s.length);        
        
        return ret;

    }

    public static String getTime(long time) {
        long min = 0;
        long seg = 0;
        long mil = 0;
        
        min = time / (1000 * 60);
        time = time - min *(1000 * 60);

        seg = time / 1000;
        time = time - seg * 1000;
        mil = time;
        
        return "Tiempo total: " + min + " m " + seg + " s " + mil + " ms ";

    }
  
  public static Vertice createService(StringTokenizer tokens) {
	String str,name;
	double[] qos = new double[5];
	double[] qos_real = null;
        int transac=0;
	str = tokens.nextToken(); //skip Indice
	name = tokens.nextToken(); //Service name
	for (int j = 0; j< 9;j++)
	    str = tokens.nextToken(); // skip functionality, ins, and outs
	str = tokens.nextToken(); // Transactonal property

	if (str.compareTo("p")==0) transac=Vertice.PIVOT;
	else if  (str.compareTo("pr")==0) transac=Vertice.PIVOTRET;
	else if (str.compareTo("a")==0) transac=Vertice.ATOMIC;
	else if  (str.compareTo("ar")==0) transac=Vertice.ATOMICRET;
	else if (str.compareTo("c")==0) transac=Vertice.COMPENSA;
	else if  (str.compareTo("cr")==0) transac=Vertice.COMPENSARET;

	for (int i=0; i<5 ; i++) {
            str = tokens.nextToken();
	    qos[i] = new Double(str); 	    
        }


 	Vertice s= new Vertice(name,Vertice.TRANSITION,transac,qos, qos_real);

	return s;
    }


  public static Vertice createServiceExhaOneSix(StringTokenizer tokens) {
	String str,name;
	double[] qos = new double[7]; 
	double[] qos_real = null; 
        int transac=0;

	str = tokens.nextToken(); //skip Indice
	name = tokens.nextToken(); //Service name
	for (int j = 0; j< 9;j++)
	    str = tokens.nextToken(); // skip functionality, ins, and outs
	str = tokens.nextToken(); // Transactonal property

	if (str.compareTo("p")==0) transac=Vertice.PIVOT;
	else if  (str.compareTo("pr")==0) transac=Vertice.PIVOTRET;
	else if (str.compareTo("a")==0) transac=Vertice.ATOMIC;
	else if  (str.compareTo("ar")==0) transac=Vertice.ATOMICRET;
	else if (str.compareTo("c")==0) transac=Vertice.COMPENSA;
	else if  (str.compareTo("cr")==0) transac=Vertice.COMPENSARET;

	for (int i=0; i<5 ; i++) {
            str = tokens.nextToken();
	    qos[i] = new Double(str); 	    
        }


 	Vertice s= new Vertice(name,Vertice.TRANSITION,transac,qos, qos_real);

	return s;
    }

  public static Vertice createServiceExha(StringTokenizer tokens) {
	String str,name;
	double[] qos = new double[7]; 
	double[] qos_real = null;
        int transac=0;

	str = tokens.nextToken(); //skip Indice
	name = tokens.nextToken(); //Service name
	for (int j = 0; j< 6;j++)
	    str = tokens.nextToken(); // skip functionality, ins, and outs
	str = tokens.nextToken(); // Transactonal property

	if (str.compareTo("p")==0) transac=Vertice.PIVOT;
	else if  (str.compareTo("pr")==0) transac=Vertice.PIVOTRET;
	else if (str.compareTo("a")==0) transac=Vertice.ATOMIC;
	else if  (str.compareTo("ar")==0) transac=Vertice.ATOMICRET;
	else if (str.compareTo("c")==0) transac=Vertice.COMPENSA;
	else if  (str.compareTo("cr")==0) transac=Vertice.COMPENSARET;

	for (int i=0; i<7 ; i++) {
            str = tokens.nextToken();
	    qos[i] = new Double(str); 	    
        }


 	Vertice s= new Vertice(name,Vertice.TRANSITION,transac,qos, qos_real);

	return s;
    }


   public static ArrayList<Vertice> createAttri(StringTokenizer tokens, int type) {
	int max1,max2;
   	ArrayList<Vertice> ret;
	String str="";
	if (type ==0) {max1 =5; max2=3;}
        else {max1 = 3; max2=8;}
	ret = new ArrayList<Vertice>();
	for (int j = 0; j<max2 ;j++)
	    str = tokens.nextToken(); // skip ind, name, and functionality

	
	for (int i = 0; i< max1; i++) {
    	    str = tokens.nextToken();

	    if (str.compareTo("X")==0) break;
	    else {
	    	Vertice v = new Vertice(str,Vertice.PLACE);
		ret.add(v);
	     }
	}
	return ret; 
  }

   public static ArrayList<Vertice> createAttriExhaOneSix(StringTokenizer tokens, int type) {
	int max1,max2;
   	ArrayList<Vertice> ret;
	String str="";
	if (type ==0) {max1 =5; max2=3;} //input
        else {max1 = 3; max2=8;} //outputs
	ret = new ArrayList<Vertice>();
	for (int j = 0; j<max2 ;j++)
	    str = tokens.nextToken(); // skip ind, name, and functionality and in when i am reading outputs
	
	for (int i = 0; i< max1; i++) {
    	    str = tokens.nextToken();
	    if (str.compareTo("X")==0) break;
	    else {
	    	Vertice v = new Vertice(str,Vertice.PLACE);
		ret.add(v);
	     }
	}
	return ret; 
  }

   public static ArrayList<Vertice> createAttriExha(StringTokenizer tokens, int type) {
	int max1,max2;
   	ArrayList<Vertice> ret;
	String str="";
	if (type ==0) {max1 =2; max2=3;} //input
        else {max1 = 3; max2=5;} //outputs
	ret = new ArrayList<Vertice>();
	for (int j = 0; j<max2 ;j++)
	    str = tokens.nextToken(); // skip ind, name, and functionality and in when i am reading outputs
	
	for (int i = 0; i< max1; i++) {
    	    str = tokens.nextToken();
	    if (str.compareTo("X")==0) break;
	    else {
	    	Vertice v = new Vertice(str,Vertice.PLACE);
		ret.add(v);
	     }
	}
	return ret; 
  }

  public static ArrayList<Vertice> createAttriQ(StringTokenizer tokens, int size, int type) {
	int max1,max2;
   	ArrayList<Vertice> ret;
	String str="";
	if (size ==0) {max1 =3;} //small
        else if (size == 1){max1 = 6;} //medium
	else {max1 = 10;} // large

	if (type ==0) {max2=3;} //inpouts
        else {max2=3+max1;} //outputs

	ret = new ArrayList<Vertice>();
	for (int j = 0; j<max2;j++)
	    str = tokens.nextToken(); // skip Query[0] = (, and inp if I considering outputs
	
	for (int i = 0; i< max1; i++) {
    	    str = tokens.nextToken();

	    if (str.compareTo("X")==0) break;
	    else {
	    	Vertice v = new Vertice(str,Vertice.PLACE);
		ret.add(v);
	     }
	}
	return ret; 
  }


public static ArrayList<Vertice> createAttriQ56I12O(StringTokenizer tokens, int type) {
	int max2,max1;
   	ArrayList<Vertice> ret;
	String str="";

	if (type ==0) {max1=6;max2=3;} //inpouts
        else {max1=2;max2=9;} //outputs

	ret = new ArrayList<Vertice>();
	for (int j = 0; j<max2;j++)
	    str = tokens.nextToken(); // skip Query[0] = (, and inp if I considering outputs

	
	for (int i = 0; i< max1; i++) {
    	    str = tokens.nextToken();

	    if (str.compareTo("X")==0) break;
	    else {
	    	Vertice v = new Vertice(str,Vertice.PLACE);
		ret.add(v);
	     }
	}

	return ret; 
  }




  public static double[] createWeighhtsQ(StringTokenizer tokens, int size) {
	int max1;
   	ArrayList<Vertice> ret;
	String str="";
	double[] qos = new double[5];
	if (size ==0) {max1 =9;} //small
        else if (size == 1){max1 = 15;} //medium
	else {max1 = 23;} // large

	ret = new ArrayList<Vertice>();
	for (int j = 0; j<max1;j++)
	    str = tokens.nextToken(); // skip Query[0] = (, and inp if I considering outputs

	for (int i=0; i<5 ; i++) {
            str = tokens.nextToken();

	    qos[i] = new Double(str); 	    

        }
	
	return qos; 
  }

  public static double[] createWeighhtsQ56I12O(StringTokenizer tokens) {
	int max1=11;
   	ArrayList<Vertice> ret;
	String str="";
	double[] qos = new double[5];
	
	ret = new ArrayList<Vertice>();
	for (int j = 0; j<max1;j++)
	    str = tokens.nextToken(); // skip Query[0] = (, and inp and outputs

	for (int i=0; i<5 ; i++) {
            str = tokens.nextToken();

	    qos[i] = new Double(str); 	    

        }
	
	return qos; 
  }

        
}
